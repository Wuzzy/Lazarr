# Lazarr!
Be a pirate and solve laser puzzles.

A pirate-themed 3D puzzle game by Wuzzy.

Current version: 2.0.0.beta.4

**PLEASE NOTE**: THIS GAME IS INCOMPLETE!

The game is playable and has 50 levels,
but it needs more polishing and testing.
Please give feedback!

Stay tuned for version 2.0.0!

### Missing features

* More levels
* Review low-quality/frustrating/boring levels
* Improve level sorting / organization
* Better-looking pirate ship
* Prevent nerdpoling (an exploit to circumvent some of the puzzles)
* Polishing
* A secret

## Gameplay

You're a pirate in the search of treasure.
Sail to different places of the world and find all shiny gold blocks
in treasure chests to advance to the next level.

But there's a catch! Most chests are locked by a mysterious mechanism
and the only way to unlock them is by solving puzzles with lasers.
Mirrors and other things will help you.

### Getting started

Your adventure begins at an old shipwreck. Find your way to the
treasure chest and open it to get your first gold block.

The on-screen instructions and Goldie the Parrot will guide you
through the game.

If you’ve completed the first level, you will enter your pirate ship,
the base of all your pirate operations.
Here you can travel to the next level, get help, view all your
loot, and more.

(In this beta version, the first 14 levels are basically a tutorial.
Future versions might change the level ordering so players don’t
play through the entire tutorial first.)

Your goal is to find all gold blocks in the game.

## Installing

You need Luanti version 5.10.0 (or later) to play this.
Download Luanti from <https://www.luanti.org>.

Refer to Luanti documentation to learn how to install a game in Luanti.

Once installed, create a world and start. The world contains your game's
progress and custom levels.

## Level Editor

Lazarr! contains a level editor. Here you can create custom levels.

Your pirate ship contains several books that explain how to use it.
Alternatively, you may also read the text file `LEVEL_EDITOR.md`.

Note that gold blocks collected in custom levels do not count towards
your total loot.

## Level packs

It’s possible to extend this game with custom level packs from the
community. These are collections of multiple levels.
Unlike for single levels from the level editor, Lazarr! keeps track of
your progress in level packs.

Level packs come in the form of Luanti mods.
Level pack mods have a name like `lzr_pack_<name>`, e.g. `lzr_pack_example`.
To use a level pack, enable the level pack mod in Luanti before starting
your Lazarr! world. It will then appear under the custom levels in your
pirate ship.

Levels created in the level editor end up in the special “Single levels”
level pack.

## History

Originally, this game was hastily made for the Minetest Game Jam 2021 in the
final day of the competition.
Version 1.2 was the version that was rated by the jury and players.
This version only had 10 levels, and level 8 was broken.
There was no base ship or any in-game explanation of the game.
But the main laser game mechanic worked, something I am very proud of I actually
managed to hack this together in only in one day.
Unsurprisingly, the game ranked poorly, only at rank 13.

In the years after, this game has seen some improvements since then,
but I'm still not satisfied with it.

## Credits/License

Lazarr!, a pirate-themed laser puzzle game
Copyright (C) 2024  Wuzzy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

The media files are released under a separate free cultural works license:
Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

You can find a copy of both licenses here:

* GNU GPLv3: `LICENSE_GPLv3.txt`
* CC BY-SA 4.0: `LICENSE_CC_BY_SA_4_0.txt`

See CREDITS.md for credits and more details.
