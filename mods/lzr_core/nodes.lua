local S = minetest.get_translator("lzr_core")

minetest.register_node("lzr_core:stone", {
	description = S("Stone"),
	tiles = {"default_stone.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})


minetest.register_node("lzr_core:stone_block", {
	description = S("Stone Block"),
	tiles = {"default_stone_block.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:stone_block_mossy", {
	description = S("Mossy Stone Block"),
	tiles = {"default_stone_block_mossy.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:stone_brick", {
	description = S("Stone Brick"),
	tiles = {"default_stone_brick.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:stone_brick_mossy", {
	description = S("Mossy Stone Brick"),
	tiles = {"default_stone_brick_mossy.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:stone_brick_circular", {
	description = S("Circular Stone"),
	tiles = {"default_stone_rune.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:stone_brick_circular_mossy", {
	description = S("Mossy Circular Stone"),
	tiles = {"default_stone_rune_mossy.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:stone_pillar", {
	description = S("Stone Pillar"),
	paramtype2 = "facedir",
	tiles = {
		"lzr_core_stone_pillar_top.png",
		"lzr_core_stone_pillar_top.png^[transformFY",
		"lzr_core_stone_pillar.png^[transformFX",
		"lzr_core_stone_pillar.png",
		"lzr_core_stone_pillar.png^[transformFX",
		"lzr_core_stone_pillar.png",
	},
	groups = { breakable = 1, rotatable = 3, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})


minetest.register_node("lzr_core:cave_stone", {
	description = S("Cave Stone"),
	tiles = {"lzr_core_cave_stone.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:cave_stone_block", {
	description = S("Cave Stone Block"),
	tiles = {"lzr_core_cave_stone_block.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:cave_stone_block_mossy", {
	description = S("Mossy Cave Stone Block"),
	tiles = {"lzr_core_cave_stone_block_mossy.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:cave_stone_brick", {
	description = S("Cave Stone Brick"),
	tiles = {"lzr_core_cave_stone_brick.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:cave_stone_brick_mossy", {
	description = S("Mossy Cave Stone Brick"),
	tiles = {"lzr_core_cave_stone_brick_mossy.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:cave_stone_brick_circular", {
	description = S("Circular Cave Stone"),
	tiles = {"lzr_core_cave_stone_rune.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:cave_stone_brick_circular_mossy", {
	description = S("Mossy Circular Cave Stone"),
	tiles = {"lzr_core_cave_stone_rune_mossy.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:cave_stone_pillar", {
	description = S("Cave Stone Pillar"),
	paramtype2 = "facedir",
	tiles = {
		"lzr_core_cave_stone_pillar_top.png",
		"lzr_core_cave_stone_pillar_top.png^[transformFY",
		"lzr_core_cave_stone_pillar.png^[transformFX",
		"lzr_core_cave_stone_pillar.png",
		"lzr_core_cave_stone_pillar.png^[transformFX",
		"lzr_core_cave_stone_pillar.png",
	},
	groups = { breakable = 1, rotatable = 3, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})


minetest.register_node("lzr_core:island_stone", {
	description = S("Island Stone"),
	tiles = {"lzr_core_island_stone.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:seabed", {
	description = S("Seabed"),
	tiles = {"lzr_core_seabed.png"},
	groups = { breakable = 1, ground = 1 },
	sounds = lzr_sounds.node_sound_dirt_defaults(),
})

minetest.register_node("lzr_core:sand", {
	description = S("Sand"),
	tiles = {"default_sand.png"},
	groups = { breakable = 1, ground = 1 },
	sounds = lzr_sounds.node_sound_sand_defaults(),
})
minetest.register_node("lzr_core:sandstone", {
	description = S("Sandstone"),
	tiles = {"default_sandstone.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:sandstone_block", {
	description = S("Sandstone Block"),
	tiles = {"default_sandstone_block.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:sandstone_brick", {
	description = S("Sandstone Brick"),
	tiles = {"default_sandstone_brick.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_core:dirt", {
	description = S("Dirt"),
	tiles = {"default_dirt.png"},
	groups = { breakable = 1, ground = 1 },
	sounds = lzr_sounds.node_sound_dirt_defaults(),
})

minetest.register_node("lzr_core:stone_cracked", {
	description = S("Cracked Stone"),
	tiles = {"lzr_core_stone_cracked.png"},
	groups = { breakable = 1, cracked = 1, stone = 1, explosion_destroys = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:cave_stone_cracked", {
	description = S("Cracked Cave Stone"),
	tiles = {"lzr_core_cave_stone_cracked.png"},
	groups = { breakable = 1, cracked = 1, stone = 1, explosion_destroys = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:sandstone_cracked", {
	description = S("Cracked Sandstone"),
	tiles = {"lzr_core_sandstone_cracked.png"},
	groups = { breakable = 1, cracked = 1, stone = 1, explosion_destroys = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_core:island_stone_cracked", {
	description = S("Cracked Island Stone"),
	tiles = {"lzr_core_island_stone_cracked.png"},
	groups = { breakable = 1, cracked = 1, stone = 1, explosion_destroys = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})


minetest.register_node("lzr_core:dirt_with_grass", {
	description = S("Dirt with Grass"),
	tiles = {"default_grass.png", "default_dirt.png",
		{name = "default_dirt.png^default_grass_side.png", tileable_vertical = false}},
	groups = { breakable = 1, ground = 1 },
	sounds = lzr_sounds.node_sound_dirt_defaults({
		footstep = { name = "lzr_sounds_footstep_grass", gain = 0.4 },
	}),
})

minetest.register_node("lzr_core:dirt_with_jungle_litter", {
	description = S("Dirt with Jungle Litter"),
	tiles = {"default_rainforest_litter.png", "default_dirt.png",
		{name = "default_dirt.png^default_rainforest_litter_side.png", tileable_vertical = false}},
	groups = { breakable = 1, ground = 1 },
	sounds = lzr_sounds.node_sound_dirt_defaults({
		footstep = { name = "lzr_sounds_footstep_jungle_litter", gain = 0.4 },
	}),
})

minetest.register_node("lzr_core:shrub_leaves", {
	description = S("Shrub Leaves"),
	paramtype = "light",
	drawtype = "allfaces",
	tiles = {"lzr_core_shrub_leaves.png"},
	use_texture_alpha = "clip",
	groups = { breakable = 1, leaves = 1 },
	sounds = lzr_sounds.node_sound_leaves_defaults(),
	is_ground_content = false,
})

minetest.register_node("lzr_core:trigs", {
	description = S("Twigs"),
	paramtype = "light",
	drawtype = "allfaces",
	tiles = {"lzr_core_twigs.png"},
	use_texture_alpha = "clip",
	groups = { breakable = 1 },
	sounds = lzr_sounds.node_sound_sticks_defaults(),
	is_ground_content = false,
})

local palm_leaves = {
	{ "bright_palm_leaves", S("Bright Palm Leaves"), "lzr_core_palm_leaves_top.png", "lzr_core_palm_leaves.png" },
	{ "dark_palm_leaves", S("Dark Palm Leaves"), "lzr_core_palm_leaves_dark_top.png", "lzr_core_palm_leaves_dark.png" },
}

for p=1, #palm_leaves do
	local id = palm_leaves[p][1]
	local desc = palm_leaves[p][2]
	local tex1 = palm_leaves[p][3]
	local tex2 = palm_leaves[p][4]
	lzr_laser.register_element("lzr_core:"..id, {
		description = desc,
		paramtype = "light",
		drawtype = "mesh",
		__mesh_off = "lzr_core_palm_leaves_laser.obj",
		__mesh_on = "lzr_core_palm_leaves_laser.obj",
		collision_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
		},
		__tiles_off = {
			{name = tex1, backface_culling = false},
			"blank.png",
			{name = tex2, backface_culling = false},
		},
		__tiles_on = {
			{name = tex1, backface_culling = false},
			lzr_laser.LASER_TILE,
			{name = tex2, backface_culling = false},
		},
		__light_source_on = lzr_globals.LASER_GLOW,
		__use_texture_alpha_off = "clip",
		__use_texture_alpha_on = lzr_laser.ALPHA_LASER,
		groups = { laser_block = 1, breakable = 1, leaves = 1 },
		sounds = lzr_sounds.node_sound_leaves_defaults(),
		is_ground_content = false,
	}, { group = "palm_leaves" } )
end

minetest.register_node("lzr_core:tree", {
	description = S("Tree Trunk"),
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = { breakable = 1, rotatable = 3, tree = 1 },
	sounds = lzr_sounds.node_sound_hard_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("lzr_core:palm_tree", {
	description = S("Palm Tree Trunk"),
	tiles = {"lzr_core_palm_tree_top.png", "lzr_core_palm_tree_top.png", "lzr_core_palm_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = { breakable = 1, rotatable = 3, tree = 1 },
	sounds = lzr_sounds.node_sound_hard_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("lzr_core:coconut_tree", {
	description = S("Coconut Tree Trunk"),
	tiles = {"coconut_tree_top.png", "coconut_tree_top.png", "coconut_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = { breakable = 1, rotatable = 3, tree = 1 },
	sounds = lzr_sounds.node_sound_hard_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("lzr_core:coconut", {
	description = S("Coconut"),
	paramtype2 = "facedir",
	tiles = {"coconut_top.png","coconut.png"},
	groups = { breakable = 1, rotatable = 3, fruit = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})

minetest.register_node("lzr_core:wood", {
	description = S("Wood Planks"),
	tiles = {"default_wood.png"},
	is_ground_content = false,
	groups = { breakable = 1, wood = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})

minetest.register_node("lzr_core:wood_loose", {
	description = S("Loose Wood Planks"),
	tiles = {"lzr_core_wood_loose.png"},
	is_ground_content = false,
	groups = { breakable = 1, wood = 1 },
	sounds = lzr_sounds.node_sound_wood_squeak_defaults(),
})

minetest.register_node("lzr_core:wood_mossy", {
	description = S("Mossy Wood Planks"),
	tiles = {"lzr_core_wood_mossy.png"},
	is_ground_content = false,
	groups = { breakable = 1, wood = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})



minetest.register_node("lzr_core:palm_wood", {
	description = S("Palm Wood Planks"),
	tiles = {"lzr_core_palm_wood.png"},
	is_ground_content = false,
	groups = { breakable = 1, wood = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})

minetest.register_node("lzr_core:coconut_wood", {
	description = S("Coconut Wood Planks"),
	tiles = {"lzr_core_coconut_wood.png"},
	is_ground_content = false,
	groups = { breakable = 1, wood = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})

-- A full non-waving block of water.
-- The laser will just be blocked when it hits water,
-- this is intentional. We don't support "lasers through
-- liquids".
-- Unlike in other games, water is completely static.
-- Water does NOT flow, there is NO flowing water node.
-- Laser-compatible, so it can be used in levels.
minetest.register_node("lzr_core:water_source", {
	description = S("Water Source"),
	drawtype = "liquid",
	-- Intentionally not waving to be laser-compatible
	waving = 0,
	tiles = {
		{
			name = "default_water_source_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
		{
			name = "default_water_source_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	use_texture_alpha = "blend",
	pointable = false,
	paramtype = "light",
	walkable = false,
	buildable_to = false,
	is_ground_content = false,
	liquidtype = "source",
	liquid_range = 0,
	liquid_renewable = false,
	liquid_alternative_flowing = "lzr_core:water_source",
	liquid_alternative_source = "lzr_core:water_source",
	liquid_viscosity = 1,
	move_resistance = 1,
	liquid_range = 0,
	liquid_renewable = false,
	liquids_pointable = true,
	post_effect_color = {a = 128, r = 17, g = 69, b = 135},
	post_effect_color_shaded = true,
	groups = {water = 3, liquid = 3},
	sounds = lzr_sounds.node_sound_water_defaults(),
})

-- Same as lzr_core:water_source, but
-- acts as an inpenetrable barrier
minetest.register_node("lzr_core:water_source_barrier", {
	--~ Block that acts as an inpenetrable barrier
	description = S("Water Source Barrier"),
	_tt_help = S("Block that the player can’t move through"),
	drawtype = "liquid",
	-- Intentionally not waving to be laser-compatible
	waving = 0,
	inventory_image = "default_water.png^lzr_core_water_source_barrier.png",
	tiles = {
		{
			name = "default_water_source_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
		{
			name = "default_water_source_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	use_texture_alpha = "blend",
	pointable = "blocking",
	paramtype = "light",
	walkable = true,
	buildable_to = false,
	is_ground_content = false,
	liquidtype = "source",
	liquid_range = 0,
	liquid_renewable = false,
	liquid_alternative_flowing = "lzr_core:water_source_barrier",
	liquid_alternative_source = "lzr_core:water_source_barrier",
	liquid_viscosity = 1,
	move_resistance = 1,
	liquid_range = 0,
	liquid_renewable = false,
	liquids_pointable = true,
	post_effect_color = {a = 128, r = 17, g = 69, b = 135},
	post_effect_color_shaded = true,
	groups = {water = 3, liquid = 3, not_in_creative_inventory = 1},
	sounds = lzr_sounds.node_sound_water_defaults(),
})


-- For invisible level borders,
-- should act like an invisible barrier.
minetest.register_node("lzr_core:barrier", {
	--~ Block that acts as an inpenetrable barrier
	description = S("Barrier"),
	_tt_help = S("Invisible block that the player can’t move through"),
	drawtype = "airlike",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	wield_image = "lzr_core_barrier.png",
	inventory_image = "lzr_core_barrier.png",
	groups = { barrier = 1, breakable = 1, not_in_creative_inventory = 1 },
	is_ground_content = false,

	-- Can't point through or select; player should not be able to
	-- build at the level borders or point anything through it.
	pointable = "blocking",
	pointabilities = {
		nodes = {
			["lzr_core:barrier"] = true,
		}
	},

	-- Prevent building things on the barrier
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		return itemstack
	end,
})

-- Rain membrane corner position
local RMC = 0.25
-- Rain membrane offset (a tiny value);
-- determines the X/Z size of the columns
local RMO = 0.01

-- HACK: A variant of the barrier node that lets rain through (mostly)
-- but not the player. This node can be used as a
-- level ceiling for rainy levels.
minetest.register_node("lzr_core:rain_membrane", {
	--~ Block that acts as a barrier. Player can't move through but rain can
	description = S("Rain Membrane"),
	_tt_help = S("Invisible block that the player can’t move through but rain can"),
	drawtype = "airlike",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = true,
	wield_image = "lzr_core_rain_membrane.png",
	inventory_image = "lzr_core_rain_membrane.png",
	groups = { rain_membrane = 1, breakable = 1, not_in_creative_inventory = 1 },

	pointable = "blocking",
	pointabilities = {
		nodes = {
			["lzr_core:rain_membrane"] = true,
		}
	},

	collision_box = lzr_globals.RAIN_MEMBRANE_COLLISION_BOX,
	selection_box = {
		type = "regular",
	},
	is_ground_content = false,

	-- Prevent building things on the rain membrane
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		return itemstack
	end,
})

minetest.register_alias("lzr_core:bright_palm_leaves", "lzr_core:bright_palm_leaves_fixed")
minetest.register_alias("lzr_core:dark_palm_leaves", "lzr_core:dark_palm_leaves_fixed")
minetest.register_alias("lzr_core:ocean_water_source", "lzr_core:water_source")
