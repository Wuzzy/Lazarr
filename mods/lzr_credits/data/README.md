# Syntax of `credits.csv` for `lzr_credits`

`credits.csv` contains the credits for this mod. This is a CSV file
compliant with RFC 4180.

It is a list of people as well as many information about for what
exactly they are being credited for. When this file is rendered,
the information will be rendered in the same order as in this file.

Each row/record has a type in column 1. The meaning of the other columns depend
on the the type. Unused columns must be left empty.

These are the supported types:

## Type `header`

This represents a header text. This is used to categorize the credits, e.g. Sounds, Music, Programming, etc.

Rows:

* Row 2: Header text
* Other rows: *unused*

## Type `person`

This represents a single person who is credited for something.

Rows:

* Row 2: Person nickname
* Row 3: Official person name
* Row 4: What the person is credited for
* Row 5: Denotes the type of row 4 by a number:
	* 0: Translatable text, normal font (default)
	* 1: Untranslatable text, italic font (useful for music track titles)
* Row 6: URL of the original source file
* Row 7: Original file name(s). Multiple files may be separated by "|". File name MUST NOT contain the character "|"
* Row 8: In-game file name(s). Multiple files may be separated by "|". File name MUST NOT contain the character "|"
* Row 9: License, using SPDX license identifier <https://spdx.org/licenses/> or one of the special values below
* Row 10: Denotes whether the file was modified from the original. (required for compliance with the Attribution clause in Creative Commons licenses)
	* 0: Unspecified (default)
	* 1: Original file, unmodified
	* 2: Modified from the original

Provide as many information as possible. At least a name (official, nick, or both) and the license are required.
For the person name, their self-chosen name MUST be used.

All other columns are optional but. Note that some fields are still required for stuff under a Creative Commons license with the Attribution clause.

For the license, the following special values are supported (compatible with the SPDX Specification):

* `LicenseRef-1`: For the license OGA-BY 3.0 <https://static.opengameart.org/OGA-BY-3.0.txt>
* `NONE`: When a license does not apply. Only use this when the thing in question is something abstract like an idea, or for a "Special Thanks" section. Otherwise, this must be avoided

## Type `people`

This represents a group of people who are collectively credited for something.
May also be used for organizations and similar.

Rows:

Same as for `person`, except row 2 is for the names of multiple people and row 3 is unused.
