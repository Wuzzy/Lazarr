-- This Lua file is not actually meant to be executed. Instead, it contains
-- dummy function calls to NS() to allow translation scripts to detect the strings.
-- MUST be updated before release when the credits have changed.

-- To update this list, do this:
-- 1. Set `lzr_print_special_translatable_strings` in the settings to `true`
-- 2. Start game
-- 3. Paste the relevant part of the console output into this file

local NS = function(s) return end

-- LIST OF STRINGS BEGINS HERE --

NS("Idea")
NS("Lead Programming")
NS("Additional Programming")
NS("Player model")
NS("Stairs and slabs")
NS("Decoration blocks from xdecor mod")
NS("Original screwdriver2 mod, used for the rotating hook")
NS("Level Design")
NS("2D Graphics")
NS("Water, wood (normal), tree (normal), stone (normal, tile, mossy tile, brick, mossy brick, circular, mossy circular), sandstone brick, sandstone block, gold block, wooden chest, onyx chest, ship lightbox, wood lightbox, light crate, heavy crate, mossy heavy crate, bonfire, seaweed, purple coral, crab grass, fire, wool (cloth), bomb, bookshelf, teleporter, television, pickaxe, bucket")
NS("Barrel (normal), cabinet, half cabinet, candle (floor, inventory, wall, wield), cauldron, cobweb, flower pot, empty shelf, iron lightbox, lantern, multishelf, wooden lightbox, woodframed glass, wood frame, chair/table wood, television, barricade, saw")
NS("Emitter base image")
NS("Detector base image")
NS("Emitter extra images")
NS("Detector extra images")
NS("Sandstone, palm wood, coconut wood, loose wood, mossy wood, cave stone (normal, block, brick, pillar), stone pillar, circular stone, mossy circular stone, forge animation, cold forge, hanging candle, ship’s wheel, mirror, mixer, beam splitter, double mirror, crystal, ant grass (small and large), iron grate/bars, rusty iron grate/bars, old crate, old barrel, screws, wooden door, maps, paintings")
NS("Parrots")
NS("Player texture")
NS("Scorched player texture")
NS("Raindrop, crosshair, wieldhand, smoke puff, bomb smoke, book interface, damage screen, trigger icons, laser particles, level corner marker, menu item marker, laser patterns, trigger tool, forbidden icon")
NS("Main menu images")
NS("Vessels shelf")
NS("Ocean stone (all variants), ocean lantern")
NS("Island-style blocks: Dirt, grass cover, island stone, island grass, sand, seabed, shrub leaves, palm tree, twigs")
NS("Wild Cotton")
NS("Skulls")
NS("Coconut and coconut tree")
NS("Thatch")
NS("Hotbar")
NS("Dialog window background")
NS("Dialog window background")
NS("Dialog window button")
NS("Hook")
NS("Speaker")
NS("Any texture not listed above")
NS("3D Graphics")
NS("Mirror, crystal, ship’s wheel, parrot, trivial shapes")
NS("Skull")
NS("Table, chair")
NS("Player model")
NS("Music")
NS("Sound Effects")
NS("Stone digging, dirt footstep/placement/digging, sand footstep/placement/digging, cloth footstep/placement/digging, leaves footstep, grass footstep/digging, jungle litter footstep, water footstep/placement/digging, sticks footstep/placement, forge footstep, inventory full warning")
NS("Stone footstep")
NS("Stone placement")
NS("Metal digging")
NS("Metal digging")
NS("Metal footstep")
NS("Metal placement")
NS("Glass footstep")
NS("Glass digging")
NS("Glass placement")
NS("Wood footstep")
NS("Wood placement and digging")
NS("Normal rotation, wood rotation")
NS("Glass/mirror rotation")
NS("Stone rotation")
NS("Metal rotation")
NS("Laser emitter activation")
NS("Level entering and leaving")
NS("Level completion jingle")
NS("Game completion jingle")
NS("Button")
NS("Barricade burning")
NS("Sticks digging / barricade breaking")
NS("Skull footstep/placement/digging/rotation")
NS("Squeaky wood")
NS("Chest opening")
NS("Chest opening fails")
NS("Chest lock breaks")
NS("Chest lock regenerates")
NS("Detector activates/deactivates")
NS("Lightbox activates/deactivates")
NS("Speaker turns on/off")
NS("Player damage")
NS("Skull laugh")
NS("Bomb fuse")
NS("Bomb explosion")
NS("Change color or screw")
NS("Old crate breaks")
NS("Default block digging/placement")
NS("Parrot curr")
NS("Door lock")
NS("Any sound not listed above")
NS("Translations")
NS("German")
NS("Chinese (traditional)")
NS("Spanish")
NS("French")
NS("Russian")
