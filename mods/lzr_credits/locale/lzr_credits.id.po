# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Lazarr! package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Lazarr! 2.0.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: mods/lzr_credits/init.lua:307
msgid "Credits for Lazarr!"
msgstr ""

#. ~ Credits line. @1 = what the author is credited for, @2 = official/formal author name, @3 author nickname
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:376
msgid "@1: @2 (alias @3)"
msgstr ""

#. ~ Credits line. @1 = official/formal author name, @2 author nickname
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:383
msgid "@1 (alias @2)"
msgstr ""

#. ~ Credits line. @1 = what the author is credited for, @2 = author name
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:403
msgid "@1: @2"
msgstr ""

#. ~ list separator
#: mods/lzr_credits/init.lua:421
msgid ", "
msgstr ""

#. ~ List of file names.
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:428
msgid "• File name: @1"
msgid_plural "• File names: @1"
msgstr[0] ""
msgstr[1] ""

#. ~ List of original file names.
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:436
msgid "• Original file name: @1"
msgid_plural "• Original file names: @1"
msgstr[0] ""
msgstr[1] ""

#. ~ For the URL of the origin of a credited file. @1 = the URL.
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:443
msgid "• Origin: @1"
msgstr ""

#. ~ Annotation for modified files
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:448
msgid "• Modifications were made"
msgstr ""

#. ~ File license.
#. IMPORTANT: If a "<", ">", or "\" occurs in the translation, you MUST escape it by putting a backslash in front
#: mods/lzr_credits/init.lua:459
msgid "• License: @1"
msgstr ""

#: mods/lzr_credits/init.lua:478
msgid "Display full credits"
msgstr ""

#: mods/lzr_credits/init.lua:479
msgid ""
"Display additional information, including license, file names and links to "
"the source material"
msgstr ""

#: mods/lzr_credits/init.lua:500
msgid "Display the list of people who worked on this game"
msgstr ""

#: mods/lzr_credits/_locale.lua:14
msgid "Idea"
msgstr ""

#: mods/lzr_credits/_locale.lua:15
msgid "Lead Programming"
msgstr ""

#: mods/lzr_credits/_locale.lua:16
msgid "Additional Programming"
msgstr ""

#: mods/lzr_credits/_locale.lua:17 mods/lzr_credits/_locale.lua:53
msgid "Player model"
msgstr ""

#: mods/lzr_credits/_locale.lua:18
msgid "Stairs and slabs"
msgstr ""

#: mods/lzr_credits/_locale.lua:19
msgid "Decoration blocks from xdecor mod"
msgstr ""

#: mods/lzr_credits/_locale.lua:20
msgid "Original screwdriver2 mod, used for the rotating hook"
msgstr ""

#: mods/lzr_credits/_locale.lua:21
msgid "Level Design"
msgstr ""

#: mods/lzr_credits/_locale.lua:22
msgid "2D Graphics"
msgstr ""

#: mods/lzr_credits/_locale.lua:23
msgid ""
"Water, wood (normal), tree (normal), stone (normal, tile, mossy tile, brick, "
"mossy brick, circular, mossy circular), sandstone brick, sandstone block, "
"gold block, wooden chest, onyx chest, ship lightbox, wood lightbox, light "
"crate, heavy crate, mossy heavy crate, bonfire, seaweed, purple coral, crab "
"grass, fire, wool (cloth), bomb, bookshelf, teleporter, television, pickaxe, "
"bucket"
msgstr ""

#: mods/lzr_credits/_locale.lua:24
msgid ""
"Barrel (normal), cabinet, half cabinet, candle (floor, inventory, wall, "
"wield), cauldron, cobweb, flower pot, empty shelf, iron lightbox, lantern, "
"multishelf, wooden lightbox, woodframed glass, wood frame, chair/table wood, "
"television, barricade, saw"
msgstr ""

#: mods/lzr_credits/_locale.lua:25
msgid "Emitter base image"
msgstr ""

#: mods/lzr_credits/_locale.lua:26
msgid "Detector base image"
msgstr ""

#: mods/lzr_credits/_locale.lua:27
msgid "Emitter extra images"
msgstr ""

#: mods/lzr_credits/_locale.lua:28
msgid "Detector extra images"
msgstr ""

#: mods/lzr_credits/_locale.lua:29
msgid ""
"Sandstone, palm wood, coconut wood, loose wood, mossy wood, cave stone "
"(normal, block, brick, pillar), stone pillar, circular stone, mossy circular "
"stone, forge animation, cold forge, hanging candle, ship’s wheel, mirror, "
"mixer, beam splitter, double mirror, crystal, ant grass (small and large), "
"iron grate/bars, rusty iron grate/bars, old crate, old barrel, screws, "
"wooden door, maps, paintings"
msgstr ""

#: mods/lzr_credits/_locale.lua:30
msgid "Parrots"
msgstr ""

#: mods/lzr_credits/_locale.lua:31
msgid "Player texture"
msgstr ""

#: mods/lzr_credits/_locale.lua:32
msgid "Scorched player texture"
msgstr ""

#: mods/lzr_credits/_locale.lua:33
msgid ""
"Raindrop, crosshair, wieldhand, smoke puff, bomb smoke, book interface, "
"damage screen, trigger icons, laser particles, level corner marker, menu "
"item marker, laser patterns, trigger tool, forbidden icon"
msgstr ""

#: mods/lzr_credits/_locale.lua:34
msgid "Main menu images"
msgstr ""

#: mods/lzr_credits/_locale.lua:35
msgid "Vessels shelf"
msgstr ""

#: mods/lzr_credits/_locale.lua:36
msgid "Ocean stone (all variants), ocean lantern"
msgstr ""

#: mods/lzr_credits/_locale.lua:37
msgid ""
"Island-style blocks: Dirt, grass cover, island stone, island grass, sand, "
"seabed, shrub leaves, palm tree, twigs"
msgstr ""

#: mods/lzr_credits/_locale.lua:38
msgid "Wild Cotton"
msgstr ""

#: mods/lzr_credits/_locale.lua:39
msgid "Skulls"
msgstr ""

#: mods/lzr_credits/_locale.lua:40
msgid "Coconut and coconut tree"
msgstr ""

#: mods/lzr_credits/_locale.lua:41
msgid "Thatch"
msgstr ""

#: mods/lzr_credits/_locale.lua:42
msgid "Hotbar"
msgstr ""

#: mods/lzr_credits/_locale.lua:43 mods/lzr_credits/_locale.lua:44
msgid "Dialog window background"
msgstr ""

#: mods/lzr_credits/_locale.lua:45
msgid "Dialog window button"
msgstr ""

#: mods/lzr_credits/_locale.lua:46
msgid "Hook"
msgstr ""

#: mods/lzr_credits/_locale.lua:47
msgid "Speaker"
msgstr ""

#: mods/lzr_credits/_locale.lua:48
msgid "Any texture not listed above"
msgstr ""

#: mods/lzr_credits/_locale.lua:49
msgid "3D Graphics"
msgstr ""

#: mods/lzr_credits/_locale.lua:50
msgid "Mirror, crystal, ship’s wheel, parrot, trivial shapes"
msgstr ""

#: mods/lzr_credits/_locale.lua:51
msgid "Skull"
msgstr ""

#: mods/lzr_credits/_locale.lua:52
msgid "Table, chair"
msgstr ""

#: mods/lzr_credits/_locale.lua:54
msgid "Music"
msgstr ""

#: mods/lzr_credits/_locale.lua:55
msgid "Sound Effects"
msgstr ""

#: mods/lzr_credits/_locale.lua:56
msgid ""
"Stone digging, dirt footstep/placement/digging, sand footstep/placement/"
"digging, cloth footstep/placement/digging, leaves footstep, grass footstep/"
"digging, jungle litter footstep, water footstep/placement/digging, sticks "
"footstep/placement, forge footstep, inventory full warning"
msgstr ""

#: mods/lzr_credits/_locale.lua:57
msgid "Stone footstep"
msgstr ""

#: mods/lzr_credits/_locale.lua:58
msgid "Stone placement"
msgstr ""

#: mods/lzr_credits/_locale.lua:59 mods/lzr_credits/_locale.lua:60
msgid "Metal digging"
msgstr ""

#: mods/lzr_credits/_locale.lua:61
msgid "Metal footstep"
msgstr ""

#: mods/lzr_credits/_locale.lua:62
msgid "Metal placement"
msgstr ""

#: mods/lzr_credits/_locale.lua:63
msgid "Glass footstep"
msgstr ""

#: mods/lzr_credits/_locale.lua:64
msgid "Glass digging"
msgstr ""

#: mods/lzr_credits/_locale.lua:65
msgid "Glass placement"
msgstr ""

#: mods/lzr_credits/_locale.lua:66
msgid "Wood footstep"
msgstr ""

#: mods/lzr_credits/_locale.lua:67
msgid "Wood placement and digging"
msgstr ""

#: mods/lzr_credits/_locale.lua:68
msgid "Normal rotation, wood rotation"
msgstr ""

#: mods/lzr_credits/_locale.lua:69
msgid "Glass/mirror rotation"
msgstr ""

#: mods/lzr_credits/_locale.lua:70
msgid "Stone rotation"
msgstr ""

#: mods/lzr_credits/_locale.lua:71
msgid "Metal rotation"
msgstr ""

#: mods/lzr_credits/_locale.lua:72
msgid "Laser emitter activation"
msgstr ""

#: mods/lzr_credits/_locale.lua:73
msgid "Level entering and leaving"
msgstr ""

#: mods/lzr_credits/_locale.lua:74
msgid "Level completion jingle"
msgstr ""

#: mods/lzr_credits/_locale.lua:75
msgid "Game completion jingle"
msgstr ""

#: mods/lzr_credits/_locale.lua:76
msgid "Button"
msgstr ""

#: mods/lzr_credits/_locale.lua:77
msgid "Barricade burning"
msgstr ""

#: mods/lzr_credits/_locale.lua:78
msgid "Sticks digging / barricade breaking"
msgstr ""

#: mods/lzr_credits/_locale.lua:79
msgid "Skull footstep/placement/digging/rotation"
msgstr ""

#: mods/lzr_credits/_locale.lua:80
msgid "Squeaky wood"
msgstr ""

#: mods/lzr_credits/_locale.lua:81
msgid "Chest opening"
msgstr ""

#: mods/lzr_credits/_locale.lua:82
msgid "Chest opening fails"
msgstr ""

#: mods/lzr_credits/_locale.lua:83
msgid "Chest lock breaks"
msgstr ""

#: mods/lzr_credits/_locale.lua:84
msgid "Chest lock regenerates"
msgstr ""

#: mods/lzr_credits/_locale.lua:85
msgid "Detector activates/deactivates"
msgstr ""

#: mods/lzr_credits/_locale.lua:86
msgid "Lightbox activates/deactivates"
msgstr ""

#: mods/lzr_credits/_locale.lua:87
msgid "Speaker turns on/off"
msgstr ""

#: mods/lzr_credits/_locale.lua:88
msgid "Player damage"
msgstr ""

#: mods/lzr_credits/_locale.lua:89
msgid "Skull laugh"
msgstr ""

#: mods/lzr_credits/_locale.lua:90
msgid "Bomb fuse"
msgstr ""

#: mods/lzr_credits/_locale.lua:91
msgid "Bomb explosion"
msgstr ""

#: mods/lzr_credits/_locale.lua:92
msgid "Change color or screw"
msgstr ""

#: mods/lzr_credits/_locale.lua:93
msgid "Old crate breaks"
msgstr ""

#: mods/lzr_credits/_locale.lua:94
msgid "Default block digging/placement"
msgstr ""

#: mods/lzr_credits/_locale.lua:95
msgid "Parrot curr"
msgstr ""

#: mods/lzr_credits/_locale.lua:96
msgid "Door lock"
msgstr ""

#: mods/lzr_credits/_locale.lua:97
msgid "Any sound not listed above"
msgstr ""

#: mods/lzr_credits/_locale.lua:98
msgid "Translations"
msgstr ""

#: mods/lzr_credits/_locale.lua:99
msgid "German"
msgstr ""

#: mods/lzr_credits/_locale.lua:100
msgid "Chinese (traditional)"
msgstr ""

#: mods/lzr_credits/_locale.lua:101
msgid "Spanish"
msgstr ""

#: mods/lzr_credits/_locale.lua:102
msgid "French"
msgstr ""

#: mods/lzr_credits/_locale.lua:103
msgid "Russian"
msgstr ""
