# `lzr_weather` API

This mod adds rain and lightning. The weather can *only* be changed by API calls.
Weather is independent from the sky, and no sound effects are included;
those are supposed to be done by other mods.

## Weather types

The following weather types are available:

* `"clear"`: Clear weather (does nothing)
* `"drizzle"`: Light rain
* `"rain"`: Medium rain
* `"storm"`: Heavy rain and occassional lightning

## Functions

The following functions are available:

### `lzr_weather.get_weather()`

Returns the current weather as a string.

### `lzr_weather.get_weather_types()`

Returns a table of all available weather types.

### `lzr_weather.set_weather(weather_type)`

Set the weather to the specified weather type.

### `lzr_weather.weather_exists(weather_type)`

Returns true if the given weather type exists,
false otherwise.
