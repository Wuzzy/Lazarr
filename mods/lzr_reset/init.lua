local S = minetest.get_translator("lzr_reset")

lzr_reset = {}
lzr_reset.reset_progress = function()
	-- Reset level progress of all level packs
	local names = lzr_levels.get_level_pack_names()
	for n=1, #names do
		local level_pack = lzr_levels.get_level_pack(names[n])
		if level_pack then
			lzr_levels.clear_level_progress(level_pack)
		end
	end

	-- Reset hidden parrot progress
	lzr_parrot_npc.clear_hidden_parrot_progress()
	lzr_menu.remove_hidden_parrots()
	lzr_menu.remove_painting("parrot_finder")

	-- Reset "menu seen" status
	lzr_levels.set_menu_seen(false)

	local player = minetest.get_player_by_name("singleplayer")
	if player then
		lzr_messages.show_message(player, S("Game progress reset"), 5)
	end
end

minetest.register_chatcommand("reset_progress", {
	privs = {},
	params = "yes",
	description = S("Reset your progress in this game"),
	func = function(name, param)
		if param == "yes" then
			lzr_reset.reset_progress()

			-- Message
			return true, S("Game progress reset.")
		else
			return false, S("To reset the game progress, use “/reset_progress yes”")
		end
	end,
})


