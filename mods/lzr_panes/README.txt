Lazarr! mod: lzr_panes
======================
Simple flat panes for things like iron bars, windows, etc.
Does not contain any nodes, this is an API for mods to use.

Mod by Wuzzy, released under MIT License (see `license.txt`).

