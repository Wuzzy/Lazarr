local HTHICK = 1/16 -- half pane thickness

lzr_panes = {}
function lzr_panes.register_pane(basename, def)
	local groups
	if def.groups then
		groups = table.copy(def.groups)
	else
		groups = {}
	end
	groups.pane = 1
	groups.laser_block  = 1
	groups.rotatable = 3

	local front = def.textures[1]
	local leftright = def.textures[2]
	local topbottom = def.textures[3]
	local back = def.textures[4] or front

	lzr_laser.register_element(basename .. "_flat", {
		description = def.description,
		_tt_help = def._tt_help,
		drawtype = "mesh",
		paramtype = "light",
		is_ground_content = false,
		sunlight_propagates = true,
		inventory_image = def.inventory_image,
		wield_image = def.wield_image,
		paramtype2 = "facedir",

		on_rightclick = def.on_rightclick,
		on_punch = def.on_punch,

		__mesh_off = "lzr_panes_pane_on.obj",
		__mesh_on = "lzr_panes_pane_on.obj",
		__tiles_off = {
			{ name = topbottom, backface_culling = true },
			{ name = leftright, backface_culling = true },
			{ name = front, backface_culling = true },
			{ name = back, backface_culling = true },
			"blank.png", -- no laser
		},
		__tiles_on = {
			{ name = topbottom, backface_culling = true },
			{ name = leftright, backface_culling = true },
			{ name = front, backface_culling = true },
			{ name = back, backface_culling = true },
			lzr_laser.LASER_TILE,
		},
		__light_source_on = lzr_globals.LASER_GLOW,
		__use_texture_alpha_off = "clip",
		__use_texture_alpha_on = lzr_laser.ALPHA_LASER,

		groups = groups,
		sounds = def.sounds,
		selection_box = {
			type = "fixed",
			fixed = {{-1/2, -1/2, -HTHICK, 1/2, 1/2, HTHICK}},
		},
		collision_box = {
			type = "fixed",
			fixed = {{-1/2, -1/2, -HTHICK, 1/2, 1/2, HTHICK}},
		},

	}, { group = def.element_group } )
end


