lzr_hand = {}

-- Range of editor hand
local RANGE_EDITOR = 20.0

-- Default hand
local handdef = {
	type = "none",
	wield_image = "wieldhand.png",
	wield_scale = {x=1,y=1,z=2.5},
	tool_capabilities = {
		max_drop_level = 0,
		groupcaps = {
			takable = { times = { [1] = 0, [2] = 0, [3] = 0 } },
			punchdig = { times = { [1] = 0, [2] = 0, [3] = 0 } },
		},
	},
	-- When using the [Place] key on a trigger node with the bare
	-- hand, show its trigger info (HUD overlay).
	-- It's somewhat hacky we override on_place of the hand for this,
	-- but it surprisingly works without side-effects.
	on_place = function(itemstack, placer, pointed_thing)
		if pointed_thing.type ~= "node" then
			-- IMPORTANT: We MUST use this return statement in
			-- any case where the trigger info function was
			-- not case, so that node-specific on_rightclick
			-- handlers (and similar) events still fire.
			return minetest.item_place(itemstack, placer, pointed_thing)
		end
		-- Make sure the external mod functions are available before
		-- calling them
		if not minetest.global_exists("lzr_triggers") or not minetest.global_exists("lzr_laser") then
			return minetest.item_place(itemstack, placer, pointed_thing)
		end
		-- If pointed node is a trigger, show its info
		local pos = pointed_thing.under
		local meta = minetest.get_meta(pos)
		local trigger_id = meta:get_string("trigger_id")
		if lzr_triggers.trigger_exists(trigger_id) then
			lzr_laser.show_trigger_info(trigger_id, placer)
			-- Return to override the default placement handler
			return nil
		end
		return minetest.item_place(itemstack, placer, pointed_thing)
	end,

	-- Implied default hand range is 4
}
minetest.register_item(":", handdef)

-- Editor hand
-- Identical to default hand except it has increased range
local editor_handdef = table.copy(handdef)
editor_handdef.range = RANGE_EDITOR
editor_handdef.wield_image = "wieldhand.png"
editor_handdef.inventory_image = "wieldhand.png"
if not editor_handdef.groups then
	editor_handdef.groups = {}
end
editor_handdef.groups.not_in_creative_inventory = 1
minetest.register_item("lzr_hand:hand_editor", editor_handdef)

-- Scorched hand
-- Identical to default hand except it is scorched
local scorched_handdef = table.copy(handdef)
scorched_handdef.wield_image = "lzr_hand_hand_scorched.png"
scorched_handdef.inventory_image = "lzr_hand_hand_scorched.png"
if not scorched_handdef.groups then
	scorched_handdef.groups = {}
end
scorched_handdef.groups.not_in_creative_inventory = 1
minetest.register_item("lzr_hand:hand_scorched", scorched_handdef)

-- Switch hand type for player.
-- hand_hype is one of "normal", "scorched" or "editor"
function lzr_hand.set_hand(player, hand_type)
	local inv = player:get_inventory()
	if hand_type == "normal" then
		minetest.log("action", "[lzr_hand] Setting hand to normal hand")
		inv:set_stack("hand", 1, "")
	elseif hand_type == "scorched" then
		minetest.log("action", "[lzr_hand] Setting hand to scorched hand")
		inv:set_stack("hand", 1, "lzr_hand:hand_scorched")
	elseif hand_type == "editor" then
		minetest.log("action", "[lzr_hand] Setting hand to editor hand")
		inv:set_stack("hand", 1, "lzr_hand:hand_editor")
	end
end
