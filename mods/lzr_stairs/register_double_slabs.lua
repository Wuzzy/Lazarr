-- Register double slabs

-- A double slab is a full cube which looks like two slabs have been put
-- on top of each other: A big one at the bottom, and thinner one on top
-- to fill in the gap.
-- A reverse double slab is the same, except the thin slab is at the bottom.

-- Naming rules:
-- * Double slab: lzr_stairs:double_slab_<subname>
-- * Reverse double slab: lzr_stairs:double_slab_reverse_<subname>

-- Double slabs are registered "by hand", i.e. without any template function.
-- By convention, double slabs should not be used to combine 2 slabs of
-- different type (e.g. stone + mossy stone)....

local S = minetest.get_translator("lzr_stairs")


local on_rotate = function(pos, node, rotate_axis, rotate_dir)
	if rotate_axis ~= "y" then
		node.name = string.gsub(node.name, "lzr_stairs:double_slab_", "lzr_stairs:double_slab_reverse_")
		minetest.swap_node(pos, node)
		return true
	else
		return false
	end
end
local on_rotate_reverse = function(pos, node, rotate_axis, rotate_dir)
	if rotate_axis ~= "y" then
		node.name = string.gsub(node.name, "lzr_stairs:double_slab_reverse_", "lzr_stairs:double_slab_")
		minetest.swap_node(pos, node)
		return true
	else
		return false
	end
end

-- Stone Block
minetest.register_node("lzr_stairs:double_slab_stone_block", {
	description = S("Double Stone Block Slab"),
	tiles = {"default_stone_block.png", "default_stone_block.png", "lzr_stairs_stone_block_slab.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate,
})
minetest.register_node("lzr_stairs:double_slab_reverse_stone_block", {
	description = S("Reverse Double Stone Block Slab"),
	tiles = {"default_stone_block.png", "default_stone_block.png", "lzr_stairs_stone_block_slab_top.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate_reverse,
})

-- Mossy Stone Block
minetest.register_node("lzr_stairs:double_slab_stone_block_mossy", {
	description = S("Double Mossy Stone Block Slab"),
	tiles = {"default_stone_block_mossy.png", "default_stone_block_mossy.png", "lzr_stairs_stone_block_mossy_slab.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate,
})
minetest.register_node("lzr_stairs:double_slab_reverse_stone_block_mossy", {
	description = S("Reverse Double Mossy Stone Block Slab"),
	tiles = {"default_stone_block_mossy.png", "default_stone_block_mossy.png", "lzr_stairs_stone_block_mossy_slab_top.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate_reverse,
})

-- Cave Stone Block
minetest.register_node("lzr_stairs:double_slab_cave_stone_block", {
	description = S("Double Cave Stone Block Slab"),
	tiles = {"lzr_core_cave_stone_block.png", "lzr_core_cave_stone_block.png", "lzr_stairs_cave_stone_block_slab.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate,
})
minetest.register_node("lzr_stairs:double_slab_reverse_cave_stone_block", {
	description = S("Reverse Double Cave Stone Block Slab"),
	tiles = {"lzr_core_cave_stone_block.png", "lzr_core_cave_stone_block.png", "lzr_stairs_cave_stone_block_slab_top.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate_reverse,
})

-- Mossy Cave Stone Block
minetest.register_node("lzr_stairs:double_slab_cave_stone_block_mossy", {
	description = S("Double Mossy Cave Stone Block Slab"),
	tiles = {"lzr_core_cave_stone_block_mossy.png", "lzr_core_cave_stone_block_mossy.png", "lzr_stairs_cave_stone_block_mossy_slab.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate,
})
minetest.register_node("lzr_stairs:double_slab_reverse_cave_stone_block_mossy", {
	description = S("Reverse Double Mossy Cave Stone Block Slab"),
	tiles = {"lzr_core_cave_stone_block_mossy.png", "lzr_core_cave_stone_block_mossy.png", "lzr_stairs_cave_stone_block_mossy_slab_top.png"},
	groups = { breakable = 1, stone = 1, double_slab = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_stone_defaults(),

	_lzr_on_rotate = on_rotate_reverse,
})
