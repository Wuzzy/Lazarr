Stairs for Lazarr!
==================
This mod adds stairs and slabs, but with adjustments for
the game Lazarr! to make them compatible with lasers.

This mod was forked from the 'stairs' mod of Minetest Game.

Differences from the original:
* Stair and slab are slightly thicker
* Slab has a laser variant
* Slab can't be rotated

See license.txt for license information.

Authors of source code
----------------------
Originally by Kahrl <kahrl@gmx.net> (LGPLv2.1+) and
celeron55, Perttu Ahola <celeron55@gmail.com> (LGPLv2.1+)
Various Luanti developers and contributors (LGPLv2.1+)
Modfications by Wuzzy for Lazarr! (LGPLv2.1+)

