-- Load support for translation.
local S = minetest.get_translator("lzr_stairs")

-- Same as S, but will be ignored by translation updater scripts
local T = S

-- IMPORTANT: In this file, the node descriptions are NOT
-- marked as translatable. This is because some node descriptions
-- are generated automatically, which is bad for translations.
-- The list of node descriptions to be used in the game
-- is in translate.lua, which must be manually refreshed
-- before a release. See translate.lua for details.


-- If enabled, will print a list of translatable strings
-- for the registered nodes to the console so it can
-- be copied into this source file.
local GENERATE_TRANSLATABLE_STRING_LIST = minetest.settings:get_bool("lzr_print_special_translatable_strings", false)

-- Local functions so we can apply translations.
-- IMPORTANT: The description argument MUST be provided as bare,
-- untranslated string (see above).
local function my_register_slab(subname, recipeitem, groups, images, desc_slab, sounds, worldaligntex, images_top_slab)
	lzr_stairs.register_slab(subname, recipeitem, groups, images, S(desc_slab), sounds, worldaligntex, images_top_slab)

	if GENERATE_TRANSLATABLE_STRING_LIST then
		print(("NS(%q)"):format(desc_slab))
	end
end
local function my_register_stair(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
	lzr_stairs.register_stair(subname, recipeitem, groups, images, S(desc_stair), sounds, worldaligntex, vertical_rotation)

	if GENERATE_TRANSLATABLE_STRING_LIST then
		print(("NS(%q)"):format(desc_stair))
	end
end
local function my_register_stair_inner(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
	lzr_stairs.register_stair_inner(subname, recipeitem, groups, images, "", sounds, worldaligntex, T("Inner " .. desc_stair), vertical_rotation)

	if GENERATE_TRANSLATABLE_STRING_LIST then
		print(("NS(%q)"):format("Inner "..desc_stair))
	end
end
local function my_register_stair_outer(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
	lzr_stairs.register_stair_outer(subname, recipeitem, groups, images, "", sounds, worldaligntex, T("Outer " .. desc_stair), vertical_rotation)

	if GENERATE_TRANSLATABLE_STRING_LIST then
		print(("NS(%q)"):format("Outer "..desc_stair))
	end
end

local function my_register_stairs(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
	my_register_stair(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
	my_register_stair_inner(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
	my_register_stair_outer(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
end
local function my_register_stairs_and_slab(subname, recipeitem, groups, images, desc_stair, desc_slab, sounds, worldaligntex, images_top_slab, vertical_rotation)
	my_register_stairs(subname, recipeitem, groups, images, desc_stair, sounds, worldaligntex, vertical_rotation)
	my_register_slab(subname, recipeitem, groups, images, desc_slab, sounds, worldaligntex, images_top_slab)
end

-- Register default stairs and slabs

if GENERATE_TRANSLATABLE_STRING_LIST then
	print("-- TRANSLATABLE STRINGS FOR STAIRS AND SLABS [lzr_stairs] --")
end

my_register_stairs_and_slab(
	"wood",
	"lzr_core:wood",
	{breakable=1, wood=1},
	{"default_wood.png"},
	"Wooden Stair",
	"Wooden Slab",
	lzr_sounds.node_sound_wood_defaults(),
	true
)
my_register_stairs_and_slab(
	"wood_mossy",
	"lzr_core:wood_mossy",
	{breakable=1, wood=1},
	{"lzr_core_wood_mossy.png"},
	"Mossy Wooden Stair",
	"Mossy Wooden Slab",
	lzr_sounds.node_sound_wood_defaults(),
	true
)
my_register_stairs_and_slab(
	"palm_wood",
	"lzr_core:palm_wood",
	{breakable=1, wood=1},
	{"lzr_core_palm_wood.png"},
	"Palm Wood Stair",
	"Palm Wood Slab",
	lzr_sounds.node_sound_wood_defaults(),
	true
)
my_register_stairs_and_slab(
	"coconut_wood",
	"lzr_core:coconut_wood",
	{breakable=1, wood=1},
	{"lzr_core_coconut_wood.png"},
	"Coconut Wood Stair",
	"Coconut Wood Slab",
	lzr_sounds.node_sound_wood_defaults(),
	true
)

my_register_slab(
	"stone_block",
	nil,
	{breakable=1, stone=1},
	{"default_stone_block.png", "default_stone_block.png", "lzr_stairs_stone_block_slab.png"},
	"Stone Block Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true,
	{"default_stone_block.png", "default_stone_block.png", "lzr_stairs_stone_block_slab_top.png"}
)
my_register_stair(
	"stone_block",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_stone_block_slab.png^[transformFY","default_stone_block.png","lzr_stairs_stone_block_stair.png^[transformFX","lzr_stairs_stone_block_stair.png","default_stone_block.png","lzr_stairs_stone_block_slab.png"},
	"Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_inner(
	"stone_block",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_stone_block_stair_inner_top.png","default_stone_block.png","lzr_stairs_stone_block_stair.png^[transformFX","default_stone_block.png","default_stone_block.png","lzr_stairs_stone_block_stair.png"},
	"Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_outer(
	"stone_block",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_stone_block_stair_outer_top.png","default_stone_block.png","lzr_stairs_stone_block_stair_outer_right.png","lzr_stairs_stone_block_stair.png","lzr_stairs_stone_block_stair.png^[transformFX","lzr_stairs_stone_block_stair_outer_left.png"},
	"Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)

my_register_slab(
	"stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"default_stone_block_mossy.png", "default_stone_block_mossy.png", "lzr_stairs_stone_block_mossy_slab.png"},
	"Mossy Stone Block Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true,
	{"default_stone_block_mossy.png", "default_stone_block_mossy.png", "lzr_stairs_stone_block_mossy_slab_top.png"}
)
my_register_stair(
	"stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_stone_block_mossy_slab.png^[transformFY","default_stone_block_mossy.png","lzr_stairs_stone_block_mossy_stair.png^[transformFX","lzr_stairs_stone_block_mossy_stair.png","default_stone_block_mossy.png","lzr_stairs_stone_block_mossy_slab.png"},
	"Mossy Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_inner(
	"stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_stone_block_mossy_stair_inner_top.png","default_stone_block_mossy.png","lzr_stairs_stone_block_mossy_stair.png^[transformFX","default_stone_block_mossy.png","default_stone_block_mossy.png","lzr_stairs_stone_block_mossy_stair.png"},
	"Mossy Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_outer(
	"stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_stone_block_mossy_stair_outer_top.png","default_stone_block_mossy.png","lzr_stairs_stone_block_mossy_stair_outer_right.png","lzr_stairs_stone_block_mossy_stair.png","lzr_stairs_stone_block_mossy_stair.png^[transformFX","lzr_stairs_stone_block_mossy_stair_outer_left.png"},
	"Mossy Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)


my_register_slab(
	"cave_stone_block",
	nil,
	{breakable=1, stone=1},
	{"lzr_core_cave_stone_block.png", "lzr_core_cave_stone_block.png", "lzr_stairs_cave_stone_block_slab.png"},
	"Cave Stone Block Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true,
	{"lzr_core_cave_stone_block.png", "lzr_core_cave_stone_block.png", "lzr_stairs_cave_stone_block_slab_top.png"}
)
my_register_stair(
	"cave_stone_block",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_cave_stone_block_slab.png^[transformFY","lzr_core_cave_stone_block.png","lzr_stairs_cave_stone_block_stair.png^[transformFX","lzr_stairs_cave_stone_block_stair.png","lzr_core_cave_stone_block.png","lzr_stairs_cave_stone_block_slab.png"},
	"Cave Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_inner(
	"cave_stone_block",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_cave_stone_block_stair_inner_top.png","lzr_core_cave_stone_block.png","lzr_stairs_cave_stone_block_stair.png^[transformFX","lzr_core_cave_stone_block.png","lzr_core_cave_stone_block.png","lzr_stairs_cave_stone_block_stair.png"},
	"Cave Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_outer(
	"cave_stone_block",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_cave_stone_block_stair_outer_top.png","lzr_core_cave_stone_block.png","lzr_stairs_cave_stone_block_stair_outer_right.png","lzr_stairs_cave_stone_block_stair.png","lzr_stairs_cave_stone_block_stair.png^[transformFX","lzr_stairs_cave_stone_block_stair_outer_left.png"},
	"Cave Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)

my_register_slab(
	"cave_stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"lzr_core_cave_stone_block_mossy.png", "lzr_core_cave_stone_block_mossy.png", "lzr_stairs_cave_stone_block_mossy_slab.png"},
	"Mossy Cave Stone Block Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true,
	{"lzr_core_cave_stone_block_mossy.png", "lzr_core_cave_stone_block_mossy.png", "lzr_stairs_cave_stone_block_mossy_slab_top.png"}
)
my_register_stair(
	"cave_stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_cave_stone_block_mossy_slab.png^[transformFY","lzr_core_cave_stone_block_mossy.png","lzr_stairs_cave_stone_block_mossy_stair.png^[transformFX","lzr_stairs_cave_stone_block_mossy_stair.png","lzr_core_cave_stone_block_mossy.png","lzr_stairs_cave_stone_block_mossy_slab.png"},
	"Mossy Cave Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_inner(
	"cave_stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_cave_stone_block_mossy_stair_inner_top.png","lzr_core_cave_stone_block_mossy.png","lzr_stairs_cave_stone_block_mossy_stair.png^[transformFX","lzr_core_cave_stone_block_mossy.png","lzr_core_cave_stone_block_mossy.png","lzr_stairs_cave_stone_block_mossy_stair.png"},
	"Mossy Cave Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)
my_register_stair_outer(
	"cave_stone_block_mossy",
	nil,
	{breakable=1, stone=1},
	{"lzr_stairs_cave_stone_block_mossy_stair_outer_top.png","lzr_core_cave_stone_block_mossy.png","lzr_stairs_cave_stone_block_mossy_stair_outer_right.png","lzr_stairs_cave_stone_block_mossy_stair.png","lzr_stairs_cave_stone_block_mossy_stair.png^[transformFX","lzr_stairs_cave_stone_block_mossy_stair_outer_left.png"},
	"Mossy Cave Stone Block Stair",
	lzr_sounds.node_sound_stone_defaults(),
	false
)



my_register_stairs_and_slab(
	"stone",
	"lzr_core:stone",
	{breakable=1, stone=1},
	{"default_stone.png"},
	"Stone Stair",
	"Stone Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true
)
my_register_stairs_and_slab(
	"cave_stone",
	"lzr_core:cave_stone",
	{breakable=1, stone=1},
	{"lzr_core_cave_stone.png"},
	"Cave Stone Stair",
	"Cave Stone Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true
)
my_register_stairs_and_slab(
	"island_stone",
	"lzr_core:island_stone",
	{breakable=1, stone=1},
	{"lzr_core_island_stone.png"},
	"Island Stone Stair",
	"Island Stone Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true
)
my_register_stairs_and_slab(
	"ocean_stone",
	"lzr_decor:ocean_stone",
	{breakable=1, stone=1},
	{"xocean_stone.png"},
	"Ocean Stone Stair",
	"Ocean Stone Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true
)
my_register_stairs_and_slab(
	"ocean_cobble",
	"lzr_decor:ocean_cobble",
	{breakable=1, stone=1},
	{"xocean_cobble.png"},
	"Ocean Cobblestone Stair",
	"Ocean Cobblestone Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true
)
my_register_stairs_and_slab(
	"ocean_bricks",
	"lzr_decor:ocean_bricks",
	{breakable=1, stone=1},
	{"xocean_brick.png"},
	"Ocean Bricks Stair",
	"Ocean Bricks Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true
)

my_register_stairs_and_slab(
	"thatch",
	"lzr_decor:thatch",
	{breakable=1,thatch=1},
	{"dryplants_thatch.png"},
	"Thatch Stair",
	"Thatch Slab",
	lzr_sounds.node_sound_leaves_defaults(),
	true
)
my_register_stairs_and_slab(
	"thatch_wet",
	"lzr_decor:thatch_wet",
	{breakable=1,thatch=1},
	{"dryplants_thatch_wet.png"},
	"Wet Thatch Stair",
	"Wet Thatch Slab",
	lzr_sounds.node_sound_leaves_defaults(),
	true
)

my_register_stairs_and_slab(
	"dirt",
	"lzr_core:dirt",
	{breakable=1,ground=1},
	{"default_dirt.png"},
	"Dirt Stair",
	"Dirt Slab",
	lzr_sounds.node_sound_dirt_defaults(),
	true
)
my_register_stairs_and_slab(
	"seabed",
	"lzr_core:seabed",
	{breakable=1,ground=1},
	{"lzr_core_seabed.png"},
	"Seabed Stair",
	"Seabed Slab",
	lzr_sounds.node_sound_dirt_defaults(),
	true
)
my_register_stairs_and_slab(
	"sand",
	"lzr_core:sand",
	{breakable=1,ground=1},
	{"default_sand.png"},
	"Sand Stair",
	"Sand Slab",
	lzr_sounds.node_sound_sand_defaults(),
	true
)
my_register_stairs_and_slab(
	"sandstone",
	"lzr_core:sandstone",
	{breakable=1,stone=1},
	{"default_sandstone.png"},
	"Sandstone Stair",
	"Sandstone Slab",
	lzr_sounds.node_sound_stone_defaults(),
	true
)

-- Dirt with Grass
my_register_slab(
	"dirt_with_grass",
	nil,
	{breakable=1,ground=1},
	{
		{name="default_grass.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_grass_overlay_slab.png"
	},
	"Dirt Slab with Grass",
	minetest.registered_nodes["lzr_core:dirt_with_grass"].sounds,
	false,
	{
		{name="default_grass.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_grass_overlay_slab_top.png"
	}
)
my_register_stair(
	"dirt_with_grass",
	"lzr_core:dirt_with_grass",
	{breakable=1,ground=1},
	{
		{name="default_grass.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_grass_overlay_stair.png^[transformFX",
		"default_dirt.png^lzr_stairs_grass_overlay_stair.png",
		"default_dirt.png^default_grass_side.png",
		"default_dirt.png^lzr_stairs_grass_overlay_slab.png"
	},
	"Dirt Stair with Grass",
	minetest.registered_nodes["lzr_core:dirt_with_grass"].sounds,
	false,
	-- Disallow vertical rotation; because of the grass, the node always needs to stand upright
	-- (same for all the following stair variants as well as dirt w/ jungle litter nodes)
	false
)
my_register_stair_inner(
	"dirt_with_grass",
	nil,
	{breakable=1,ground=1},
	{
		{name="default_grass.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^(lzr_stairs_grass_overlay_stair_inner.png^[transformFX)",
		"default_dirt.png^default_grass_side.png",
		"default_dirt.png^default_grass_side.png",
		"default_dirt.png^lzr_stairs_grass_overlay_stair_inner.png"
	},
	"Dirt Stair with Grass",
	minetest.registered_nodes["lzr_core:dirt_with_grass"].sounds,
	false,
	false
)
my_register_stair_outer(
	"dirt_with_grass",
	nil,
	{breakable=1,ground=1},
	{
		{name="default_grass.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_grass_overlay_stair_outer_right.png",
		"default_dirt.png^lzr_stairs_grass_overlay_stair.png",
		"default_dirt.png^(lzr_stairs_grass_overlay_stair.png^[transformFX)",
		"default_dirt.png^lzr_stairs_grass_overlay_stair_outer_left.png"
	},
	"Dirt Stair with Grass",
	minetest.registered_nodes["lzr_core:dirt_with_grass"].sounds,
	false,
	false
)


-- Dirt with Jungle Litter
my_register_slab(
	"dirt_with_jungle_litter",
	nil,
	{breakable=1,ground=1},
	{
		{name="default_rainforest_litter.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_slab.png"
	},
	"Dirt Slab with Jungle Litter",
	minetest.registered_nodes["lzr_core:dirt_with_jungle_litter"].sounds,
	false,
	{
		{name="default_rainforest_litter.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_slab_top.png"
	}
)

my_register_stair(
	"dirt_with_jungle_litter",
	"lzr_core:dirt_with_jungle_litter",
	{breakable=1,ground=1},
	{
		{name="default_rainforest_litter.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_stair.png^[transformFX",
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_stair.png",
		"default_dirt.png^default_rainforest_litter_side.png",
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_slab.png"
	},
	"Dirt Stair with Jungle Litter",
	minetest.registered_nodes["lzr_core:dirt_with_jungle_litter"].sounds,
	false, false
)
my_register_stair_inner(
	"dirt_with_jungle_litter",
	nil,
	{breakable=1,ground=1},
	{
		{name="default_rainforest_litter.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^(lzr_stairs_rainforest_litter_overlay_stair_inner.png^[transformFX)",
		"default_dirt.png^default_rainforest_litter_side.png",
		"default_dirt.png^default_rainforest_litter_side.png",
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_stair_inner.png"
	},
	"Dirt Stair with Jungle Litter",
	minetest.registered_nodes["lzr_core:dirt_with_jungle_litter"].sounds,
	false,
	false
)
my_register_stair_outer(
	"dirt_with_jungle_litter",
	nil,
	{breakable=1,ground=1},
	{
		{name="default_rainforest_litter.png", align_style="world"},
		{name="default_dirt.png", align_style="world"},
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_stair_outer_right.png",
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_stair.png",
		"default_dirt.png^(lzr_stairs_rainforest_litter_overlay_stair.png^[transformFX)",
		"default_dirt.png^lzr_stairs_rainforest_litter_overlay_stair_outer_left.png"
	},
	"Dirt Stair with Jungle Litter",
	minetest.registered_nodes["lzr_core:dirt_with_jungle_litter"].sounds,
	false,
	false
)

dofile(minetest.get_modpath("lzr_stairs").."/register_double_slabs.lua")
