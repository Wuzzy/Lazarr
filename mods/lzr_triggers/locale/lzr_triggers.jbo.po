msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain lzr_triggers x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: jbo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/lzr_triggers/check.lua:5
msgid "Malformed trigger ID"
msgstr ""

#: mods/lzr_triggers/check.lua:6
msgid "Invalid sender type of trigger"
msgstr ""

#: mods/lzr_triggers/check.lua:7
msgid "Invalid receiver type of trigger"
msgstr ""

#: mods/lzr_triggers/check.lua:8
msgid "Malformed trigger location"
msgstr ""

#: mods/lzr_triggers/check.lua:9
msgid "Trigger assigned to node that doesn’t support triggers"
msgstr ""

#: mods/lzr_triggers/check.lua:10
msgid "Locked chest can’t be opened due to lack of senders"
msgstr ""

#: mods/lzr_triggers/dialog.lua:55
msgid "Triggers"
msgstr ""

#: mods/lzr_triggers/dialog.lua:97
msgid "player"
msgstr ""

#: mods/lzr_triggers/dialog.lua:114
msgid "start"
msgstr ""

#: mods/lzr_triggers/dialog.lua:158
msgid "No triggers."
msgstr ""

#. ~ Trigger list header: Trigger identifier
#: mods/lzr_triggers/dialog.lua:164 mods/lzr_triggers/dialog.lua:190
#: mods/lzr_triggers/dialog.lua:241
msgid "ID"
msgstr ""

#. ~ Trigger list header: Trigger location
#: mods/lzr_triggers/dialog.lua:166 mods/lzr_triggers/dialog.lua:191
msgid "Location"
msgstr ""

#. ~ Trigger list header: Trigger node
#: mods/lzr_triggers/dialog.lua:168 mods/lzr_triggers/dialog.lua:192
msgid "Node"
msgstr ""

#. ~ Trigger list header: Number of receivers
#: mods/lzr_triggers/dialog.lua:170 mods/lzr_triggers/dialog.lua:193
msgid "#Recv."
msgstr ""

#. ~ Trigger list header: Number of senders
#: mods/lzr_triggers/dialog.lua:172 mods/lzr_triggers/dialog.lua:194
msgid "#Send."
msgstr ""

#. ~ Trigger list header: Sender type
#: mods/lzr_triggers/dialog.lua:175
msgid "Send. type"
msgstr ""

#. ~ Trigger list header: Receiver type
#: mods/lzr_triggers/dialog.lua:178 mods/lzr_triggers/dialog.lua:196
msgid "Recv. type"
msgstr ""

#: mods/lzr_triggers/dialog.lua:184
msgid "Triggers:"
msgstr ""

#: mods/lzr_triggers/dialog.lua:189
msgid "Columns:"
msgstr ""

#: mods/lzr_triggers/dialog.lua:190
msgid "@1: Unique trigger identifier"
msgstr ""

#: mods/lzr_triggers/dialog.lua:191
msgid ""
"@1: Current node location (start = initial position, player = in player "
"inventory)"
msgstr ""

#: mods/lzr_triggers/dialog.lua:192
msgid "@1: Name of the node that triggers"
msgstr ""

#: mods/lzr_triggers/dialog.lua:193
msgid "@1: Number of receivers this trigger sends to"
msgstr ""

#: mods/lzr_triggers/dialog.lua:194
msgid "@1: Number of senders this trigger receives from"
msgstr ""

#: mods/lzr_triggers/dialog.lua:195
msgid "Sig. type"
msgstr ""

#: mods/lzr_triggers/dialog.lua:195
msgid "@1: Sender type"
msgstr ""

#: mods/lzr_triggers/dialog.lua:196
msgid "@1: Receiver type"
msgstr ""

#: mods/lzr_triggers/dialog.lua:202
msgid "Sender types:"
msgstr ""

#: mods/lzr_triggers/dialog.lua:205 mods/lzr_triggers/dialog.lua:209
msgid "@1: @2"
msgstr ""

#: mods/lzr_triggers/dialog.lua:207
msgid "Receiver types:"
msgstr ""

#. ~ Trigger list header: Trigger type
#: mods/lzr_triggers/dialog.lua:239
msgid "Type"
msgstr ""

#: mods/lzr_triggers/dialog.lua:243
msgid "Receiver"
msgstr ""

#: mods/lzr_triggers/dialog.lua:250
msgid "Sender"
msgstr ""

#. ~ Shown when there are no triggers in a trigger list
#: mods/lzr_triggers/dialog.lua:259
msgid "None"
msgstr ""

#: mods/lzr_triggers/dialog.lua:262
msgid "Signals of trigger @1:"
msgstr ""

#: mods/lzr_triggers/dialog.lua:273
msgid "Show a list of all triggers"
msgstr ""

#: mods/lzr_triggers/dialog.lua:279
msgid "No player."
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:145
msgid "Activate OFF"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:147
msgid "Activate ON"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:149
msgid "Activate TOGGLE"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:151
msgid "Deactivate OFF"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:153
msgid "Deactivate ON"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:155
msgid "Deactivate TOGGLE"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:157
msgid "Synchronous"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:159
msgid "Synchronous inverted"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:161
msgid "Toggle"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:163
msgid "Toggle OFF"
msgstr ""

#. ~ Sender type name
#: mods/lzr_triggers/init.lua:165
msgid "Toggle ON"
msgstr ""

#. ~ Sender type description for sender type 'Synchronous'
#: mods/lzr_triggers/init.lua:171
msgid "send ON signal when activated, send OFF signal when deactivated"
msgstr ""

#. ~ Sender type description for sender type 'Synchronous inverted'
#: mods/lzr_triggers/init.lua:173
msgid "send OFF signal when activated, send ON signal when deactivated"
msgstr ""

#. ~ Sender type description for sender type 'Toggle'
#: mods/lzr_triggers/init.lua:175
msgid "send TOGGLE signal when toggled"
msgstr ""

#. ~ Sender type description for sender type 'Toggle ON'
#: mods/lzr_triggers/init.lua:177
msgid "send ON signal when toggled"
msgstr ""

#. ~ Sender type description for sender type 'Toggle OFF'
#: mods/lzr_triggers/init.lua:179
msgid "send OFF signal when toggled"
msgstr ""

#. ~ Sender type description for sender type 'Activate ON'
#: mods/lzr_triggers/init.lua:181
msgid "send ON signal when activated"
msgstr ""

#. ~ Sender type description for sender type 'Activate TOGGLE'
#: mods/lzr_triggers/init.lua:183
msgid "send TOGGLE signal when activated"
msgstr ""

#. ~ Sender type description for sender type 'Activate OFF'
#: mods/lzr_triggers/init.lua:185
msgid "send OFF signal when activated"
msgstr ""

#. ~ Sender type description for sender type 'Deactivate ON'
#: mods/lzr_triggers/init.lua:187
msgid "send ON signal when deactivated"
msgstr ""

#. ~ Sender type description for sender type 'Deactivate TOGGLE'
#: mods/lzr_triggers/init.lua:189
msgid "send TOGGLE signal when deactivated"
msgstr ""

#. ~ Sender type description for sender type 'Deactivate OFF'
#: mods/lzr_triggers/init.lua:191
msgid "send OFF signal when deactivated"
msgstr ""

#. ~ Receiver type name
#: mods/lzr_triggers/init.lua:246
msgid "Any"
msgstr ""

#. ~ Receiver type name
#: mods/lzr_triggers/init.lua:248
msgid "Synchronous AND"
msgstr ""

#. ~ Receiver type description for type 'Any'
#: mods/lzr_triggers/init.lua:253
msgid "react to any signal"
msgstr ""

#. ~ Receiver type description for type 'Synchronous AND'
#: mods/lzr_triggers/init.lua:255
msgid ""
"activates when receiving a signal and all its synchronous senders are active "
"and all its inverted synchronous senders are inactive; deactivates when "
"receiving a signal when that’s not the case"
msgstr ""
