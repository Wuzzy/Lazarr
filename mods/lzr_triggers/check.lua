local S = minetest.get_translator("lzr_triggers")


lzr_triggers.CHECK_ERROR_TEXTS = {
	bad_id = S("Malformed trigger ID"),
	bad_sender_type = S("Invalid sender type of trigger"),
	bad_receiver_type = S("Invalid receiver type of trigger"),
	bad_location = S("Malformed trigger location"),
	bad_node = S("Trigger assigned to node that doesn’t support triggers"),
	locked_chest_without_senders = S("Locked chest can’t be opened due to lack of senders"),
}

lzr_triggers.check_triggers = function(exit_after_first_error)
	local errors = {}
	local erroring_trigger_ids = {}
	-- Test: Invalid triggers
	for trigger_id, trigger in pairs(lzr_triggers.get_triggers()) do
		-- Check trigger for validity
		local trigger_pos = minetest.string_to_pos(trigger_id)
		if not trigger_pos then
			table.insert(errors, "bad_id")
			table.insert(erroring_trigger_ids, trigger_id)
			if exit_after_first_error then
				break
			end
		end
		if not trigger.sender_type or type(trigger.sender_type) ~= "number" or trigger.sender_type < 0 or trigger.sender_type > lzr_triggers.MAX_SENDER_TYPE then
			table.insert(errors, "bad_sender_type")
			table.insert(erroring_trigger_ids, trigger_id)
			if exit_after_first_error then
				break
			end
		end
		if not trigger.receiver_type or type(trigger.receiver_type) ~= "number" or trigger.receiver_type < 0 or trigger.receiver_type > lzr_triggers.MAX_RECEIVER_TYPE then
			table.insert(errors, "bad_receiver_type")
			table.insert(erroring_trigger_ids, trigger_id)
			if exit_after_first_error then
				break
			end
		end
		if not trigger.location or (trigger.location ~= "player" and type(trigger.location) ~= "table") then
			table.insert(errors, "bad_location")
			table.insert(erroring_trigger_ids, trigger_id)
			if exit_after_first_error then
				break
			end
		end
		-- Only nodes marked as sender or receiver (or both) are allowed to be assigned as trigger
		local node
		if type(trigger.location) == "table" then
			node = minetest.get_node(trigger.location)
		elseif trigger.location == "player" then
			local player = minetest.get_player_by_name("singleplayer")
			if player then
				local item = lzr_triggers.find_trigger_in_player_inventory(player, trigger_id)
				if item then
					node = { name = item:get_name(), param2 = 0 }
				end
			end
		end
		if node and minetest.get_item_group(node.name, "receiver") == 0 and minetest.get_item_group(node.name, "sender") == 0
				-- Special case: orphan receivers are seen as receivers (and thus valid)
				-- for the purposes of this check
				and minetest.get_item_group(node.name, "orphan_receiver") == 0 then
			table.insert(errors, "bad_node")
			table.insert(erroring_trigger_ids, trigger_id)
			if exit_after_first_error then
				break
			end
		end
		-- Locked chests that don't receive signals (=unsolvable level)
		if minetest.get_item_group(node.name, "chest") == 2 then
			local send = lzr_triggers.get_senders(trigger_id)
			if #send == 0 then
				table.insert(errors, "locked_chest_without_senders")
				table.insert(erroring_trigger_ids, trigger_id)
				if exit_after_first_error then
					break
				end
			end
		end
	end
	if #errors == 0 then
		return true
	else
		return false, errors, erroring_trigger_ids
	end
end
