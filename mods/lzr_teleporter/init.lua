-- Adds a teleporter node. It does nothing on its own,
-- it is meant to be placed in levels as the starter
-- block for the player to spawn on.

local S = minetest.get_translator("lzr_teleporter")

-- The 'off' in 'teleporter_off' is a remainder of when
-- there was a teleporter in 'on' state but it was
-- removed because it was never used.
minetest.register_node("lzr_teleporter:teleporter_off", {
	description = S("Teleporter"),
	paramtype2 = "4dir",
	_tt_help = S("Player start point of the level").."\n"..
		--~ The "teleporter" block (=start block of a level) has a white dot at one of the sides, marking the initial player look direction
		S("Player will face towards the white dot").."\n"..
		--~ Only one teleporter block allowed per level
		S("(only one per level allowed)"),
	tiles = {
		{ name = "lzr_teleporter_off_top.png", align_style = "world" },
		{ name = "lzr_teleporter_off_bottom.png", align_style = "world" },
		"lzr_teleporter_off_side.png",
		"lzr_teleporter_off_side.png",
		"lzr_teleporter_off_front.png",
		-- This texture shows a white dot to show the initial direction
		"lzr_teleporter_off_rear.png",
	},
	groups = { breakable = 1, rotatable = 3, teleporter = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})


minetest.register_alias("lzr_teleporter:teleporter_on", "lzr_teleporter:teleporter_off")
