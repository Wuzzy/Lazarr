-- Override Luanti's default node dropping handling.
-- Node drops are added to digger's inventory,
-- but they will NOT spawn item entities if the inventory is full or non-existing.
minetest.handle_node_drops = function(pos, drops, digger)
	-- Add dropped items to object's inventory
	local inv = digger and digger:get_inventory()
	if not inv then
		return
	end
	local give_item
	for _, dropped_item in pairs(drops) do
		inv:add_item("main", dropped_item)
	end
end
