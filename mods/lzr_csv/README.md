# `lzr_csv`: CSV Parser for Lazarr!

This mod can parse and write CSV (Comma-Separated Values)
files from and to strings.
Only CSV files that are RFC 4180-compliant are supported
to keep it simple.

This mod aims to be as simple as possible by avoiding
unnecessary and confusing features. Only the bare minimum,
but enough to be useful.

See `API.md` for the function reference.



## About RFC 4180

The CSV file format is really ancient but remains popular
because it is so simple. There was a long time where there
was no formal definition and many softwares disagree on the
"correct" definition of CSV.
Thus, RFC 4180 was born to fix this mess.

You can find RFC 4180 here <https://www.rfc-editor.org/rfc/rfc4180>



## License

This is free software, released under the terms of
the MIT License. <https://mit-license.org>
