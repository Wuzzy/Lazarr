# `lzr_csv` API

With this mod you can parse and write CSV from and to strings.
This mod is compliant with RFC 4180.

This file explains how to use it in Lua.

## Function reference

### `lzr_csv.parse_csv(text)`

Parses the string `text` which should be in a valid
CSV format according to RFC 4180 and returns a CSV table (see below).
`text` must be UTF-8 encoded.

All rows/records must have the same number of entries.

If the input is determined to be invalid, the function will
return `nil, <error message>` where `<error message>` is a
string to display in the log.

## `lzr_csv.write_csv(rows)`

Given a CSV table named `rows` as input (see below),
will return a string that is valid CSV syntax.


## CSV tables

A CSV table is the primary data structure representing the
data from a CSV-formatted file.

A CSV table is a table of tables.

The outer tables represent the rows.
The inner tables represent the values in each row and contain strings.

The length of each row *must* be equal.

For example, this table:

    {
    	{"value1", "value2", "value3"}, -- row 1
    	{"value4", "value5", "value6"}, -- row 2
    },

Is equivalent to the following CSV file:

    value1,value2,value3
    value4,value5,value6
