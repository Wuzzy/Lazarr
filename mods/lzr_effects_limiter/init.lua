lzr_effects_limiter = {}

local active_effects = {}

local registered_effects = {}

lzr_effects_limiter.register_effect = function(effect_id, def)
	registered_effects[effect_id] = {
		duration = math.ceil(def.duration * 1000000),
		max_count = def.max_count,
	}
	active_effects[effect_id] = {}
end

lzr_effects_limiter.add_effect = function(effect_id)
	local time = minetest.get_us_time()
	table.insert(active_effects[effect_id], time)
end

local remove_expired_effects = function(effect_id)
	local time = minetest.get_us_time()
	local effects = active_effects[effect_id]
	if #effects == 0 then
		return
	end
	local duration = registered_effects[effect_id].duration
	local remove = {}
	for e=1, #effects do
		local effect_time = effects[e]
		if time >= effect_time + duration then
			table.insert(remove, e)
		end
	end
	for r=1, #remove do
		table.remove(active_effects[effect_id], r)
	end
end

lzr_effects_limiter.can_add_effect = function(effect_id)
	remove_expired_effects(effect_id)
	if #active_effects[effect_id] > registered_effects[effect_id].max_count then
		return false
	else
		return true
	end
end

lzr_effects_limiter.add_effect_if_possible = function(effect_id)
	if lzr_effects_limiter.can_add_effect(effect_id) then
		lzr_effects_limiter.add_effect(effect_id)
		return true
	else
		return false
	end
end
