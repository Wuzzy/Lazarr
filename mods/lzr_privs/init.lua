lzr_privs = {}

local EDIT_PRIVS = { "fly", "fast", "noclip" }

lzr_privs.grant_edit_privs = function(player)
	local pname = player:get_player_name()
	local privs = minetest.get_player_privs(pname)
	for e=1, #EDIT_PRIVS do
		privs[EDIT_PRIVS[e]] = true
	end
	minetest.set_player_privs(pname, privs)
	minetest.log("action", "[lzr_privs] Editing privileges granted to "..pname)
end

lzr_privs.revoke_edit_privs = function(player)
	local pname = player:get_player_name()
	local privs = minetest.get_player_privs(pname)
	for e=1, #EDIT_PRIVS do
		privs[EDIT_PRIVS[e]] = nil
	end
	minetest.set_player_privs(pname, privs)
	minetest.log("action", "[lzr_privs] Editing privileges revoked from "..pname)
end

minetest.register_on_joinplayer(function(player)
	-- Reset editing privs if rejoining game in wrong gamestate
	if lzr_gamestate.get_state() ~= lzr_gamestate.EDITOR then
		lzr_privs.revoke_edit_privs(player)
	end
end)
