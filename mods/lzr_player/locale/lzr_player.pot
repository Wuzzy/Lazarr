# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Lazarr! package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Lazarr! 2.0.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/lzr_player/init.lua:15
msgid "Loading …"
msgstr ""

#: mods/lzr_player/init.lua:23
msgid "Current level: @1"
msgstr ""

#: mods/lzr_player/init.lua:27
msgid "Restart level"
msgstr ""

#: mods/lzr_player/init.lua:32 mods/lzr_player/init.lua:84
#: mods/lzr_player/init.lua:108
msgid "Inventory"
msgstr ""

#: mods/lzr_player/init.lua:48
msgid "Continue"
msgstr ""

#: mods/lzr_player/init.lua:50
msgid "Return to Ship"
msgstr ""

#: mods/lzr_player/init.lua:85 mods/lzr_player/init.lua:109
msgid "Get items"
msgstr ""

#: mods/lzr_player/init.lua:87
msgid "Level editor"
msgstr ""

#: mods/lzr_player/init.lua:88
msgid "Exit"
msgstr ""

#: mods/lzr_player/init.lua:89
msgid "Level settings"
msgstr ""

#: mods/lzr_player/init.lua:90
msgid "Save level"
msgstr ""

#: mods/lzr_player/init.lua:91
msgid "Load level"
msgstr ""
