local S = minetest.get_translator("lzr_player")
local F = minetest.formspec_escape

local INV_WIDTH_EDITOR = tonumber(minetest.settings:get("lzr_inventory_width_editor") or 8)
INV_WIDTH_EDITOR = math.min(10, math.max(8, INV_WIDTH_EDITOR))

local INV_SLOTS_PLAY = 4

lzr_player = {}

-- Set inventory formspec while in loading state
lzr_player.set_loading_inventory_formspec = function(player)
	-- Just an empty formspecj
	player:set_inventory_formspec("formspec_version[7]size[6,1.5]"..
		"label[1,0.75;"..F(S("Loading …")).."]")
end

lzr_player.set_play_inventory = function(player, current_level_name, can_restart, hide_items)
	local inv = player:get_inventory()
	inv:set_size("main", INV_SLOTS_PLAY)
	local label_level = ""
	if current_level_name ~= nil and current_level_name ~= "" then
		label_level = "label[0.5.3,3.0;"..F(S("Current level: @1", current_level_name)).."]"
	end
	local restart = ""
	if can_restart ~= true then
		restart = "button_exit[6.3,1.8;4.2,0.6;__lzr_levels_restart;"..F(S("Restart level")).."]"
	end

	local item_list
	if not hide_items then
		item_list = "label[0.5,0.5;"..F(S("Inventory")).."]"..
		"list[current_player;main;0.5,0.9;"..INV_SLOTS_PLAY..",1]"
	else
		item_list = ""
	end
	player:set_inventory_formspec(
		"formspec_version[7]size[11,3.6]"..

		-- Inventory
		item_list..

		-- Current level name (hidden if empty or unknown)
		label_level..

		-- Level navigation buttons
		"set_focus[continue;true]"..
		"button_exit[6.3,0.5;4.2,0.6;continue;"..F(S("Continue")).."]"..
		restart..
		"button_exit[6.3,2.5;4.2,0.6;__lzr_levels_leave;"..F(S("Return to Ship")).."]"
	)

	lzr_hand.set_hand(player, "normal")
end
lzr_player.set_editor_inventory = function(player, no_formspec)
	local inv = player:get_inventory()
	inv:set_size("main", INV_WIDTH_EDITOR * 4)
	lzr_hand.set_hand(player, "editor")

	if no_formspec then
		return
	end

	local sizew
	local buttonx_left = 0.65
	local buttonx_right = INV_WIDTH_EDITOR - 2.5
	local button_w = INV_WIDTH_EDITOR - 3.35
	if INV_WIDTH_EDITOR == 10 then
		sizew = 13.3
		buttonx_right = 6.7
		button_w = 5.9
	elseif INV_WIDTH_EDITOR == 9 then
		sizew = 12.05
		buttonx_right = 6.1
		button_w = 5.3
	else
		sizew = 10.8
		buttonx_right = 5.5
		button_w = 4.65
	end
	local box_w = sizew - 1.05
	local buttonx_getitem = sizew - 3.55
	player:set_inventory_formspec(
		"formspec_version[7]size["..sizew..",8.3]label[0.5,0.5;"..F(S("Inventory")).."]list[current_player;main;0.5,0.9;"..INV_WIDTH_EDITOR..",4]"..
		"button["..buttonx_getitem..",0.175;3,0.6;__lzr_level_editor_get_item;"..F(S("Get items")).."]"..
		"box[0.5,5.9;"..box_w..",2.1;#00000080]"..
		"label[0.6,6.15;"..F(S("Level editor")).."]"..
		"button_exit["..buttonx_left..",6.5;"..button_w..",0.6;__lzr_level_editor_exit;"..F(S("Exit")).."]"..
		"button["..buttonx_right..",6.5;"..button_w..",0.6;__lzr_level_editor_settings;"..F(S("Level settings")).."]"..
		"button["..buttonx_left..",7.3;"..button_w..",0.6;__lzr_level_editor_save;"..F(S("Save level")).."]"..
		"button["..buttonx_right..",7.3;"..button_w..",0.6;__lzr_level_editor_load;"..F(S("Load level")).."]"
	)
	-- NOTE: Event handling in lzr_editor
end
lzr_player.set_dev_inventory = function(player)
	local inv = player:get_inventory()
	inv:set_size("main", INV_WIDTH_EDITOR * 4)
	local sizew = INV_WIDTH_EDITOR + 2.8
	if INV_WIDTH_EDITOR == 10 then
		sizew = 13.3
	elseif INV_WIDTH_EDITOR == 9 then
		sizew = 12.05
	else
		sizew = 10.8
	end
	local buttonx = sizew - 3.55
	player:set_inventory_formspec(
		"formspec_version[7]size["..sizew..",6.3]label[0.5,0.5;"..F(S("Inventory")).."]list[current_player;main;0.5,0.9;"..INV_WIDTH_EDITOR..",4]"..
		"button["..buttonx..",0.2;3,0.6;__lzr_devmode_get_item;"..F(S("Get items")).."]"
	)
	-- NOTE: Event handling in lzr_devmode

	lzr_hand.set_hand(player, "editor")
end
lzr_player.set_menu_inventory = function(player)
	local inv = player:get_inventory()
	inv:set_size("main", INV_SLOTS_PLAY)
	player:set_inventory_formspec("")

	lzr_hand.set_hand(player, "normal")
end

minetest.register_on_joinplayer(function(player)
	local inv = player:get_inventory()
	inv:set_size("craft", 0)
	inv:set_size("hand", 1)
	lzr_sky.set_sky(lzr_globals.DEFAULT_SKY)

	lzr_player.set_menu_inventory(player)
end)

-- Can't drop items
function minetest.item_drop(itemstack, dropper, pos)
	local state = lzr_gamestate.get_state()
	if state == lzr_gamestate.EDITOR or state == lzr_gamestate.DEV then
		-- Destroy item in editor or def mode
		return ""
	else
		return itemstack
	end
end
