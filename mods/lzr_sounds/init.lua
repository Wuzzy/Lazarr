lzr_sounds = {}

function lzr_sounds.node_sound_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "", gain = 1.0}
	table.dug = table.dug or
			{name = "default_dug_node", gain = 0.25}
	table.place = table.place or
			{name = "default_place_node_hard", gain = 1.0}
	return table
end

function lzr_sounds.node_sound_stone_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_stone", gain = 0.9}
	table.place = table.place or
			{name = "lzr_sounds_place_stone", gain = 1.0}
	table.dug = table.dug or
			{name = "lzr_sounds_dug_stone", gain = 1.0}
	table._rotate = table._rotate or
			{name = "lzr_sounds_rotate_stone", gain = 0.2}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_leaves_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_leaves", gain = 1.0}
	table.dug = table.dug or
			{name = "lzr_sounds_footstep_leaves", gain = 1.0}
	table.place = table.place or
			{name = "default_place_node", gain = 1.0}
	table._rotate = table._rotate or
			{name = "lzr_sounds_footstep_leaves", gain = 0.6, pitch = 1.2}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_grass_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_grass", gain = 0.4}
	table.dug = table.dug or
			{name = "lzr_sounds_dug_grass", gain = 1.0}
	table.place = table.place or
			{name = "default_place_node", gain = 1.0}
	table._rotate = table._rotate or
			{name = "lzr_sounds_footstep_leaves", gain = 0.6, pitch = 1.2}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_dirt_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "default_dirt_footstep", gain = 0.6}
	table.dug = table.dug or
			{name = "lzr_sounds_dug_dirt", gain = 0.6}
	table.place = table.place or
			{name = "lzr_sounds_place_dirt", gain = 0.6}
	table._rotate = table._rotate or
			{name = "lzr_sounds_place_dirt", gain = 0.4, pitch = 1.2}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_sand_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "default_sand_footstep", gain = 0.4}
	table.dug = table.dug or
			{name = "lzr_sounds_dug_sand", gain = 0.9}
	table.place = table.place or
			{name = "lzr_sounds_place_sand", gain = 0.9}
	table._rotate = table._rotate or
			{name = "lzr_sounds_place_sand", gain = 0.7, pitch = 1.2}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_wood_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_wood", gain = 0.8}
	table.dug = table.dug or
			{name = "lzr_sounds_place_wood", gain = 0.8, pitch = 0.95 }
	table.place = table.place or
			{name = "lzr_sounds_place_wood", gain = 0.8}
	table._rotate = table._rotate or
			{name = "lzr_sounds_rotate_wood", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_sticks_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_sticks", gain = 1.0 }
	table.dug = table.dug or
			{name = "lzr_sounds_dug_sticks", gain = 1.0 }
	table.place = table.place or
			{name = "lzr_sounds_place_sticks", gain = 1.0 }
	lzr_sounds.node_sound_wood_defaults(table)
	return table
end



function lzr_sounds.node_sound_hard_wood_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_wood", gain = 1.0, pitch = 0.8}
	table.dug = table.dug or
			{name = "lzr_sounds_place_wood", gain = 1.0, pitch = 0.85 }
	table.place = table.place or
			{name = "lzr_sounds_place_wood", gain = 1.0, pitch = 0.9}
	lzr_sounds.node_sound_wood_defaults(table)
	return table
end

function lzr_sounds.node_sound_wood_squeak_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_wood_squeak", gain = 0.5}
	lzr_sounds.node_sound_wood_defaults(table)
	return table
end

function lzr_sounds.node_sound_glass_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_glass", gain = 0.3}
	table.place = table.place or
			{name = "lzr_sounds_place_glass", gain = 0.3}
	table.dig = table.dig or
			{name = "lzr_sounds_footstep_glass", gain = 0.5}
	table.dug = table.dug or
			{name = "lzr_sounds_dig_glass", gain = 1.0}
	table._rotate = table._rotate or
			{name = "lzr_sounds_rotate_glass", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_metal_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "default_metal_footstep", gain = 0.4}
	table.dig = table.dig or
			{name = "default_dig_metal", gain = 0.5}
	table.dug = table.dug or
			{name = "default_dug_metal", gain = 0.5}
	table.place = table.place or
			{name = "default_place_node_metal", gain = 0.5}
	table._rotate = table._rotate or
			{name = "lzr_sounds_rotate_metal", gain = 1.0}
	lzr_sounds.node_sound_defaults(table)
	return table
end

function lzr_sounds.node_sound_cloth_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_cloth", gain = 0.5}
	table.dug = table.dug or
			{name = "lzr_sounds_dug_cloth", gain = 1.0}
	table.place = table.place or
			{name = "lzr_sounds_place_cloth", gain = 1.0}
	table._rotate = table._rotate or
			{name = "lzr_sounds_place_cloth", gain = 0.9, pitch = 1.2}
	lzr_sounds.node_sound_defaults(table)
	return table
end



function lzr_sounds.node_sound_water_defaults(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "lzr_sounds_footstep_water", gain = 1.0}
	table.dug = table.dug or
			{name = "lzr_sounds_dug_water", gain = 0.9}
	table.place = table.place or
			{name = "lzr_sounds_place_water", gain = 0.3}
	table._rotate = table._rotate or
			{name = "lzr_sounds_place_water", gain = 0.3, pitch = 1.2}
	lzr_sounds.node_sound_defaults(table)
	return table
end

