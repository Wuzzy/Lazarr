# `lzr_hook`

This mod adds the rotating hook, an important tool to rotate blocks in Lazarr!

It can be used on rotatable and takable blocks.

## Controls

* Press [Punch] at block edge: "Push" the block towards that edge
* Press [Place] anywhere at block: Rotate block clockwise
* Press [Sneak]+[Punch]/[Place]: Reverse rotation

## Origin

It is based on the [screwdriver2] mod by 12Me21
from <https://forum.minetest.net/viewtopic.php?f=9&t=20856>,
but heavily modified to work with Lazarr!

The mods are *not* compatible with each other.

## License

This mod is released under the MIT License. See `LICENSE.txt`
for details.
