msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain lzr_hook x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-06 14:42+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/lzr_hook/init.lua:374
msgid "Rotating Hook"
msgstr ""

#: mods/lzr_hook/init.lua:375
msgid "Punch to push edge, place to rotate face"
msgstr ""

#: mods/lzr_hook/init.lua:376
msgid "Sneak to reverse rotation direction"
msgstr ""
