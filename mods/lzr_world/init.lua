-- This mod provides shared functions for information about the game
-- world.

lzr_world = {}

local level_size = vector.copy(lzr_globals.DEFAULT_LEVEL_SIZE)
local level_pos = vector.copy(lzr_globals.DEFAULT_LEVEL_POS)
local level_trigger = {}

-- Returns current level size
lzr_world.get_level_size = function()
	return level_size
end

-- Returns current level position
lzr_world.get_level_pos= function()
	return level_pos
end

-- Sets the current level size
lzr_world.set_level_size = function(new_size)
	level_size = vector.copy(new_size)
	minetest.log("verbose", "[lzr_world] Level size set to: "..minetest.pos_to_string(new_size))
end

-- Sets the current level position
lzr_world.set_level_pos = function(new_pos)
	level_pos = vector.copy(new_pos)
	minetest.log("verbose", "[lzr_world] Level pos set to: "..minetest.pos_to_string(new_pos))
end

-- Returns current minimum and maximum level positions
-- (2 return values, in that order)
lzr_world.get_level_bounds = function()
	local offset = table.copy(level_size)
	offset = vector.offset(offset, -1, -1, -1)
	return level_pos, vector.add(level_pos, offset)
end

-- Returns true if pos is within current level bounds.
lzr_world.is_in_level_bounds = function(pos)
	local offset = table.copy(lzr_world.get_level_size())
	offset = vector.offset(offset, -1, -1, -1)
	return vector.in_area(pos, level_pos, vector.add(level_pos, offset))
end

-- Convert (absolute) world position to (relative) level position
lzr_world.world_pos_to_level_pos = function(world_pos)
	local lpos = lzr_world.get_level_pos()
	return vector.subtract(world_pos, lpos)
end
-- Convert (relative) level position to (absolute) world position
lzr_world.level_pos_to_world_pos = function(level_pos)
	local lpos = lzr_world.get_level_pos()
	return vector.add(level_pos, lpos)
end


