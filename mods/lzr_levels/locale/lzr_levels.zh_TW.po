msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain lzr_levels x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 21:36+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/lzr_levels/init.lua:1368
#, fuzzy
msgid "Level complete!"
msgstr "最終關卡已破解！"

#: mods/lzr_levels/init.lua:1372
msgid "There are no treasures here!"
msgstr ""

#: mods/lzr_levels/init.lua:1406
msgid "Yarr! You did it! Our ship’s full of gold now. We’re rich!"
msgstr ""

#: mods/lzr_levels/init.lua:1407
msgid "You’ve collected every treasure in the known world!"
msgstr ""

#: mods/lzr_levels/init.lua:1408
msgid "No puzzle was too hard, no security mechanism could stop you."
msgstr ""

#: mods/lzr_levels/init.lua:1409
msgid "You have become the Perfect Plunderer!"
msgstr ""

#: mods/lzr_levels/init.lua:1411
msgid ""
"Thanks for playing the beta version of Lazarr! Stay tuned for more puzzles "
"in version 2.0.0."
msgstr ""

#: mods/lzr_levels/init.lua:1448
msgid "Final level completed!"
msgstr "最終關卡已破解！"

#. ~ Fallback name for untitled levels. @1 = technical level name based on file name
#: mods/lzr_levels/init.lua:1606
msgid "Untitled (@1)"
msgstr ""

#: mods/lzr_levels/init.lua:1891
msgid "Display information about the identity of the current level"
msgstr ""

#: mods/lzr_levels/init.lua:1907
msgid "• Level name: @1"
msgstr ""

#: mods/lzr_levels/init.lua:1909
msgid "• Level name (in your language): @1"
msgstr ""

#: mods/lzr_levels/init.lua:1910
msgid "• Level name (in English): @1"
msgstr ""

#: mods/lzr_levels/init.lua:1913
msgid "• File name: @1"
msgstr ""

#: mods/lzr_levels/init.lua:1916
msgid "Single levels"
msgstr ""

#: mods/lzr_levels/init.lua:1915 mods/lzr_levels/init.lua:1919
msgid "• Level pack: @1"
msgstr ""

#: mods/lzr_levels/init.lua:1921
msgid "• Level number: @1"
msgstr ""

#: mods/lzr_levels/init.lua:1929
msgid "Could not get level information."
msgstr ""

#: mods/lzr_levels/init.lua:1932
msgid "You’re in the level editor."
msgstr ""

#: mods/lzr_levels/init.lua:1934 mods/lzr_levels/init.lua:1955
msgid "Not playing in a level!"
msgstr "你不再關卡內！"

#: mods/lzr_levels/init.lua:1942
msgid "Restart current level"
msgstr "重新開始"

#: mods/lzr_levels/init.lua:1950 mods/lzr_levels/init.lua:2012
msgid "Can’t restart while loading!"
msgstr ""

#: mods/lzr_levels/init.lua:1953
msgid "Can’t restart during the level solution test!"
msgstr ""

#: mods/lzr_levels/init.lua:2006
msgid "Can’t leave while loading!"
msgstr ""

#~ msgid "Leave current level"
#~ msgstr "離開關卡"

#~ msgid "Reset level progress"
#~ msgstr "重設關卡進度"

#~ msgid "Level progress resetted."
#~ msgstr "關卡重設完成。"

#~ msgid "To reset level progress, use “/reset_progress yes”"
#~ msgstr "運行「/reset_progress yes」以重設關卡進度"
