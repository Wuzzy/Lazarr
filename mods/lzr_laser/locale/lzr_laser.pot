# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Lazarr! package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Lazarr! 2.0.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/lzr_laser/blocks.lua:22
msgid "Heavy Crate"
msgstr ""

#: mods/lzr_laser/blocks.lua:31
msgid "Old Crate"
msgstr ""

#: mods/lzr_laser/blocks.lua:42
msgid "Mossy Heavy Crate"
msgstr ""

#: mods/lzr_laser/blocks.lua:50
msgid "Light Crate"
msgstr ""

#: mods/lzr_laser/blocks.lua:51
msgid "Light enough to be picked up"
msgstr ""

#: mods/lzr_laser/blocks.lua:60
msgid "Mirror"
msgstr ""

#: mods/lzr_laser/blocks.lua:61
msgid "Deflects a laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:120
msgid "Beam Splitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:121
msgid "Both deflects a laser and lets it through"
msgstr ""

#. ~ Beam splitter block description with active lasers inside. @1 = a color code uniquely identifying the laser colors
#: mods/lzr_laser/blocks.lua:370 mods/lzr_laser/blocks.lua:381
#: mods/lzr_laser/blocks.lua:392
msgid "Beam Splitter (active, @1)"
msgstr ""

#: mods/lzr_laser/blocks.lua:414
msgid "Double Mirror"
msgstr ""

#: mods/lzr_laser/blocks.lua:415
msgid "Deflects lasers on both sides"
msgstr ""

#. ~ Double mirror block description with active lasers inside. @1 = a color code uniquely identifying the laser colors
#: mods/lzr_laser/blocks.lua:566 mods/lzr_laser/blocks.lua:577
#: mods/lzr_laser/blocks.lua:588
msgid "Double Mirror (active, @1)"
msgstr ""

#: mods/lzr_laser/blocks.lua:606
msgid "Crystal"
msgstr ""

#: mods/lzr_laser/blocks.lua:607
msgid "Deflects lasers in all directions"
msgstr ""

#. ~ A block that mixes laser colors
#: mods/lzr_laser/blocks.lua:658
msgid "Mixer"
msgstr ""

#: mods/lzr_laser/blocks.lua:659
msgid "Two lasers go in, a laser with their colors combined goes out"
msgstr ""

#: mods/lzr_laser/blocks.lua:718
msgid "Red Emitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:718
msgid "Emits a red laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:718
msgid "Would emit a red laser if it were turned on"
msgstr ""

#: mods/lzr_laser/blocks.lua:719
msgid "Green Emitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:719
msgid "Emits a green laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:719
msgid "Would emit a green laser if it were turned on"
msgstr ""

#: mods/lzr_laser/blocks.lua:720
msgid "Blue Emitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:720
msgid "Emits a blue laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:720
msgid "Would emit a blue laser if it were turned on"
msgstr ""

#: mods/lzr_laser/blocks.lua:721
msgid "Yellow Emitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:721
msgid "Emits a yellow laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:721
msgid "Would emit a yellow laser if it were turned on"
msgstr ""

#: mods/lzr_laser/blocks.lua:722
msgid "Magenta Emitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:722
msgid "Emits a magenta laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:722
msgid "Would emit a magenta laser if it were turned on"
msgstr ""

#: mods/lzr_laser/blocks.lua:723
msgid "Cyan Emitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:723
msgid "Emits a cyan laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:723
msgid "Would emit a cyan laser if it were turned on"
msgstr ""

#: mods/lzr_laser/blocks.lua:724
msgid "White Emitter"
msgstr ""

#: mods/lzr_laser/blocks.lua:724
msgid "Emits a white laser"
msgstr ""

#: mods/lzr_laser/blocks.lua:724
msgid "Would emit a white laser if it were turned on"
msgstr ""

#: mods/lzr_laser/blocks.lua:885
msgid "Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:885
msgid "Activates when a laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:888
msgid "Red Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:888
msgid "Activates when a red laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:889
msgid "Green Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:889
msgid "Activates when a green laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:890
msgid "Blue Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:890
msgid "Activates when a blue laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:891
msgid "Yellow Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:891
msgid "Activates when a yellow laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:892
msgid "Magenta Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:892
msgid "Activates when a magenta laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:893
msgid "Cyan Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:893
msgid "Activates when a cyan laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:894
msgid "White Detector"
msgstr ""

#: mods/lzr_laser/blocks.lua:894
msgid "Activates when a white laser goes into the hole"
msgstr ""

#: mods/lzr_laser/blocks.lua:1105
msgid "Hollow Barrel"
msgstr ""

#: mods/lzr_laser/blocks.lua:1161
msgid "Cursed Skull"
msgstr ""

#: mods/lzr_laser/blocks.lua:1162
msgid "Is untouchable unless a laser goes through it"
msgstr ""

#: mods/lzr_laser/blocks.lua:1163
msgid "Becomes untouchable when no laser goes through it"
msgstr ""

#: mods/lzr_laser/blocks.lua:1225
msgid "Shy Skull"
msgstr ""

#: mods/lzr_laser/blocks.lua:1226
msgid "Becomes untouchable when a laser goes through it"
msgstr ""

#: mods/lzr_laser/blocks.lua:1227
msgid "Is untouchable while a laser goes through it"
msgstr ""

#: mods/lzr_laser/blocks.lua:1287
msgid "Barricade"
msgstr ""

#: mods/lzr_laser/blocks.lua:1288
msgid "Burns on laser contact"
msgstr ""

#: mods/lzr_laser/blocks.lua:1302
msgid "Burning Barricade"
msgstr ""

#: mods/lzr_laser/blocks.lua:1303
msgid "Ignites neighboring barricades and burns up after 1 second"
msgstr ""

#: mods/lzr_laser/blocks.lua:1338 mods/lzr_laser/blocks.lua:1378
#: mods/lzr_laser/blocks.lua:1399
msgid "Bomb"
msgstr ""

#: mods/lzr_laser/blocks.lua:1340
msgid "Explodes when fuse gets ignited by laser or fire"
msgstr ""

#: mods/lzr_laser/blocks.lua:1341 mods/lzr_laser/blocks.lua:1355
msgid "Destroys cracked blocks in a 3×3×3 area"
msgstr ""

#: mods/lzr_laser/blocks.lua:1352
msgid "Ignited bomb"
msgstr ""

#: mods/lzr_laser/blocks.lua:1354
msgid "Will explode soon"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/blocks.lua:1378 mods/lzr_laser/blocks.lua:1384
#: mods/lzr_laser/elements.lua:358
msgid "@1 (iron screws)"
msgstr ""

#: mods/lzr_laser/blocks.lua:1384 mods/lzr_laser/blocks.lua:1405
msgid "Ignited Bomb"
msgstr ""

#: mods/lzr_laser/blocks.lua:1399
msgid "@1 (rotatable)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/blocks.lua:1405 mods/lzr_laser/elements.lua:526
msgid "@1 (copper screws)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/elements.lua:355
msgid "@1 (iron screws, inactive)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/elements.lua:363 mods/lzr_laser/elements.lua:619
msgid "@1 (inactive)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/elements.lua:437
msgid "@1 (iron screws, active)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/elements.lua:440 mods/lzr_laser/elements.lua:658
msgid "@1 (active)"
msgstr ""

#. ~ Annotation for a block @1. @2 is a color code uniquely identifying the active laser color in that block
#: mods/lzr_laser/elements.lua:484
msgid "@1 (iron screws, active, @2)"
msgstr ""

#. ~ Annotation for a block @1. @2 is a color code uniquely identifying the active laser color in that block
#. ~ Annotation for a block @1. @2 is a color core uniquely identifying this block's laser color
#: mods/lzr_laser/elements.lua:487 mods/lzr_laser/elements.lua:677
msgid "@1 (active, @2)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/elements.lua:523
msgid "@1 (copper screws, inactive)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/elements.lua:563
msgid "@1 (copper screws, active)"
msgstr ""

#. ~ Annotation for a block @1
#: mods/lzr_laser/elements.lua:582
msgid "@1 (copper screws, active, @2)"
msgstr ""

#. ~ Laser block description. @1 = color code uniquely identifying the color(s) of the laser beam(s) in that node
#: mods/lzr_laser/laser.lua:217
msgid "Laser (@1)"
msgstr ""

#. ~ Description of a block that is both laser and invisible barrier. @1 = color code uniquely identifying the color(s) of the laser beam(s) in that node
#: mods/lzr_laser/laser.lua:237
msgid "Barrier Laser (@1)"
msgstr ""

#. ~ Description of a block that is both laser and invisible barrier that lets rain through but not the player. @1 = color code uniquely identifying the color(s) of the laser beam(s) in that node
#: mods/lzr_laser/laser.lua:265
msgid "Rain Membrane Laser (@1)"
msgstr ""

#: mods/lzr_laser/physics.lua:1402
msgid ""
"Enable or disable frozen lasers. When lasers are frozen, they won’t be "
"updated automatically. Useful for debugging"
msgstr ""

#: mods/lzr_laser/physics.lua:1416
msgid "Lasers are now frozen. Map updates will no longer update the lasers."
msgstr ""

#: mods/lzr_laser/physics.lua:1419
msgid "Lasers are now unfrozen. Map updates will update the lasers again."
msgstr ""

#: mods/lzr_laser/physics.lua:1425
msgid "Force a full laser update to occur in the current level boundaries"
msgstr ""

#: mods/lzr_laser/physics.lua:1436
msgid "Emit lasers from all emitters in the current level boundaries"
msgstr ""

#: mods/lzr_laser/physics.lua:1438
msgid "[<max. iterations>]"
msgstr ""

#: mods/lzr_laser/physics.lua:1452
msgid ""
"Remove all lasers in the current level boundaries and the current out-of-"
"bounds lasers"
msgstr ""

#: mods/lzr_laser/tools.lua:15
msgid "Block State Toggler"
msgstr ""

#: mods/lzr_laser/tools.lua:16
msgid "Turns blocks on or off"
msgstr ""

#: mods/lzr_laser/tools.lua:26 mods/lzr_laser/tools.lua:51
#: mods/lzr_laser/tools.lua:75 mods/lzr_laser/tools.lua:111
#: mods/lzr_laser/tools.lua:155
msgid "This tool only works in the level editor or development mode."
msgstr ""

#: mods/lzr_laser/tools.lua:40
msgid "Color Changer"
msgstr ""

#: mods/lzr_laser/tools.lua:41
msgid "Changes block color"
msgstr ""

#: mods/lzr_laser/tools.lua:95
msgid "Screw Changer"
msgstr ""

#: mods/lzr_laser/tools.lua:96
msgid "Cycles through screw types for blocks"
msgstr ""

#: mods/lzr_laser/tools.lua:201
msgid "Sender Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:202
msgid "Punch node to select a sender to add receivers to in Receiver Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:206
msgid "Receiver Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:207
msgid ""
"Punch node to add a node as a receiver to the list of receivers of the "
"selected sender from Sender Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:211
msgid "Sender Type Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:212
msgid "Punch sender node to change its sender type"
msgstr ""

#: mods/lzr_laser/tools.lua:216
msgid "Receiver Type Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:217
msgid "Punch receiver node to change its receiver type"
msgstr ""

#: mods/lzr_laser/tools.lua:221
msgid "Reset Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:222
msgid "Punch sender or receiver to remove all its trigger information"
msgstr ""

#: mods/lzr_laser/tools.lua:226
msgid "Info Mode"
msgstr ""

#: mods/lzr_laser/tools.lua:227
msgid "Punch node to expose its current trigger relations"
msgstr ""

#. ~ Tool in level editor to change triggers
#: mods/lzr_laser/tools.lua:235 mods/lzr_laser/tools.lua:260
msgid "Trigger Tool"
msgstr ""

#: mods/lzr_laser/tools.lua:238
msgid "Place to change mode"
msgstr ""

#: mods/lzr_laser/tools.lua:271
msgid "This tool only works in the level editor."
msgstr ""

#: mods/lzr_laser/tools.lua:277
msgid "This node is outside the level area."
msgstr ""

#: mods/lzr_laser/tools.lua:283
msgid "This node is neither a sender nor a receiver."
msgstr ""

#: mods/lzr_laser/tools.lua:291 mods/lzr_laser/tools.lua:343
msgid "This node isn’t a sender."
msgstr ""

#: mods/lzr_laser/tools.lua:297 mods/lzr_laser/tools.lua:349
#: mods/lzr_laser/tools.lua:374 mods/lzr_laser/tools.lua:395
#: mods/lzr_laser/tools.lua:407
msgid "ERROR: This node wasn’t initialized as a trigger!"
msgstr ""

#: mods/lzr_laser/tools.lua:309
msgid "Now setting receivers for sender: @1"
msgstr ""

#: mods/lzr_laser/tools.lua:312 mods/lzr_laser/tools.lua:368
msgid "This node isn’t a receiver."
msgstr ""

#: mods/lzr_laser/tools.lua:319
msgid "Select a sender in Sender Mode first!"
msgstr ""

#: mods/lzr_laser/tools.lua:325
msgid "The selected sender no longer exists."
msgstr ""

#: mods/lzr_laser/tools.lua:336
msgid "Added signal from sender @1 to receiver @2."
msgstr ""

#: mods/lzr_laser/tools.lua:338
msgid ""
"This receiver was already added to the receiver list of the sender at @1."
msgstr ""

#. ~ @1: short sender type name, @2: long sender type description
#. ~ @1: short receiver type name, @2: long receiver type description
#: mods/lzr_laser/tools.lua:360 mods/lzr_laser/tools.lua:385
msgid "@1 (@2)"
msgstr ""

#: mods/lzr_laser/tools.lua:365
msgid "Sender type of sender @1 changed to @2."
msgstr ""

#: mods/lzr_laser/tools.lua:390
msgid "Receiver type of receiver @1 changed to @2."
msgstr ""

#. ~ Node was reset at @1.
#: mods/lzr_laser/tools.lua:402
msgid "Reset node at @1."
msgstr ""

#. ~ list separator
#: mods/lzr_laser/tools.lua:421 mods/lzr_laser/tools.lua:431
msgid ", "
msgstr ""

#: mods/lzr_laser/tools.lua:435
msgid "This node sends to: @1"
msgstr ""

#: mods/lzr_laser/tools.lua:438
msgid "This node receives from: @1"
msgstr ""

#: mods/lzr_laser/tools.lua:441
msgid ""
"This node is a sender and receiver. It neither sends nor receives signals."
msgstr ""

#: mods/lzr_laser/tools.lua:443
msgid "This node is a receiver. It does not receive signals."
msgstr ""

#: mods/lzr_laser/tools.lua:445
msgid "This node is a sender. It does not send signals."
msgstr ""

#. ~ Trigger tool was set to the new mode @1
#: mods/lzr_laser/tools.lua:462
msgid "Tool set to @1!"
msgstr ""
