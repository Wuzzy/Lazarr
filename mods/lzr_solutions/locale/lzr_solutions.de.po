msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain lzr_solutions x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-06 14:42+0100\n"
"PO-Revision-Date: 2025-01-06 15:03+0000\n"
"Last-Translator: Wuzzy <wuzzy@disroot.org>\n"
"Language-Team: German <https://translate.codeberg.org/projects/lazarr/"
"lzr_solutions/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.9.2\n"

#: mods/lzr_solutions/init.lua:454
msgid "Level pack solution test PASSED!"
msgstr "Levelpaketlösungstest BESTANDEN!"

#: mods/lzr_solutions/init.lua:473
msgid "Level solution test PASSED!"
msgstr "Levellösungstest BESTANDEN!"

#: mods/lzr_solutions/init.lua:505
msgid "Could not create and/or access world solutions path."
msgstr ""
"Weltlösungspfad konnte entweder nicht erstellt werden oder es konnte nicht "
"auf ihn zugegriffen werden."

#: mods/lzr_solutions/init.lua:522
msgid "Solution file written to: @1"
msgstr "Lösungsdatei geschrieben zu: @1"

#: mods/lzr_solutions/init.lua:524
msgid "Could not write solution file."
msgstr "Lösungsdatei konnte nicht geschrieben werden."

#: mods/lzr_solutions/init.lua:534
msgid "Recording finished."
msgstr "Aufzeichnung abgeschlossen."

#: mods/lzr_solutions/init.lua:540 mods/lzr_solutions/init.lua:658
msgid "Recording cancelled."
msgstr "Aufzeichnung abgebrochen."

#: mods/lzr_solutions/init.lua:557
msgid "Replay saved solution for current level, if one exists"
msgstr ""
"Gespeicherte Wiederholung für das aktuelle Level abspielen, falls sie "
"existiert"

#: mods/lzr_solutions/init.lua:560 mods/lzr_solutions/init.lua:604
#: mods/lzr_solutions/init.lua:634
msgid "Already replaying a solution!"
msgstr "Es wird bereits eine Lösung wiedergegeben!"

#: mods/lzr_solutions/init.lua:562 mods/lzr_solutions/init.lua:606
#: mods/lzr_solutions/init.lua:636
msgid "Already recording!"
msgstr "Es findet bereits eine Aufzeichnung statt!"

#: mods/lzr_solutions/init.lua:566 mods/lzr_solutions/init.lua:628
msgid "Not playing in a level!"
msgstr "Es wird gerade nicht in einem Level gespielt!"

#: mods/lzr_solutions/init.lua:574
msgid "No solution available."
msgstr "Keine Lösung verfügbar."

#: mods/lzr_solutions/init.lua:588
msgid "Replay started."
msgstr "Wiederholung gestartet."

#: mods/lzr_solutions/init.lua:590
msgid "CSV error in solution: @1."
msgstr "CSV-Fehler in Lösung: @1"

#: mods/lzr_solutions/init.lua:593
msgid "No solution file available."
msgstr "Keine Lösungsdatei verfügbar."

#: mods/lzr_solutions/init.lua:600
msgid "[<level pack>]"
msgstr "[<Levelpaket>]"

#: mods/lzr_solutions/init.lua:601
msgid "Test the solutions of all levels of a level pack"
msgstr "Die Lösungen aller Levels eines Levelpakets testen"

#: mods/lzr_solutions/init.lua:616
msgid "This level pack doesn’t exist!"
msgstr "Dieses Levelpaket existiert nicht!"

#: mods/lzr_solutions/init.lua:624
msgid ""
"Start or stop recording solution for current level, writing to a solution "
"file when stopping"
msgstr ""
"Aufzeichnung der Lösung für das aktuelle Level starten oder stoppen; beim "
"Stopp wird in eine Lösungsdatei geschrieben"

#: mods/lzr_solutions/init.lua:639
msgid "Recording started."
msgstr "Aufzeichnung gestartet."

#: mods/lzr_solutions/init.lua:643 mods/lzr_solutions/init.lua:655
msgid "Not recording!"
msgstr "Es läuft keine Aufzeichnung!"

#: mods/lzr_solutions/init.lua:648 mods/lzr_solutions/init.lua:650
msgid "Recording stopped."
msgstr "Aufzeichnung gestoppt."

#~ msgid "Core level solution test PASSED!"
#~ msgstr "Hauptlevellösungsstest BESTANDEN!"
