local S = minetest.get_translator("lzr_decor")

local pixelbox = function(size, boxes)
	local fixed = {}
	for _, box in ipairs(boxes) do
		-- `unpack` has been changed to `table.unpack` in newest Lua versions.
		local x, y, z, w, h, l = unpack(box)
		fixed[#fixed + 1] = {
			(x / size) - 0.5,
			(y / size) - 0.5,
			(z / size) - 0.5,
			((x + w) / size) - 0.5,
			((y + h) / size) - 0.5,
			((z + l) / size) - 0.5
		}
	end

	return {type = "fixed", fixed = fixed}
end

lzr_panes.register_pane("lzr_decor:bar", {
	description = S("Iron Bars"),
	textures = {"lzr_decor_bar.png", "lzr_decor_bar_top.png", "lzr_decor_bar_top.png"},
	inventory_image = "lzr_decor_bar.png",
	wield_image = "lzr_decor_bar.png",
	sounds = lzr_sounds.node_sound_metal_defaults(),
	element_group = "laser_element_bar",
})
lzr_panes.register_pane("lzr_decor:grate", {
	description = S("Iron Grate"),
	textures = {"lzr_decor_grate.png", "lzr_decor_bar_top.png", "lzr_decor_bar_top.png"},
	inventory_image = "lzr_decor_grate.png",
	wield_image = "lzr_decor_grate.png",
	sounds = lzr_sounds.node_sound_metal_defaults(),
	element_group = "laser_element_grate",
})
lzr_panes.register_pane("lzr_decor:rusty_bar", {
	description = S("Rusty Iron Bars"),
	sounds = lzr_sounds.node_sound_metal_defaults(),
	textures = {"lzr_decor_rusty_bar.png", "lzr_decor_rusty_bar_top.png", "lzr_decor_rusty_bar_top.png"},
	inventory_image = "lzr_decor_rusty_bar.png",
	wield_image = "lzr_decor_rusty_bar.png",
	element_group = "laser_element_rusty_bar",
})
lzr_panes.register_pane("lzr_decor:rusty_grate", {
	description = S("Rusty Iron Grate"),
	sounds = lzr_sounds.node_sound_metal_defaults(),
	textures = {"lzr_decor_rusty_grate.png", "lzr_decor_rusty_bar_top.png", "lzr_decor_rusty_bar_top.png"},
	inventory_image = "lzr_decor_rusty_grate.png",
	wield_image = "lzr_decor_rusty_grate.png",
	element_group = "laser_element_rusty_grate",
})
lzr_panes.register_pane("lzr_decor:wood_frame", {
	description = S("Wood Frame"),
	sounds = lzr_sounds.node_sound_wood_defaults(),
	textures = {"xdecor_wood_frame.png", "xdecor_wood_frame_top.png", "xdecor_wood_frame_top.png"},
	inventory_image = "xdecor_wood_frame.png",
	wield_image = "xdecor_wood_frame.png",
	element_group = "laser_element_wood_frame",
})
lzr_panes.register_pane("lzr_decor:woodframed_glass", {
	description = S("Wood-Framed Glass Pane"),
	textures = {
		"lzr_decor_woodframed_glass_pane.png",
		"lzr_decor_woodframed_glass_pane_top.png",
		"lzr_decor_woodframed_glass_pane_top.png",
	},
	inventory_image = "lzr_decor_woodframed_glass_pane.png",
	wield_image = "lzr_decor_woodframed_glass_pane.png",
	sounds = lzr_sounds.node_sound_glass_defaults(),
	element_group = "laser_element_woodframed_glass",
	groups = { glass = 1 },
})

minetest.register_node("lzr_decor:bonfire", {
	description = S("Bonfire"),
	walkable = false,
	drawtype = "plantlike",
	paramtype = "light",
	light_source = 11,
	inventory_image = "lzr_decor_bonfire.png^[verticalframe:7:0",
	wield_image = "lzr_decor_bonfire.png^[verticalframe:7:0",
	use_texture_alpha = "clip",
	selection_box = {
		type = "fixed",
		fixed = { -5/16, -0.5, -5/16, 5/16, 7/16, 5/16 },
	},
	tiles = {
		{ name = "lzr_decor_bonfire.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 2.0,
		},},
	},
	groups = {breakable = 1, laser_incompatible = 1},
})

minetest.register_node("lzr_decor:barrel", {
	description = S("Barrel"),
	tiles = {"xdecor_barrel_top.png", "xdecor_barrel_top.png", "xdecor_barrel_sides.png"},
	paramtype2 = "facedir",
	groups = {breakable = 1, rotatable = 3, barrel = 1},
	sounds = lzr_sounds.node_sound_wood_defaults()
})
-- Breaks at the slightest touch
minetest.register_node("lzr_decor:barrel_old", {
	description = S("Old Barrel"),
	tiles = {"lzr_decor_barrel_old_top.png", "lzr_decor_barrel_old_top.png", "lzr_decor_barrel_old_sides.png"},
	paramtype2 = "facedir",
	groups = {breakable = 1, rotatable = 3, punchdig = 1, explosion_destroys = 1, barrel = 1},
	sounds = lzr_sounds.node_sound_wood_squeak_defaults({
		dug = { name = "lzr_laser_crate_old_break", gain = 0.35 },
	}),
	drop = "",
})

local function register_storage(name, desc, def)
	minetest.register_node(name, {
		description = desc,
		tiles = def.tiles,
		drawtype = def.drawtype,
		paramtype = def.paramtype,
		paramtype2 = def.paramtype2,
		node_box = def.node_box,
		on_place = def.on_place,
		groups = def.groups or {breakable = 1, rotatable = 3, furniture = 1 },
		sounds = lzr_sounds.node_sound_wood_defaults(),
		use_texture_alpha = def.use_texture_alpha,
	})
end

register_storage("lzr_decor:cabinet", S("Wooden Cabinet"), {
	paramtype2 = "4dir",
	tiles = {
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_cabinet_sides.png", "xdecor_cabinet_front.png"
	},
})

lzr_laser.register_element("lzr_decor:cabinet_half", {
	description = S("Half Wooden Cabinet"),
	paramtype2 = "4dir",
	drawtype = "mesh",
	__mesh_off = "lzr_decor_cabinet_half.obj",
	__mesh_on = "lzr_decor_cabinet_half.obj",
	__tiles_off = {
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_half_cabinet_sides.png", "xdecor_half_cabinet_front.png",
		"blank.png",
	},
	__tiles_on = {
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_half_cabinet_sides.png", "xdecor_half_cabinet_front.png",
		lzr_laser.LASER_TILE,
	},

	__use_texture_alpha_off = "clip",
	__use_texture_alpha_on = lzr_laser.ALPHA_LASER,
	__light_source_on = lzr_globals.LASER_GLOW,

	paramtype = "light",
	is_ground_content = false,
	groups = { laser_block = 1, breakable = 1, rotatable = 3 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
	collision_box = {
		type = "fixed",
		-- The slab nodebox is slightly higher than half of a node
		-- so that an incoming laser from any direction (except the top)
		-- will be visually blocked.
		fixed = {-0.5, -1/16, -0.5, 0.5, 0.5, 0.5},
	},
	selection_box = {
		type = "fixed",
		fixed = {-0.5, -1/16, -0.5, 0.5, 0.5, 0.5},
	},
}, { group = "cabinet_half" } )

register_storage("lzr_decor:empty_shelf", S("Empty Shelf"), {
	paramtype2 = "4dir",
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^xdecor_empty_shelf.png",
	},
})

register_storage("lzr_decor:bookshelf", S("Bookshelf"), {
	paramtype2 = "4dir",
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^lzr_decor_bookshelf.png",
	},
})

register_storage("lzr_decor:vessels_shelf", S("Vessels Shelf"), {
	paramtype2 = "4dir",
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^lzr_decor_vessels_shelf.png",
	},
})

register_storage("lzr_decor:multishelf", S("Multi Shelf"), {
	paramtype2 = "4dir",
	tiles = {
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		{name="default_wood.png", align_style="world"},
		"default_wood.png^xdecor_multishelf.png",
	},
})

minetest.register_node("lzr_decor:candle", {
	description = S("Candle"),
	light_source = 12,
	drawtype = "torchlike",
	inventory_image = "xdecor_candle_inv.png",
	wield_image = "xdecor_candle_wield.png",
	paramtype2 = "wallmounted",
	walkable = false,
	groups = { breakable = 1, rotatable = 3, laser_incompatible = 1 },
	tiles = {
		{
			name = "xdecor_candle_floor.png",
			animation = {type="vertical_frames", length = 1.5}
		},
		{
			name = "xdecor_candle_hanging.png",
			animation = {type="vertical_frames", length = 1.5}
		},
		{
			name = "xdecor_candle_wall.png",
			animation = {type="vertical_frames", length = 1.5}
		}
	},
	selection_box = {
		type = "wallmounted",
		wall_top = {-0.25, -0.3, -0.25, 0.25, 0.5, 0.25},
		wall_bottom = {-0.25, -0.5, -0.25, 0.25, 0.1, 0.25},
		wall_side = {-0.5, -0.35, -0.15, -0.15, 0.4, 0.15}
	},
	sounds = lzr_sounds.node_sound_defaults(),
})

minetest.register_node("lzr_decor:chair", {
	description = S("Chair"),
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "4dir",
	tiles = {"xdecor_wood.png"},
	sounds = lzr_sounds.node_sound_wood_defaults(),
	groups = { breakable = 1, rotatable = 3, furniture = 1, laser_incompatible = 1 },
	node_box = pixelbox(16, {
		{3,  0, 11,   2, 16, 2},
		{11, 0, 11,   2, 16, 2},
		{5,  9, 11.5, 6,  6, 1},
		{3,  0,  3,   2,  6, 2},
		{11, 0,  3,   2,  6, 2},
		{3,  6,  3,  10,  2, 8}
	}),
})

lzr_laser.register_element("lzr_decor:ships_wheel", {
	description = S("Ship’s Wheel"),
	paramtype2 = "4dir",
	drawtype = "mesh",
	__mesh_off = "lzr_decor_ships_wheel.obj",
	__mesh_on = "lzr_decor_ships_wheel.obj",
	paramtype = "light",
	__light_source_on = lzr_globals.LASER_GLOW,
	__tiles_off = {"lzr_decor_ships_wheel_front.png", "lzr_decor_ships_wheel_side.png", "lzr_decor_ships_wheel_stick.png", "blank.png"},
	__tiles_on = {"lzr_decor_ships_wheel_front.png", "lzr_decor_ships_wheel_side.png", "lzr_decor_ships_wheel_stick.png", lzr_laser.LASER_TILE},

	sounds = lzr_sounds.node_sound_wood_defaults(),
	groups = { laser_block = 1, breakable = 1, rotatable = 3, furniture = 1 },
	is_ground_content = false,
	collision_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -1/16, 0.5, 0.5, 1/16 },
		},
	},
	selection_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -1/16, 0.5, 0.5, 1/16 },
		},
	},
	__use_texture_alpha_off = "clip",
	__use_texture_alpha_on = lzr_laser.ALPHA_LASER,
}, { group = "ships_wheel" })

--[[ Cobwebs restrict player movement by slowing them
down and disallowing jumps. Their full-cube selection
box also prevents pointing through.
But they can be destroyed by lasers.
While in a cobweb, the player MUST be unable to reach
any higher block except those within stepheight
difference (e.g. slabs)
Example uses for cobwebs:
* Enclose a chest (forcing player to remove them first with laser)
* Blocking a path to higher places
* Decoration, obviously ]]
minetest.register_node("lzr_decor:cobweb", {
	description = S("Cobweb"),
	drawtype = "plantlike",
	paramtype = "light",
	tiles = {"xdecor_cobweb.png"},
	inventory_image = "xdecor_cobweb.png",
	walkable = false,
	selection_box = {type = "regular"},
	-- This is mostly more for annoyance than
	move_resistance = 3,
	groups = { breakable = 1, laser_destroys = 1, disable_jump = 1 },
	sounds = lzr_sounds.node_sound_grass_defaults({footstep={}}),
})

minetest.register_node("lzr_decor:lantern", {
	description = S("Lantern"),
	light_source = 13,
	drawtype = "plantlike",
	inventory_image = "xdecor_lantern_inv.png",
	wield_image = "xdecor_lantern_inv.png",
	walkable = false,
	groups = { breakable = 1, laser_incompatible = 1 },
	tiles = {
		{
			name = "xdecor_lantern.png",
			animation = {type="vertical_frames", length = 1.5}
		}
	},
	selection_box = pixelbox(16, {{4, 0, 4, 8, 16, 8}}),
	sounds = lzr_sounds.node_sound_defaults(),
})

-- Light boxes
-- Decorative blocks that glow. They come in an "on" and "off" state.
local lightboxes = {
	-- Table values (in this order): description (on), description (off), tile (on), tile (off), light level
	iron_lightbox = { S("Iron Light Box"), S("Iron Light Box (off)"), "xdecor_iron_lightbox.png", "lzr_decor_iron_lightbox_off.png", 13 },
	wooden2_lightbox = { S("Wooden Light Box"), S("Wooden Light Box (off)"), "xdecor_wooden2_lightbox.png", "lzr_decor_wooden2_lightbox_off.png", 13 },
	ship_lightbox = { S("Ship Light Box"), S("Ship Light Box (off)"), "lzr_decor_ship_lightbox.png", "lzr_decor_ship_lightbox_off.png", 13 },
	ocean_lantern = { S("Ocean Lantern"), S("Ocean Lantern (off)"), "xocean_lantern.png", "lzr_decor_xocean_lantern_off.png", minetest.LIGHT_MAX },
}

for l, data in pairs(lightboxes) do
	-- Light box (turned on)
	minetest.register_node("lzr_decor:" .. l, {
		description = data[1],
		paramtype = "light",
		sunlight_propagates = true,
		tiles = {data[3]},
		groups = { breakable = 1, lightbox = 2, receiver = 1 },
		light_source = data[5],
		sounds = lzr_sounds.node_sound_glass_defaults(),
		after_place_node = lzr_laser.trigger_after_place_node,
		after_dig_node = lzr_laser.trigger_after_dig_node,
		_lzr_on_toggle = function(pos)
			-- Turn off
			minetest.swap_node(pos, {name="lzr_decor:" .. l .. "_off"})
			minetest.sound_play({name="lzr_decor_lightbox_off", gain=0.5}, {pos = pos}, true)
		end,
	})
	-- Light box (turned off)
	minetest.register_node("lzr_decor:" .. l .. "_off", {
		description = data[2],
		paramtype = "light",
		sunlight_propagates = true,
		tiles = {data[4]},
		groups = { breakable = 1, lightbox = 1, receiver = 1 },
		sounds = lzr_sounds.node_sound_glass_defaults(),
		after_place_node = lzr_laser.trigger_after_place_node,
		after_dig_node = lzr_laser.trigger_after_dig_node,
		_lzr_on_toggle = function(pos)
			-- Turn on
			minetest.swap_node(pos, {name="lzr_decor:" .. l})
			minetest.sound_play({name="lzr_decor_lightbox_on", gain=0.5}, {pos = pos}, true)
		end,
	})

end

local xdecor_potted = {
	dandelion_white = S("Potted White Dandelion"),
	dandelion_yellow = S("Potted Yellow Dandelion"),
	geranium = S("Potted Geranium"),
	rose = S("Potted Rose"),
	tulip = S("Potted Tulip"),
	viola = S("Potted Viola"),
}

for f, desc in pairs(xdecor_potted) do
	minetest.register_node("lzr_decor:potted_" .. f, {
		description = desc,
		walkable = false,
		groups = { breakable = 1, potted_flower = 1, laser_incompatible = 1 },
		tiles = {"xdecor_" .. f .. "_pot.png"},
		inventory_image = "xdecor_" .. f .. "_pot.png",
		drawtype = "plantlike",
		paramtype = "light",
		selection_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.5, 0.5, -0.2, 0.5 },
		},
		sounds = lzr_sounds.node_sound_defaults(),
	})
end

local function register_hard_node(name, desc, def)
	def = def or {}
	minetest.register_node(name, {
		description = desc,
		tiles = {"xdecor_" .. name .. ".png"},
		groups = def.groups or { breakable = 1},
		sounds = def.sounds or lzr_sounds.node_sound_stone_defaults()
	})
end

minetest.register_node("lzr_decor:table", {
	description = S("Table"),
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	tiles = {"xdecor_wood.png"},
	groups = { breakable = 1, rotatable = 3, furniture = 1, laser_incompatible = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
	node_box = pixelbox(16, {
		{0, 14, 0, 16, 2, 16}, {5.5, 0, 5.5, 5, 14, 6}
	})
})

-- This node should not be used in levels. For windows, use the glass pane.
minetest.register_node("lzr_decor:woodframed_glass", {
	description = S("Wood Framed Glass"),
	paramtype = "light",
	drawtype = "glasslike_framed",
	sunlight_propagates = true,
	tiles = {"xdecor_woodframed_glass.png", "xdecor_woodframed_glass_detail.png"},
	groups = { breakable = 1, laser_incompatible = 1, glass = 1, not_in_creative_inventory = 1 },
	sounds = lzr_sounds.node_sound_glass_defaults()
})

minetest.register_node("lzr_decor:ocean_stone", {
	description = S("Ocean Stone"),
	tiles = {"xocean_stone.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_stone_cracked", {
	description = S("Cracked Ocean Stone"),
	tiles = {"lzr_decor_ocean_stone_cracked.png"},
	groups = { breakable = 1, stone = 1, cracked = 1, explosion_destroys = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_decor:ocean_stone_block", {
	description = S("Ocean Stone Block"),
	tiles = {"lzr_decor_ocean_stone_block.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_cobble", {
	description = S("Ocean Cobblestone"),
	tiles = {"xocean_cobble.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_carved", {
	description = S("Carved Ocean Stone"),
	tiles = {"xocean_carved.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_circular", {
	description = S("Circular Ocean Stone"),
	tiles = {"xocean_circular.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:ocean_bricks", {
	description = S("Ocean Bricks"),
	tiles = {"xocean_brick.png"},
	groups = { breakable = 1, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})

minetest.register_node("lzr_decor:ocean_pillar", {
	description = S("Ocean Pillar"),
	paramtype2 = "facedir",
	tiles = {
		"lzr_decor_ocean_pillar_top.png",
		"lzr_decor_ocean_pillar_top.png^[transformFY",
		"lzr_decor_ocean_pillar.png^[transformFX",
		"lzr_decor_ocean_pillar.png",
		"lzr_decor_ocean_pillar.png^[transformFX",
		"lzr_decor_ocean_pillar.png",
	},
	groups = { breakable = 1, rotatable = 3, stone = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults(),
})
minetest.register_node("lzr_decor:thatch", {
	description = S("Thatch"),
	tiles = {"dryplants_thatch.png"},
	is_ground_content = false,
	groups = { breakable = 1, thatch = 1 },
	sounds = lzr_sounds.node_sound_leaves_defaults(),
})

minetest.register_node("lzr_decor:thatch_wet", {
	description = S("Wet Thatch"),
	tiles = {"dryplants_thatch_wet.png"},
	is_ground_content = false,
	groups = { breakable = 1, thatch = 1 },
	sounds = lzr_sounds.node_sound_leaves_defaults(),
})

minetest.register_node("lzr_decor:cloth_white", {
	description = S("White Cloth"),
	tiles = {"wool_white.png"},
	is_ground_content = false,
	groups = { breakable = 1, cloth = 1 },
	sounds = lzr_sounds.node_sound_cloth_defaults(),
	_lzr_next_color = "lzr_decor:cloth_black",
	_lzr_prev_color = "lzr_decor:cloth_black",
})

minetest.register_node("lzr_decor:cloth_black", {
	description = S("Black Cloth"),
	tiles = {"wool_black.png"},
	is_ground_content = false,
	groups = { breakable = 1, cloth = 1 },
	sounds = lzr_sounds.node_sound_cloth_defaults(),
	_lzr_next_color = "lzr_decor:cloth_white",
	_lzr_prev_color = "lzr_decor:cloth_white",
})

minetest.register_node("lzr_decor:forge", {
	description = S("Forge"),
	tiles = {
		"lzr_decor_forge_off_top.png",
		"lzr_decor_forge_bottom.png",
		"lzr_decor_forge_side.png",
	},
	is_ground_content = false,
	groups = { breakable = 1, furniture = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults({
		footstep = { name = "lzr_decor_footstep_forge", gain = 0.9 },
	}),
})
minetest.register_node("lzr_decor:forge_lit", {
	description = S("Forge (lit)"),
	tiles = {
		{ name = "lzr_decor_forge_top_anim.png", animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 4 }},
		"lzr_decor_forge_bottom.png",
		"lzr_decor_forge_side.png"
	},
	light_source = 5,
	is_ground_content = false,
	groups = { breakable = 1, furniture = 1 },
	sounds = lzr_sounds.node_sound_stone_defaults({
		footstep = { name = "lzr_decor_footstep_forge", gain = 0.9 },
	}),
})

minetest.register_node("lzr_decor:cauldron_water", {
	description = S("Cauldron with Water"),
	tiles = {"xdecor_cauldron_top_idle.png", "xdecor_cauldron_bottom.png", "xdecor_cauldron_side.png"},
	is_ground_content = false,
	groups = { breakable = 1, furniture = 1 },
	sounds = lzr_sounds.node_sound_metal_defaults(),
})

minetest.register_node("lzr_decor:cauldron", {
	description = S("Cauldron"),
	tiles = {"xdecor_cauldron_top_empty.png", "xdecor_cauldron_bottom.png", "xdecor_cauldron_side.png"},
	is_ground_content = false,
	groups = { breakable = 1, furniture = 1 },
	sounds = lzr_sounds.node_sound_metal_defaults(),
})

minetest.register_node("lzr_decor:blanket_table", {
	description = S("Table with Blanket"),
	tiles = {"lzr_decor_blanket_table_top.png", "lzr_decor_blanket_table_bottom.png", "lzr_decor_blanket_table_side.png"},
	is_ground_content = false,
	groups = { breakable = 1, furniture = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})
minetest.register_node("lzr_decor:working_table", {
	description = S("Working Table"),
	tiles = {"lzr_decor_working_table_top.png", "lzr_decor_working_table_bottom.png", "lzr_decor_working_table_side.png"},
	is_ground_content = false,
	groups = { breakable = 1, furniture = 1 },
	sounds = lzr_sounds.node_sound_wood_defaults(),
})






minetest.register_alias("lzr_panes:bar_flat", "lzr_decor:bar_flat_fixed")
minetest.register_alias("lzr_panes:grate_flat", "lzr_decor:grate_flat_fixed")
minetest.register_alias("lzr_panes:rusty_bar_flat", "lzr_decor:rusty_bar_flat_fixed")
minetest.register_alias("lzr_panes:rusty_grate_flat", "lzr_decor:rusty_grate_flat_fixed")
minetest.register_alias("lzr_panes:wood_frame_flat", "lzr_decor:wood_frame_flat_fixed")

minetest.register_alias("lzr_panes:bar_flat_on", "lzr_decor:bar_flat_fixed_on_1")
minetest.register_alias("lzr_panes:grate_flat_on", "lzr_decor:grate_flat_fixed_on_1")
minetest.register_alias("lzr_panes:rusty_bar_flat_on", "lzr_decor:rusty_bar_flat_fixed_on_1")
minetest.register_alias("lzr_panes:rusty_grate_flat_on", "lzr_decor:rusty_grate_flat_fixed_on_1")
minetest.register_alias("lzr_panes:wood_frame_flat_on", "lzr_decor:wood_frame_flat_fixed_on_1")

minetest.register_alias("lzr_decor:cabinet_half", "lzr_decor:cabinet_half_fixed")
