-- This file registers all music files in the game.
-- For full credits, see CREDITS.md in the top
-- directory of Lazarr!

-- Track titles are intentionally kept untranslated.

-- Ocean music pack by Leonardo Paz
-- CC BY 4.0
-- https://opengameart.org/content/ocean-music-pack
lzr_ambience.register_ambience("exploring", "lzr_ambience_leopaz_exploring_the_ocean", 0.2, "Exploring the Ocean")
lzr_ambience.register_ambience("village", "lzr_ambience_leopaz_seaside_village", 0.2, "Seaside Village")
lzr_ambience.register_ambience("sirens", "lzr_ambience_leopaz_sirens_call", 0.2, "Siren's Call")
lzr_ambience.register_ambience("stranded", "lzr_ambience_leopaz_stranded_in_the_ocean", 0.3, "Stranded in the Ocean")

-- Tracks by Vandalorum (vandalorum.bandcamp.com)
-- CC BY 4.0
-- https://opengameart.org/content/water-level-synth-music
lzr_ambience.register_ambience("nautilus", "lzr_ambience_vandalorum_a_friendly_nautilus_shows_us_the_way", 0.2, "A Friendly Nautilus Shows us the Way")
lzr_ambience.register_ambience("mist", "lzr_ambience_vandalorum_a_mist_hangs_over_the_water", 0.2, "A Mist Hangs Over the Water")
lzr_ambience.register_ambience("ruins", "lzr_ambience_vandalorum_ruins_of_the_ancient_atlantians", 0.2, "Ruins of the Ancient Atlantians")
lzr_ambience.register_ambience("tide", "lzr_ambience_vandalorum_swept_beneath_the_tide", 0.2, "Swept Beneath the Tide")
lzr_ambience.register_ambience("storm", "lzr_ambience_vandalorum_the_cold_wizard_summons_a_storm", 0.2, "The Cold Wizard Summons a Storm")

-- Tracks by Kevin MacLeod (incompetech.com)
-- CC BY 4.0
lzr_ambience.register_ambience("mystery", "lzr_ambience_kml_ancient_mystery_waltz_allegro", 0.2, "Ancient Mystery Waltz Allegro")
lzr_ambience.register_ambience("marimba", "lzr_ambience_kml_ave_marimba", 0.2, "Ave Marimba")
lzr_ambience.register_ambience("island", "lzr_ambience_kml_island_meet_and_greet", 0.1, "Island Meet and Greet")
lzr_ambience.register_ambience("moonlight", "lzr_ambience_kml_moonlight_beach", 0.1, "Moonlight Beach")
lzr_ambience.register_ambience("morgana", "lzr_ambience_kml_morgana_rides", 0.45, "Morgana Rides")
lzr_ambience.register_ambience("river", "lzr_ambience_kml_river_fire", 0.25, "River Fire")
lzr_ambience.register_ambience("tiki", "lzr_ambience_kml_tiki_bar_mixer", 0.1, "Tiki Bar Mixer")

-- Legacy ambience names
lzr_ambience.register_ambience_alias("ocean", "village")
lzr_ambience.register_ambience_alias("temple", "river")
