# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Lazarr! package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Lazarr! 2.0.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2024-12-10 03:16+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_Hant\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. ~ Tool that digs blocks
#: mods/lzr_tools/init.lua:13
msgid "Ultra Pickaxe"
msgstr ""

#: mods/lzr_tools/init.lua:14
msgid "Removes blocks except liquids"
msgstr ""

#. ~ Tool that digs liquids
#: mods/lzr_tools/init.lua:46
msgid "Ultra Bucket"
msgstr ""

#: mods/lzr_tools/init.lua:47
msgid "Removes liquids"
msgstr ""

#. ~ Hidden tool that digs lasers
#: mods/lzr_tools/init.lua:68
msgid "Laser Absorber"
msgstr ""

#: mods/lzr_tools/init.lua:69
msgid "Removes lasers"
msgstr ""

#. ~ Message shown when the 'laser stepper' debug tool was used to simulate the laser travel algorithm with a set number of iterations
#: mods/lzr_tools/init.lua:128
msgid "Emitted lasers with @1 iteration."
msgid_plural "Emitted lasers with @1 iterations."
msgstr[0] ""
msgstr[1] ""

#. ~ Hidden debug tool to test the laser travel algorithm.
#: mods/lzr_tools/init.lua:135
msgid "Laser Stepper"
msgstr ""

#: mods/lzr_tools/init.lua:136
msgid "Simulates the laser travel algorithm up to a given number of iterations"
msgstr ""

#: mods/lzr_tools/init.lua:137
msgid "Punch: Increase laser iterations by 1"
msgstr ""

#: mods/lzr_tools/init.lua:138
msgid "Place: Decrease laser iterations by 1"
msgstr ""

#: mods/lzr_tools/init.lua:139
msgid "Place/Punch + Sneak: Multiply iterations change by 10"
msgstr ""

#: mods/lzr_tools/init.lua:234
msgid "This tool only works in the level editor or development mode."
msgstr ""

#: mods/lzr_tools/init.lua:281
msgid "Block Variant Changer"
msgstr ""

#: mods/lzr_tools/init.lua:282
msgid "Changes a block to different variant"
msgstr ""
