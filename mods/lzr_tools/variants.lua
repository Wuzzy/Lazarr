-- Register variant cycles for the variant changer.

-- RULES:
-- * Only nodes that are similar should be grouped together
-- * All nodes in a cycle MUST be functionally equivalent,
--   e.g. a cycle with the different mirror types is not OK
-- * Colors don't count as variants. That's what the color
--   changer tool is for

lzr_tools.register_variant_cycle({
	"lzr_core:sandstone",
	"lzr_core:sandstone_block",
	"lzr_core:sandstone_brick",
	"lzr_core:island_stone",
	"lzr_core:cave_stone",
	"lzr_core:cave_stone_block",
	"lzr_core:cave_stone_block_mossy",
	"lzr_core:cave_stone_brick_circular",
	"lzr_core:cave_stone_brick_circular_mossy",
	"lzr_core:cave_stone_brick",
	"lzr_core:cave_stone_brick_mossy",
	"lzr_core:stone",
	"lzr_core:stone_block",
	"lzr_core:stone_block_mossy",
	"lzr_core:stone_brick_circular",
	"lzr_core:stone_brick_circular_mossy",
	"lzr_core:stone_brick",
	"lzr_core:stone_brick_mossy",
	"lzr_decor:ocean_stone",
	"lzr_decor:ocean_stone_block",
	"lzr_decor:ocean_carved",
	"lzr_decor:ocean_cobble",
	"lzr_decor:ocean_bricks",
	"lzr_decor:ocean_circular",
})

lzr_tools.register_variant_cycle({
	"lzr_core:cave_stone_pillar",
	"lzr_core:stone_pillar",
	"lzr_decor:ocean_pillar",
})

lzr_tools.register_variant_cycle({
	"lzr_core:stone_cracked",
	"lzr_core:cave_stone_cracked",
	"lzr_core:island_stone_cracked",
	"lzr_core:sandstone_cracked",
	"lzr_decor:ocean_stone_cracked",
})
lzr_tools.register_variant_cycle({
	"lzr_core:dirt",
	"lzr_core:dirt_with_grass",
	"lzr_core:dirt_with_jungle_litter",
})
lzr_tools.register_variant_cycle({
	"lzr_core:tree",
	"lzr_core:palm_tree",
	"lzr_core:coconut_tree",
})
lzr_tools.register_variant_cycle({
	"lzr_core:wood",
	"lzr_core:wood_mossy",
	"lzr_core:wood_loose",
	"lzr_core:palm_wood",
	"lzr_core:coconut_wood",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:blanket_table",
	"lzr_decor:working_table",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:cauldron",
	"lzr_decor:cauldron_water",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:forge",
	"lzr_decor:forge_lit",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:cabinet",
	"lzr_decor:cabinet_half",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:potted_dandelion_white",
	"lzr_decor:potted_dandelion_yellow",
	"lzr_decor:potted_tulip",
	"lzr_decor:potted_rose",
	"lzr_decor:potted_geranium",
	"lzr_decor:potted_viola",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:empty_shelf",
	"lzr_decor:bookshelf",
	"lzr_decor:vessels_shelf",
	"lzr_decor:multishelf",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:thatch",
	"lzr_decor:thatch_wet",
})
lzr_tools.register_variant_cycle({
	"lzr_laser:crate",
	"lzr_laser:crate_mossy",
})
lzr_tools.register_variant_cycle({
	"lzr_plants:island_grass",
	"lzr_plants:crab_grass",
	"lzr_plants:ant_grass_small",
	"lzr_plants:ant_grass_large",
})
lzr_tools.register_variant_cycle({
	"lzr_plants:seaweed",
	"lzr_plants:seaweed_small",
})

-- Note: Does not support laser variant
lzr_tools.register_variant_cycle({
	"lzr_core:bright_palm_leaves_fixed",
	"lzr_core:dark_palm_leaves_fixed",
})

-- Register stair/slab cycles
local stair_slab_cycles = {
	{
		"island_stone",
		"sandstone",
		"cave_stone",
		"cave_stone_block",
		"cave_stone_block_mossy",
		"stone",
		"stone_block",
		"stone_block_mossy",
		"ocean_stone",
		"ocean_cobble",
		"ocean_bricks",
	},
	{
		"thatch",
		"thatch_wet",
	},
	{
		"wood",
		"wood_mossy",
		"palm_wood",
		"coconut_wood",
	},
	{
		"dirt",
		"dirt_with_grass",
		"dirt_with_jungle_litter",
	},
}

for c=1, #stair_slab_cycles do
	local template = stair_slab_cycles[c]
	local stair_cycle = {}
	local inner_stair_cycle = {}
	local outer_stair_cycle = {}
	local slab_cycle = {}
	local top_slab_cycle = {}
	for t=1, #template do
		table.insert(stair_cycle, "lzr_stairs:stair_"..template[t])
		table.insert(inner_stair_cycle, "lzr_stairs:stair_inner_"..template[t])
		table.insert(outer_stair_cycle, "lzr_stairs:stair_outer_"..template[t])
		-- Note: Laser variants of slab are not added
		table.insert(slab_cycle, "lzr_stairs:slab_"..template[t].."_fixed")
		table.insert(top_slab_cycle, "lzr_stairs:slab_top_"..template[t].."_fixed")
	end
	lzr_tools.register_variant_cycle(stair_cycle)
	lzr_tools.register_variant_cycle(inner_stair_cycle)
	lzr_tools.register_variant_cycle(outer_stair_cycle)
	lzr_tools.register_variant_cycle(slab_cycle)
	lzr_tools.register_variant_cycle(top_slab_cycle)
end

-- Double slabs
lzr_tools.register_variant_cycle({
	"lzr_stairs:double_slab_stone_block",
	"lzr_stairs:double_slab_stone_block_mossy",
	"lzr_stairs:double_slab_cave_stone_block",
	"lzr_stairs:double_slab_cave_stone_block_mossy",
})
lzr_tools.register_variant_cycle({
	"lzr_stairs:double_slab_reverse_stone_block",
	"lzr_stairs:double_slab_reverse_stone_block_mossy",
	"lzr_stairs:double_slab_reverse_cave_stone_block",
	"lzr_stairs:double_slab_reverse_cave_stone_block_mossy",
})

-- Panes
lzr_tools.register_variant_cycle({
	"lzr_decor:bar_flat_fixed",
	"lzr_decor:grate_flat_fixed",
	"lzr_decor:rusty_bar_flat_fixed",
	"lzr_decor:rusty_grate_flat_fixed",
})

-- Light boxes
lzr_tools.register_variant_cycle({
	"lzr_decor:wooden2_lightbox",
	"lzr_decor:ship_lightbox",
	"lzr_decor:iron_lightbox",
	"lzr_decor:ocean_lantern",
})
lzr_tools.register_variant_cycle({
	"lzr_decor:wooden2_lightbox_off",
	"lzr_decor:ship_lightbox_off",
	"lzr_decor:iron_lightbox_off",
	"lzr_decor:ocean_lantern_off",
})
