local S = minetest.get_translator("lzr_incomplete_message")

minetest.register_on_joinplayer(function(player)
	minetest.chat_send_player(player:get_player_name(), minetest.colorize("#FF00FF", S("This game is incomplete!")))
end)
