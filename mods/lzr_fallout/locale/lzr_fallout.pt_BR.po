msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain lzr_fallout x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#. ~ Message when you got stuck inside skull blocks. You may be creative in the translation
#: mods/lzr_fallout/init.lua:78
msgid "You were skull-crushed!"
msgstr ""

#. ~ Message when you got stuck inside solid blocks other than skulls. You may be creative in the translation
#: mods/lzr_fallout/init.lua:84
msgid "You were between a rock and a hard place."
msgstr ""

#. ~ Message shown when the player is about to leave the level. @1 = countdown counting down the seconds
#: mods/lzr_fallout/init.lua:197
msgid "Leaving in @1 …"
msgstr ""
