local S = minetest.get_translator("lzr_tt")

tt.register_snippet(function(itemstring)
	if minetest.get_item_group(itemstring, "disable_jump") == 1 then
		return S("Cannot jump in it")
	end
end)

tt.register_snippet(function(itemstring)
	if minetest.get_item_group(itemstring, "punchdig") == 1 then
		return S("Destroyed on touch")
	end
end)

tt.register_snippet(function(itemstring)
	if minetest.get_item_group(itemstring, "explosion_destroys") == 1 then
		return S("Destroyed by explosions")
	end
end)

tt.register_snippet(function(itemstring)
	if minetest.get_item_group(itemstring, "laser_destroys") == 1 then
		return S("Destroyed by lasers")
	end
end)

