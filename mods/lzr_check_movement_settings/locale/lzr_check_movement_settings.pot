# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Lazarr! package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Lazarr! 2.0.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/lzr_check_movement_settings/init.lua:74
msgid ""
"WARNING: The player movement settings are not at the recommended values for "
"Lazarr! The physics might not work as intended!"
msgstr ""

#: mods/lzr_check_movement_settings/init.lua:75
msgid ""
"Please exit the game and reset the following Luanti settings to their "
"default value:"
msgstr ""

#. ~ list separator for list of invalid player movement settings
#: mods/lzr_check_movement_settings/init.lua:77
msgid ", "
msgstr ""

#: mods/lzr_check_movement_settings/init.lua:81
msgid ""
"You quit. Remember, Lazarr! expects the following Luanti settings to be "
"reset to the default value: @1"
msgstr ""

#: mods/lzr_check_movement_settings/init.lua:87
msgid "Exit game"
msgstr ""

#: mods/lzr_check_movement_settings/init.lua:88
msgid "Continue playing anyway"
msgstr ""
