# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Lazarr! package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Lazarr! 2.0.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/lzr_treasure/init.lua:20
msgid "Gold Block"
msgstr ""

#: mods/lzr_treasure/init.lua:174 mods/lzr_treasure/init.lua:175
msgid "Contains a gold block"
msgstr ""

#: mods/lzr_treasure/init.lua:189
msgid "Gets locked when triggered off"
msgstr ""

#: mods/lzr_treasure/init.lua:190
msgid "Lock breaks when triggered on, but it re-appears when triggered off"
msgstr ""

#: mods/lzr_treasure/init.lua:194
msgid "Lock breaks when triggered on"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:459
msgid "Wooden Chest"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:461
msgid "Locked Wooden Chest"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:463
msgid "Open Wooden Chest"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:465
msgid "Open Wooden Chest with Laser"
msgstr ""

#. ~ Block description. @1 = a treasure
#: mods/lzr_treasure/init.lua:467
msgid "Open Wooden Chest with @1"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:478
msgid "Onyx Chest"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:480
msgid "Locked Onyx Chest"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:482
msgid "Open Onyx Chest"
msgstr ""

#. ~ Block description
#: mods/lzr_treasure/init.lua:484
msgid "Open Onyx Chest with Laser"
msgstr ""

#. ~ Block description. @1 = a treasure
#: mods/lzr_treasure/init.lua:486
msgid "Open Onyx Chest with @1"
msgstr ""
