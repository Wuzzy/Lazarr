msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain lzr_mapgen x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2024-12-10 03:16+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/lzr_mapgen/init.lua:9
msgid "Ocean"
msgstr "Océano"

#: mods/lzr_mapgen/init.lua:10
msgid "Islands"
msgstr "Islas"

#: mods/lzr_mapgen/init.lua:11
msgid "Underground"
msgstr "Bajo tierra"

#: mods/lzr_mapgen/init.lua:12
msgid "Sky"
msgstr "Cielo"
