lzr_mapgen = {}

local S = minetest.get_translator("lzr_mapgen")

local backdrops = {
	"ocean", "islands", "underground", "sky"
}
local backdrop_descriptions = {
	["ocean"] = S("Ocean"),
	["islands"] = S("Islands"),
	["underground"] = S("Underground"),
	["sky"] = S("Sky"),
}

lzr_mapgen.get_backdrops = function()
	return backdrops
end

lzr_mapgen.get_backdrop_description = function(id)
	return backdrop_descriptions[id]
end

dofile(minetest.get_modpath("lzr_mapgen").."/settings.lua")
dofile(minetest.get_modpath("lzr_mapgen").."/aliases.lua")
dofile(minetest.get_modpath("lzr_mapgen").."/decorations.lua")

-- The mapgen script will both run in the global environment
-- and the mapgen environment. See mapgen.lua to learn why.
dofile(minetest.get_modpath("lzr_mapgen").."/mapgen.lua")
minetest.register_mapgen_script(minetest.get_modpath("lzr_mapgen").."/mapgen.lua")
