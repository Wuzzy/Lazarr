-- Landscape
--[[
minetest.register_alias("islands:water_source", "lzr_core:water_source")
minetest.register_alias("islands:stone", "lzr_core:stone")
minetest.register_alias("islands:dirt", "lzr_core:dirt")
minetest.register_alias("islands:dirt_with_grass_palm", "lzr_core:dirt_with_grass")
minetest.register_alias("islands:dirt_with_palm_litter", "lzr_core:dirt_with_jungle_litter")
minetest.register_alias("islands:sand", "lzr_core:sand")
minetest.register_alias("islands:seabed", "lzr_core:seabed")
]]

-- Plants and trees
minetest.register_alias("islands:grass", "lzr_plants:island_grass")
minetest.register_alias("islands:cotton", "lzr_plants:cotton")
minetest.register_alias("islands:palm_leaves", "lzr_core:bright_palm_leaves_fixed")
minetest.register_alias("islands:palm_leaves2", "lzr_core:dark_palm_leaves_fixed")
minetest.register_alias("islands:palm_tree", "lzr_core:palm_tree")

-- Unused nodes
minetest.register_alias("islands:twigs", "air")
minetest.register_alias("islands:leaves", "air")
minetest.register_alias("islands:underbrush", "air")
minetest.register_alias("islands:palm_small_bottom", "air")
minetest.register_alias("islands:palm_small_top", "air")

