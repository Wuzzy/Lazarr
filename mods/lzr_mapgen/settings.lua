-- Mapgen settings


-- Set singlenode mapgen
minetest.set_mapgen_setting('mg_name', 'singlenode', true)

-- Disable the engine lighting calculation since that will be done for a
-- mapchunk of air nodes and will be incorrect after we place nodes.
minetest.set_mapgen_setting('flags', 'nolight', true)

-- Set a fixed seed so the islands backdrop used by levels
-- will stay consistent for all players
minetest.set_mapgen_setting('seed', '5436146057422610855', true)

minetest.set_mapgen_setting("mg_biome_np_heat", 
	"90,0,(1000,1000,1000),5349,3,0.5,2",
	true)

minetest.set_mapgen_setting("mg_biome_np_humidity", 
	"54,10,(1000,1000,1000),5349,3,0.5,2",
	true)


