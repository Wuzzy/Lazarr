minetest.clear_registered_decorations()

local mpath = minetest.get_modpath("lzr_mapgen")

minetest.register_decoration({
	name = "islands:palm_tree_tall",
	deco_type = "schematic",
	place_on = {"lzr_core:dirt_with_jungle_litter"},
	sidelen = 16,
	noise_params = {
		offset = 0.001,
		scale = 0.001,
		spread = {x = 64, y = 64, z = 64},
		seed = 2,
		octaves = 2,
		persist = 0.66
	},
	place_offset_y = 1,
	y_max = 100,
	y_min = 3,
	schematic = mpath .. "/schematics/palm_tree_big.mts",
	flags = "place_center_x, place_center_z",
})

minetest.register_decoration({
	name = "islands:palm_tree",
	deco_type = "schematic",
	place_on = {"lzr_core:dirt_with_jungle_litter"},
	sidelen = 16,
	noise_params = {
		offset = 0.02,
		scale = 0.003,
		spread = {x = 64, y = 64, z = 64},
		seed = 2,
		octaves = 2,
		persist = 0.66
	},
	place_offset_y = 1,
	y_max = 100,
	y_min = 3,
	schematic = mpath .. "/schematics/palm_tree.mts",
	flags = "place_center_x, place_center_z",
})

-- island grass
minetest.register_decoration({
	name = "islands:grass",
	deco_type = "simple",
	place_on = {"lzr_core:dirt_with_jungle_litter","lzr_core:dirt_with_grass"},
	sidelen = 2,
	noise_params = {
		offset = 0.15,
		scale = 0.05,
		spread = {x = 200, y = 200, z = 200},
		seed = 100,
		octaves = 3,
		persist = 0.6
	},
	y_max = 100,
	y_min = 3,
	decoration = "lzr_plants:island_grass",
})

-- crab grass
minetest.register_decoration({
	name = "islands:crab_grass",
	deco_type = "simple",
	place_on = {"lzr_core:sand"},
	sidelen = 2,
	noise_params = {
		offset = 0.01,
		scale = 0.05,
		spread = {x = 600, y = 600, z = 600},
		seed = 100,
		octaves = 3,
		persist = 0.6
	},
	y_max = 5,
	y_min = 2,
	decoration = "lzr_plants:crab_grass",
})
minetest.register_decoration({
	name = "islands:ant_grass_small",
	deco_type = "simple",
	place_on = {"lzr_core:sand"},
	sidelen = 2,
	noise_params = {
		offset = 0.005,
		scale = 0.05,
		spread = {x = 600, y = 600, z = 600},
		seed = 101,
		octaves = 3,
		persist = 0.6
	},
	y_max = 5,
	y_min = 2,
	decoration = "lzr_plants:ant_grass_small",
	param2 = 4,
})
minetest.register_decoration({
	name = "islands:ant_grass_large",
	deco_type = "simple",
	place_on = {"lzr_core:sand"},
	sidelen = 2,
	noise_params = {
		offset = 0.005,
		scale = 0.05,
		spread = {x = 600, y = 600, z = 600},
		seed = 101,
		octaves = 3,
		persist = 0.6
	},
	y_max = 5,
	y_min = 3,
	decoration = "lzr_plants:ant_grass_large",
	param2 = 4,
})


-- cotton
minetest.register_decoration({
	name = "islands:cotton_bush",
	deco_type = "simple",
	place_on = {"lzr_core:dirt_with_jungle_litter","lzr_core:dirt_with_grass"},
	sidelen = 16,
	noise_params = {
		offset = -0.01,
		scale = 0.03,
		spread = {x = 64, y = 64, z = 64},
		seed = 1519,
		octaves = 3,
		persist = 0.6
	},
	y_max = 31000,
	y_min = 2,
	decoration = "lzr_plants:cotton",
})

minetest.register_decoration({
	name = "islands:coral_sand",
	deco_type = "simple",
	place_on = {"lzr_core:sand"},
	place_offset_y = -1,
	sidelen = 4,
	noise_params = {
		offset = -4,
		scale = 4,
		spread = {x = 50, y = 50, z = 50},
		seed = 7013,
		octaves = 3,
		persist = 0.7,
	},
	y_max = -2,
	y_min = -8,
	flags = "force_placement",
	decoration = {
		"lzr_plants:coral_purple_on_sand",
	},
})

minetest.register_decoration({
	name = "islands:seaweed_sand",
	deco_type = "simple",
	place_on = {"lzr_core:sand"},
	place_offset_y = -1,
	sidelen = 16,
	noise_params = {
		offset = -0.03,
		scale = 0.1,
		spread = {x = 200, y = 200, z = 200},
		seed = 87112,
		octaves = 3,
		persist = 0.7
	},
	y_max = -1,
	y_min = -10,
	flags = "force_placement",
	decoration = "lzr_plants:seaweed_on_sand",
})
minetest.register_decoration({
	name = "islands:seaweed_small_sand",
	deco_type = "simple",
	place_on = {"lzr_core:sand"},
	place_offset_y = -1,
	sidelen = 16,
	noise_params = {
		offset = -0.01,
		scale = 0.1,
		spread = {x = 200, y = 200, z = 200},
		seed = 87112,
		octaves = 3,
		persist = 0.7
	},
	y_max = -1,
	y_min = -10,
	flags = "force_placement",
	decoration = "lzr_plants:seaweed_small_on_sand",
})

minetest.register_decoration({
	name = "islands:seaweed_seabed",
	deco_type = "simple",
	place_on = {"lzr_core:seabed"},
	place_offset_y = -1,
	sidelen = 16,
	noise_params = {
		offset = -0.03,
		scale = 0.1,
		spread = {x = 200, y = 200, z = 200},
		seed = 87112,
		octaves = 3,
		persist = 0.7
	},
	y_max = -1,
	y_min = -10,
	flags = "force_placement",
	decoration = "lzr_plants:seaweed_on_seabed",
})

minetest.register_decoration({
	name = "islands:seaweed_small_seabed",
	deco_type = "simple",
	place_on = {"lzr_core:seabed"},
	place_offset_y = -1,
	sidelen = 16,
	noise_params = {
		offset = -0.01,
		scale = 0.1,
		spread = {x = 200, y = 200, z = 200},
		seed = 87112,
		octaves = 3,
		persist = 0.7
	},
	y_max = -1,
	y_min = -10,
	flags = "force_placement",
	decoration = "lzr_plants:seaweed_small_on_seabed",
})


