-- This mod checks the world/map backend of the current world
-- and if it's anything other than 'dummy' it displays
-- a warning to the player with instructions on how to fix this
-- manually. The 'dummy' backend means that no map will be
-- stored on disk, so the map is fully transient.
-- The map needs to be transient for the game to work
-- properly. Otherwise, it will be cluttered with artifacts
-- from old versions that won't be cleaned up, levels
-- won't be reset cleanly. And in generally it just wastes
-- disk space.

-- Lazarr! version 1.4.0 and previously used a persistent map.
-- Later versions switched to a transient map via game.conf.
-- But Luanti won't automatically transform old maps from
-- persistent to transient on a game update; this is why manual
-- intervention is neccessary.

local S = minetest.get_translator("lzr_check_world_backend")
local FS = function(...) return minetest.formspec_escape(S(...)) end

-- We manually parse the world.mt file because apparently,
-- Luanti does not allow us to access this information
-- in another
local function get_world_backend()
	local worldfile = io.open(minetest.get_worldpath().."/world.mt", "r")
	local worldfile_lines = worldfile:lines()
	local backend, backendLine
	for line, _ in worldfile_lines do
		local splits = string.split(line, "=")
		local key, value = splits[1], splits[2]
		if key then
			key = string.trim(key)
		end
		if value then
			value = string.trim(value)
		end
		if key == "backend" then
			backend = value
			backendLine = line
			break
		end
	end
	if not backend or backend == "" then
		backend = "<unknown>"
	end
	return backend, backendLine
end

local function show_backend_warning(player, backend, backendLine)
	if not backendLine then
		backendLine = "backend = ???"
	end
	local form = "formspec_version[7]size[10,6.5]"..
		"textarea[1,0.8;8,4;;;"..
			FS("WARNING! Bad world backend detected.").."\n\n"..
			FS("This world uses the wrong backend. This means the game can’t properly reset the world when you play or edit levels.").."\n\n"..
			FS("To fix this, you need to edit the file “world.mt” in:").."\n"..
			minetest.get_worldpath().."\n\n"..
			FS("Exit the game, then open world.mt in a text editor and change the line “@1” to “@2”. Save your change. ", backendLine, "backend = dummy").."\n"..
			FS("NOTE: All custom changes to this world will be lost, but custom levels are safe.")..
		"]"..
		"button_exit[1,5;3,0.8;ok;"..FS("OK").."]"
	minetest.show_formspec(player:get_player_name(), "lzr_check_world_backend:backend_warning", form)
end

-- Show warning message when bad world backend was detected.
-- TODO: Add feature to do the world.mt update automatically.
-- Problem: We don't know whether it is safe to overwrite
-- world.mt like that.
local function check_backend(player)
	local backend, backendLine = get_world_backend()
	if backend ~= "dummy" then
		minetest.log("action", "[lzr_check_world_backend] World backend was found to be "..backend..". Showing warning to player ...")
		show_backend_warning(player, backend, backendLine)
	end
end

-- Check backend on player join, unless the movement settings check failed
-- (in which case we wait)
minetest.register_on_joinplayer(function(player)
	if lzr_check_movement_settings.check_movement_settings() == false then
		return
	end
	check_backend(player)
end)

-- Check backend when player got a warning for the bad movement settings
-- and dismissed it. This makes sure that both warning dialogs appear
-- when the player has a bad world backend AND bad movement settings.
lzr_check_movement_settings.register_on_dismiss_warning(function(player)
	check_backend(player)
end)
