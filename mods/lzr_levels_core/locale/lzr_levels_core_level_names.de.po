# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Lazarr! package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Lazarr! 2.0.0\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-26 18:24+0100\n"
"PO-Revision-Date: 2025-01-26 18:36+0000\n"
"Last-Translator: Wuzzy <wuzzy@disroot.org>\n"
"Language-Team: German <https://translate.codeberg.org/projects/lazarr/"
"lzr_levels_core_level_names/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.9.2\n"

msgid "Welcome to Lazarr!"
msgstr "Willkommen bei Lazarr!"

msgid "My first Laser"
msgstr "Mein erster Laser"

msgid "Mirror Rotation"
msgstr "Spiegelrotation"

msgid "Mobile Mirror"
msgstr "Mobiler Spiegel"

msgid "Screws"
msgstr "Schrauben"

msgid "More mobile Blocks"
msgstr "Mehr mobile Blöcke"

msgid "Beam Splitter"
msgstr "Strahlteiler"

msgid "Crystals"
msgstr "Kristalle"

msgid "Barricades"
msgstr "Barrikaden"

msgid "Cobwebs"
msgstr "Spinnenweben"

msgid "Laser Colors"
msgstr "Laserfarben"

msgid "Color Mixing"
msgstr "Farbenmischen"

msgid "Bombs"
msgstr "Bomben"

msgid "Pirate Sense"
msgstr "Piratensinn"

msgid "Palm Pit"
msgstr "Palmengrund"

msgid "Stormy Boxes"
msgstr "Stürmische Kisten"

msgid "Wannabe Spiral"
msgstr "Möchtegernspirale"

msgid "Mirror Jail"
msgstr "Spiegelgefängnis"

msgid "Captain Hook’s Fishing Spot"
msgstr "Käpt’n Hooks Angelplatz"

msgid "Treasure Stash"
msgstr "Schatzbunker"

msgid "Aged like Fine Grog"
msgstr "Gealtert wie feiner Grog"

msgid "The Kitchen"
msgstr "Die Küche"

msgid "Bombastic Beach"
msgstr "Bombastischer Strand"

msgid "Coconut Milk"
msgstr "Kokosnussmilch"

msgid "Lamp Cross"
msgstr "Lampenkreuz"

msgid "26 Mirrors"
msgstr "26 Spiegel"

msgid "Dripstone Cave"
msgstr "Tropfsteinhöhle"

msgid "Inside the Seashell"
msgstr "Im Inneren der Muschel"

msgid "Watery Lights"
msgstr "Wasserlichter"

msgid "An Evening at the Beach"
msgstr "Ein Abend am Strand"

msgid "Sunken Ship"
msgstr "Versunkenes Schiff"

msgid "Withered Throne"
msgstr "Verwitterter Thron"

msgid "Chamber of Symmetry"
msgstr "Kammer der Symmetrie"

msgid "One Chest at a Time"
msgstr "Eine Kiste nach der anderen"

msgid "Temple Sewers"
msgstr "Tempelkanäle"

msgid "Suspicious Barrels"
msgstr "Verdächtige Fässer"

msgid "Return to Lamp Cross"
msgstr "Rückkehr zum Lampenkreuz"

msgid "Inside a Coconut Tree"
msgstr "Im Inneren einer Kokosnusspalme"

msgid "Chaotic Colors"
msgstr "Chaotische Farben"

msgid "Shy Skull Grounds"
msgstr "Senke der scheuen Schädel"

msgid "Cave of the Cursed Skulls"
msgstr "Höhle der verfluchten Totenköpfe"

msgid "Funny Skulls at Night"
msgstr "Lustige Totenköpfe bei Nacht"

msgid "Dead Mine"
msgstr "Tote Mine"

msgid "Rising Skull"
msgstr "Aufstieg eines Totenkopfs"

msgid "Secret Storage"
msgstr "Geheimes Lager"

msgid "Crystal Temple"
msgstr "Kristalltempel"

msgid "Ocean Eye"
msgstr "Ozeanauge"

msgid "Palm Fortress"
msgstr "Palmenfort"

msgid "X marks the Spot"
msgstr "Das X markiert die Stelle"

msgid "Criss-Cross"
msgstr "Kreuz und Quer"
