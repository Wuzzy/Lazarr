local S = minetest.get_translator("lzr_levels_core")

local SKIPPED_LEVEL = 2

if minetest.settings:get_bool("lzr_debug", false) then
	-- Debug command to make it easier to test the event when the player
	-- completed all levels.
	-- This command marks all levels but the first one as completed, so
	-- the remaining level can be manually completed to trigger the
	-- completion event.
	minetest.register_chatcommand("levels_almost_complete", {
		privs = { server = true },
		description = S("Mark all core levels as complete except one"),
		func = function(name, param)
			local core_level_data = lzr_levels.get_level_pack("__core")
			if not core_level_data then
				return false
			end
			local skip
			if #core_level_data >= SKIPPED_LEVEL then
				skip = SKIPPED_LEVEL
			else
				skip = 1
			end
			for n=1, #core_level_data do
				if n ~= skip then
					lzr_levels.mark_level_as_complete(n, core_level_data)
				end
			end
			return true
		end,
	})
end

lzr_levels.register_level_pack("__core",
	{
		--~ Name of main level pack, should be same as the game name
		title = S("Lazarr!"),
		description = S("The main adventure that comes pre-installed with the game."),
		builtin = true,
		textdomain_npc_texts = "lzr_levels_core_npc_texts",
		textdomain_level_names = "lzr_levels_core_level_names",
	}
)

minetest.register_on_joinplayer(function(player)
	if player:get_player_name() == "singleplayer" then
		local level_data_core = lzr_levels.get_level_pack("__core")
		local treasures = lzr_levels.count_total_collected_treasures(level_data_core)
		lzr_levels.on_collected_treasure(level_data_core, treasures)
	end
end)
