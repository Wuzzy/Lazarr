-- Global variables that can be used by all Lazarr! mods
-- that depend on this mod
lzr_globals = {}

--[[ Level ]]
-- Default level position. New level editor levels start here
lzr_globals.DEFAULT_LEVEL_POS = vector.new(1, 1, -23007)
-- Maximum possible level size (not counting the boundary)
lzr_globals.MAX_LEVEL_SIZE = vector.new(30, 30, 30)
-- Minimum possible level size (not counting the boundary)
lzr_globals.MIN_LEVEL_SIZE = vector.new(1, 4, 1)

--[[ Default level options ]]
-- Default level size. New level editor levels start with this size
lzr_globals.DEFAULT_LEVEL_SIZE = vector.new(10, 6, 10)
-- Default level boundary: ceiling
lzr_globals.DEFAULT_CEILING_NODE = "lzr_core:barrier"
-- Default level boundary: wall (the 4 horizontal level boundaries)
lzr_globals.DEFAULT_WALL_NODE = "lzr_core:wood"
-- Default level boundary: floor
lzr_globals.DEFAULT_FLOOR_NODE = "lzr_core:wood"
-- Default NPC texts table. The table uses NPC identifiers
-- as keys and their associated text (that they say when talked to)
-- as values. "goldie" is the only NPC identifier at the moment.
lzr_globals.DEFAULT_NPC_TEXTS = { goldie = "" }
-- Default weather ID (mod: lzr_weather)
lzr_globals.DEFAULT_WEATHER = "clear"
-- Default sky ID (mod: lzr_sky)
lzr_globals.DEFAULT_SKY = "bright_blue"
-- Default backdrop ID (a backdrop is the decorative environment around a level)
lzr_globals.DEFAULT_BACKDROP = "ocean"
-- Default backdrop position (for backdrops that support a position)
lzr_globals.DEFAULT_BACKDROP_POS = vector.zero()

--[[ Position of backdrops with a fixed position. Levels that use
the corresponding backdrop will use the listed position ]]
-- Ocean backdrop
lzr_globals.BACKDROP_POS_OCEAN = table.copy(lzr_globals.DEFAULT_LEVEL_POS)
-- Underground backdrop
lzr_globals.BACKDROP_POS_UNDERGROUND = vector.new(-1999, -2999, -23007)
-- Sky backdrop
lzr_globals.BACKDROP_POS_SKY = vector.new(-3999, 129, -23007)

--[[ "Main menu" ship options ]]
-- Where to spawn the ship
lzr_globals.MENU_SHIP_POS = vector.new(-23008, -3, -23008)

-- Note: All ship "OFFSET" globals are defined as offsets from
-- lzr_globals.MENU_SHIP_POS.

-- Min. and max. coordinates of the cuboid that defines the location of
-- several zones in the ship
lzr_globals.MENU_SHIP_ZONES = {
	captain = {
		min = vector.new(0, 9, 23.55),
		max = vector.new(14, 13, 33),
		exclusions = {
			{ min = vector.new(0,9,23), max = vector.new(3.5,12,26.5) },
			{ min = vector.new(11, 9, 23), max = vector.new(14, 13, 27) } }
	},
	bombs = {
		min = vector.new(3, 4, 2),
		max = vector.new(14, 7, 7.5)
	},
	reading_room = {
		min = vector.new(9, 4, 14),
		max = vector.new(15, 7, 23)
	},
	blocks = {
		min = vector.new(0, 4, 14),
		max = vector.new(5.5, 7, 27)
	},
}

-- Where in the ship the player spawns.
-- Used on game start or when returning to ship normally
lzr_globals.MENU_SHIP_PLAYER_SPAWN_OFFSET = vector.new(7, 9.5, 29)
-- Absolute player spawn position in ship, provided for convenience
lzr_globals.MENU_PLAYER_SPAWN_POS = vector.add(lzr_globals.MENU_SHIP_POS, lzr_globals.MENU_SHIP_PLAYER_SPAWN_OFFSET)
-- Where the hidden parrots spawn when the player has found them
lzr_globals.MENU_SHIP_HIDDEN_PARROT_OFFSETS = {
	ruby = vector.new(10, 5.5, 29),
	emmy = vector.new(10, 5.5, 30),
	saphie = vector.new(10, 5.5, 31),
	garnie = vector.new(11, 5.5, 29),
	tuckie = vector.new(11, 5.5, 30),
	dimey = vector.new(11, 5.5, 31),
}
-- Where in the ship the player respawns.
-- Used when player fell out of the map or got stuck (lzr_fallout)
lzr_globals.MENU_SHIP_PLAYER_RESPAWN_OFFSET = vector.new(6, 1.25, 9)
-- Where in the ship the player spawns on victory,
-- when all core levels have been completed.
lzr_globals.MENU_SHIP_PLAYER_WINSPAWN_OFFSET = vector.new(6, 1.25, 22)
-- Where the ship's gold stash begins. All gold blocks the player
-- has collected in the core levels will be placed here.
-- This is the minimum coordinate of a cuboid.
lzr_globals.MENU_SHIP_STASH_OFFSET = vector.new(4, 1, 17)
-- Size of the ship's gold stash cuboid
lzr_globals.MENU_SHIP_STASH_SIZE = vector.new(8, 3, 14)
-- This is the minimum coordinate of a cuboid for the 'first pass' of placing gold blocks
lzr_globals.MENU_SHIP_STASH_OFFSET_FIRST = lzr_globals.MENU_SHIP_STASH_OFFSET
-- Size of the ship's gold stash cuboi for the 'first pass' of placing gold blocks
lzr_globals.MENU_SHIP_STASH_SIZE_FIRST = vector.new(8, 2, 14)
-- This is the minimum coordinate of a cuboid for the 'second pass' of placing gold blocks
lzr_globals.MENU_SHIP_STASH_OFFSET_SECOND = vector.new(4, 3, 17)
-- Size of the ship's gold stash cuboi for the 'second pass' of placing gold blocks
lzr_globals.MENU_SHIP_STASH_SIZE_SECOND = vector.new(8, 1, 14)
-- Where to place the node that starts the game (core levels)
lzr_globals.MENU_SHIP_STARTBOOK_OFFSET = vector.new(7, 11, 31)
-- Where to place the node that opens custom level selection
lzr_globals.MENU_SHIP_CUSTOMBOOK_OFFSET = vector.new(8, 11, 31)
-- Where to place the painting "Perfect Plunderer"
-- (awarded when player completed all core levels)
lzr_globals.MENU_SHIP_PAINTING_PERFECT_PLUNDERER_OFFSET = vector.new(7, 12, 32)
-- Where to place the painting "Parrot Finder"
-- (awarded when player found all the hidden parrots)
lzr_globals.MENU_SHIP_PAINTING_PARROT_FINDER_OFFSET = vector.new(8, 12, 32)
-- Where to place the node that leads to the level editor
lzr_globals.MENU_SHIP_EDITOR_OFFSET = vector.new(13, 11, 31)
-- Where to place the node that changes sound options
lzr_globals.MENU_SHIP_SPEAKER_OFFSET = vector.new(6, 10, 25)
-- Where to place the node that changes graphics options
lzr_globals.MENU_SHIP_TELEVISION_OFFSET = vector.new(5, 10, 25)
-- Where to place the node that opens the gameplay infobooks
lzr_globals.MENU_SHIP_HOWTO_BOOKSHELF_OFFSET = vector.new(14, 11, 29)
-- Where to place the node that opens the editor infobooks
lzr_globals.MENU_SHIP_HOWTO_EDITOR_BOOKSHELF_OFFSET = vector.new(14, 11, 28)
-- Where to place the node that opens the blocks infobooks
lzr_globals.MENU_SHIP_HOWTO_BLOCKS_BOOKSHELF_OFFSET = vector.new(6, 6, 18)
-- Where to place the node that opens the credits screen
lzr_globals.MENU_SHIP_CREDITS_BOOKSHELF_OFFSET = vector.new(9, 6, 15)
-- Where to place the node that opens the level pack stats screen
lzr_globals.MENU_SHIP_LEVEL_PACKS_BOOKSHELF_OFFSET = vector.new(9, 6, 16)
-- Where to place the node that resets the game progress
lzr_globals.MENU_SHIP_RESET_BOMB_OFFSET = vector.new(6, 5, 5)

--[[ Gameplay ]]
lzr_globals.GRAVITY = tonumber(minetest.settings:get("movement_gravity")) or 9.81

--[[ Rendering ]]
-- Light level of lasers. Affects active laser blocks as well
lzr_globals.LASER_GLOW = 3
-- Alpha (=translucency) of lasers (0=minimum, 0xFF=maximum)
lzr_globals.LASER_ALPHA = 0x8E
-- Same as lzr_globals.LASER_ALPHA, but as a 2-digit hex string
lzr_globals.LASER_ALPHA_STRING = string.format("%02X", lzr_globals.LASER_ALPHA)

--[[ Audio ]]
-- How many destroy/burn sounds to start playing at once
lzr_globals.MAX_DESTROY_SOUNDS_AT_ONCE = 16

-- Maximum number of nodes the player can see. We do this because
-- the map is segmented in multiple zones (e.g. islands, deep ocean)
-- with visible seams. By making sure the player is always far enough
-- from the zone borders, and by reducing the view distance,
-- the player will never see those borders.
lzr_globals.MAX_VIEW_DISTANCE = 1000

--[[ Editor ]]
-- File name of autosaved levels (without file name suffix)
lzr_globals.AUTOSAVE_NAME = "_AUTOSAVE_"

--[[ NPCs ]]
-- The parrot NPC will spawn at the parrot spawner node
-- position plus this offset
lzr_globals.PARROT_SPAWN_OFFSET = vector.new(0, -0.49, 0)

--[[ Signals ]]
-- Maximum number of times a receiver may be triggered again
-- in the same laser update routine before overheating
-- (and deactivating for the rest of that laser update routine)
lzr_globals.MAX_RECEIVER_RECALLS = 5

--[[ Mapgen ]]
-- WARNING: This value is duplicated in lzr_mapgen.
-- Only change this value when also changing lzr_mapgen!
lzr_globals.DEEP_OCEAN_Z = -20000

--[[ Solutions ]]
-- The timers of the level solution test are multiplied by this
-- number. This speeds up the actions in the solution as well
-- as the timers of timed nodes like bombs.
-- Lower = faster. 1 = normal time.
lzr_globals.LEVEL_TEST_TIME_MULTIPLIER = 0.22

-- Laser color codes
lzr_globals.COLOR_NONE = 0 -- special case when there's no laser
lzr_globals.COLOR_RED = 1
lzr_globals.COLOR_GREEN = 2
lzr_globals.COLOR_YELLOW = 3
lzr_globals.COLOR_BLUE = 4
lzr_globals.COLOR_MAGENTA = 5
lzr_globals.COLOR_CYAN = 6
lzr_globals.COLOR_WHITE = 7

-- Maximum possible color code
lzr_globals.MAX_COLORCODE = 7

-- Laser color names (for internal use only, non-translatable)
lzr_globals.COLOR_NAMES = {
	[lzr_globals.COLOR_NONE] = "none",
	[lzr_globals.COLOR_RED] = "red",
	[lzr_globals.COLOR_GREEN] = "green",
	[lzr_globals.COLOR_BLUE] = "blue",
	[lzr_globals.COLOR_YELLOW] = "yellow",
	[lzr_globals.COLOR_MAGENTA] = "magenta",
	[lzr_globals.COLOR_CYAN] = "cyan",
	[lzr_globals.COLOR_WHITE] = "white",
}

-- Same as lzr_globals.COLOR_NAMES, but with key and value swapped
lzr_globals.COLOR_NAMES_SWAPPED = table.key_value_swap(lzr_globals.COLOR_NAMES)

-- Stores the setting lzr_opaque_lasers
lzr_globals.OPAQUE_LASERS = minetest.settings:get_bool("lzr_opaque_lasers", false)


--[[ Rain membrane collision box ]]
-- Collision box for rain membrane node
-- Rain membrane corner position
local RMC = 0.25
-- Rain membrane offset (a tiny value);
-- determines the X/Z size of the columns
local RMO = 0.01
-- Collision box of rain membrane node, should let raindrops
-- through but not the player.
lzr_globals.RAIN_MEMBRANE_COLLISION_BOX = {
	-- The collision box is made of 4 very thin columns,
	-- equally spaced apart. This is enough to prevent the player
	-- from moving through it vertically, assuming the player collisionbox is
	-- larger than the space between the columns (which is roughly
	-- RMO times 2).
	-- Because the columns are so thin, they leave enough space
	-- for most rain particles to go through, except for the few unlucky ones
	-- that directly hit the columns (but this is rare).
	type = "fixed",
	fixed = {
		{ -RMC-RMO, -0.5, -RMC-RMO, -RMC+RMO, 0.5, -RMC+RMO }, -- (-1,-1)
		{ -RMC-RMO, -0.5,  RMC-RMO, -RMC+RMO, 0.5,  RMC+RMO }, -- (-1, 1)
		{  RMC-RMO, -0.5, -RMC-RMO,  RMC+RMO, 0.5, -RMC+RMO }, -- ( 1,-1)
		{  RMC-RMO, -0.5,  RMC-RMO,  RMC+RMO, 0.5,  RMC+RMO }, -- ( 1, 1)
	},
}
