## Get Item for Lazarr!

This Luanti mod allows the player to get easy access to
items in the game for the level editor or when the player
has the 'give' privilege.

The items are accessed via the formspec that can be
opened with the function `lzr_getitem.show_formspec(player_name)`.

All items not in the `not_in_creative_inventory` group
are available.

## Credits

Created by Wuzzy.
Licensed under the MIT License (see `LICENSE.md`).
