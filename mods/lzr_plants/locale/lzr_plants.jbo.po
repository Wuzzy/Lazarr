msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain lzr_plants x.x.x\n"
"Report-Msgid-Bugs-To: Wuzzy@disroot.org\n"
"POT-Creation-Date: 2025-01-06 14:42+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: jbo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/lzr_plants/init.lua:11
msgid "Island Grass"
msgstr ""

#: mods/lzr_plants/init.lua:32
msgid "Crab Grass"
msgstr ""

#: mods/lzr_plants/init.lua:53
msgid "Small Ant Grass"
msgstr ""

#: mods/lzr_plants/init.lua:75
msgid "Large Ant Grass"
msgstr ""

#: mods/lzr_plants/init.lua:99
msgid "Large Seaweed"
msgstr ""

#: mods/lzr_plants/init.lua:121
msgid "Small Seaweed"
msgstr ""

#: mods/lzr_plants/init.lua:145
msgid "Cotton"
msgstr ""

#: mods/lzr_plants/init.lua:166
msgid "Purple Coral"
msgstr ""

#: mods/lzr_plants/init.lua:194
msgid "Purple Coral on Sand"
msgstr ""

#: mods/lzr_plants/init.lua:218
msgid "Purple Coral on Seabed"
msgstr ""

#: mods/lzr_plants/init.lua:244
msgid "Large Seaweed on Sand"
msgstr ""

#: mods/lzr_plants/init.lua:268
msgid "Large Seaweed on Seabed"
msgstr ""

#: mods/lzr_plants/init.lua:292
msgid "Small Seaweed on Sand"
msgstr ""

#: mods/lzr_plants/init.lua:316
msgid "Small Seaweed on Seabed"
msgstr ""
