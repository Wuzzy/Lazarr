-- This mod adds simple decorative plants.
-- The plants have no gameplay effect and don't funtionally interact with the puzzle.
-- For puzzle gameplay purposes, a plant behaves like air:
-- Lasers shoot through (which destroys the plant) and the player can build into it.
-- Plants are not takable by the player either.
-- Once destroyed, a plant is gone for good.

local S = minetest.get_translator("lzr_plants")

minetest.register_node("lzr_plants:island_grass", {
	description = S("Island Grass"),
	tiles = { "islands_tall_grass.png" },
	wield_image = "islands_tall_grass.png",
	inventory_image = "islands_tall_grass.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 0.5, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})

minetest.register_node("lzr_plants:crab_grass", {
	description = S("Crab Grass"),
	tiles = { "lzr_plants_crab_grass.png" },
	wield_image = "lzr_plants_crab_grass.png",
	inventory_image = "lzr_plants_crab_grass.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 1/16, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})

minetest.register_node("lzr_plants:ant_grass_small", {
	description = S("Small Ant Grass"),
	tiles = { "lzr_plants_small_ant_grass.png" },
	wield_image = "lzr_plants_small_ant_grass.png",
	inventory_image = "lzr_plants_small_ant_grass.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -7/16, -0.5, -7/16, 7/16, 0, 7/16 },
	},
	paramtype2 = "meshoptions",
	place_param2 = 4,
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})
minetest.register_node("lzr_plants:ant_grass_large", {
	description = S("Large Ant Grass"),
	tiles = { "lzr_plants_large_ant_grass.png" },
	wield_image = "lzr_plants_large_ant_grass.png",
	inventory_image = "lzr_plants_large_ant_grass.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -7/16, -0.5, -7/16, 7/16, 3/16, 7/16 },
	},
	paramtype2 = "meshoptions",
	place_param2 = 4,
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})


minetest.register_node("lzr_plants:seaweed", {
	description = S("Large Seaweed"),
	tiles = { "lzr_plants_seaweed.png" },
	wield_image = "lzr_plants_seaweed.png",
	inventory_image = "lzr_plants_seaweed.png",
	drawtype = "plantlike",
	paramtype2 = "wallmounted",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 0.5, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})

minetest.register_node("lzr_plants:seaweed_small", {
	description = S("Small Seaweed"),
	tiles = { "lzr_plants_seaweed_small.png" },
	wield_image = "lzr_plants_seaweed_small.png",
	inventory_image = "lzr_plants_seaweed_small.png",
	drawtype = "plantlike",
	paramtype2 = "wallmounted",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 1/16, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})



minetest.register_node("lzr_plants:cotton", {
	description = S("Cotton"),
	tiles = { "farming_cotton_wild.png" },
	wield_image = "farming_cotton_wild.png",
	inventory_image = "farming_cotton_wild.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 5/16, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})

minetest.register_node("lzr_plants:coral_purple", {
	description = S("Purple Coral"),
	tiles = { "lzr_plants_coral_purple.png" },
	wield_image = "lzr_plants_coral_purple.png",
	inventory_image = "lzr_plants_coral_purple.png",
	drawtype = "plantlike",
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = { -6/16, -0.5, -6/16, 6/16, 0.5, 6/16 },
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_grass_defaults(),
	groups = { punchdig = 1, laser_destroys = 1, explosion_destroys = 1, attached_node = 1, plant = 1 },
	drop = "",
	buildable_to = true,
})

--[[ Rooted plants.
These are underwater plants that can be placed by the mapgen
as decorations. They are NOT suited for use in levels,
as the laser interacts poorly with them and they cannot
be dug.
]]

minetest.register_node("lzr_plants:coral_purple_on_sand", {
	description = S("Purple Coral on Sand"),
	tiles = { "default_sand.png" },
	special_tiles = { "lzr_plants_coral_purple.png" },
	wield_image = "lzr_plants_coral_purple_sand.png",
	inventory_image = "lzr_plants_coral_purple_sand.png",
	drawtype = "plantlike_rooted",
	walkable = true,
	selection_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
			{ -6/16, 0.5, -6/16, 6/16, 1.5, 6/16 },
		},
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_sand_defaults(),
	groups = { breakable = 1, plant_on_ground = 1, not_in_creative_inventory = 1 },
	drop = "",
})

minetest.register_node("lzr_plants:coral_purple_on_seabed", {
	description = S("Purple Coral on Seabed"),
	tiles = { "lzr_core_seabed.png" },
	special_tiles = { "lzr_plants_coral_purple.png" },
	wield_image = "lzr_plants_coral_purple_seabed.png",
	inventory_image = "lzr_plants_coral_purple_seabed.png",
	drawtype = "plantlike_rooted",
	walkable = true,
	selection_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
			{ -6/16, 0.5, -6/16, 6/16, 1.5, 6/16 },
		},
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_dirt_defaults(),
	groups = { breakable = 1, plant_on_ground = 1, not_in_creative_inventory = 1 },
	drop = "",
})



minetest.register_node("lzr_plants:seaweed_on_sand", {
	description = S("Large Seaweed on Sand"),
	tiles = { "default_sand.png" },
	special_tiles = { "lzr_plants_seaweed.png" },
	wield_image = "lzr_plants_seaweed_sand.png",
	inventory_image = "lzr_plants_seaweed_sand.png",
	drawtype = "plantlike_rooted",
	walkable = true,
	selection_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
			{ -6/16, 0.5, -6/16, 6/16, 1.5, 6/16 },
		},
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_sand_defaults(),
	groups = { breakable = 1, plant_on_ground = 1, not_in_creative_inventory = 1 },
	drop = "",
})

minetest.register_node("lzr_plants:seaweed_on_seabed", {
	description = S("Large Seaweed on Seabed"),
	tiles = { "lzr_core_seabed.png" },
	special_tiles = { "lzr_plants_seaweed.png" },
	wield_image = "lzr_plants_seaweed_seabed.png",
	inventory_image = "lzr_plants_seaweed_seabed.png",
	drawtype = "plantlike_rooted",
	walkable = true,
	selection_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
			{ -6/16, 0.5, -6/16, 6/16, 1.5, 6/16 },
		},
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_dirt_defaults(),
	groups = { breakable = 1, plant_on_ground = 1, not_in_creative_inventory = 1 },
	drop = "",
})

minetest.register_node("lzr_plants:seaweed_small_on_sand", {
	description = S("Small Seaweed on Sand"),
	tiles = { "default_sand.png" },
	special_tiles = { "lzr_plants_seaweed_small.png" },
	wield_image = "lzr_plants_seaweed_small_sand.png",
	inventory_image = "lzr_plants_seaweed_small_sand.png",
	drawtype = "plantlike_rooted",
	walkable = true,
	selection_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
			{ -6/16, 0.5, -6/16, 6/16, 17/16, 6/16 },
		},
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_sand_defaults(),
	groups = { breakable = 1, plant_on_ground = 1, not_in_creative_inventory = 1 },
	drop = "",
})

minetest.register_node("lzr_plants:seaweed_small_on_seabed", {
	description = S("Small Seaweed on Seabed"),
	tiles = { "lzr_core_seabed.png" },
	special_tiles = { "lzr_plants_seaweed_small.png" },
	wield_image = "lzr_plants_seaweed_small_seabed.png",
	inventory_image = "lzr_plants_seaweed_small_seabed.png",
	drawtype = "plantlike_rooted",
	walkable = true,
	selection_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
			{ -6/16, 0.5, -6/16, 6/16, 17/16, 6/16 },
		},
	},
	waving = 1,
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	sounds = lzr_sounds.node_sound_dirt_defaults(),
	groups = { breakable = 1, plant_on_ground = 1, not_in_creative_inventory = 1 },
	drop = "",
})

