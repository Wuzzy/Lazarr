# Group reference

This file is a technical documentation of all the groups
used for the items and nodes in the game.

## Node group reference

This is a reference of all groups used for nodes.

### Digging groups

* `breakable=1`: Node is breakable with Ultra Pickaxe
* `punchdig=1`: Node breaks when punched
* `takable=1`: Node can be taken by player during an active game

### Gameplay groups

* `rotatable`: Node is rotatable by hook item
    * `1`: Always rotatable
    * `2`: (unused)
    * `3`: Rotatable in editor/dev mode only
* `laser_destroys`: Node is destroyed when a laser touches it
    * `1`: Immediate destruction
    * `2`: Node catches fire first, then is destroyed.
           `_lzr_active` must contain the name of the burning variant.
           The burning variant in turn must have `_lzr_inactive`
    * `3`: Same as `2`, but gets ignited from the top only
* `explosion_destroys=1`: Node will be destroyed by explosions
* `laser_incompatible=1`: The node is "incompatible" with lasers.
  (see `CODE_DOCUMENTATION.md`)
* `flammable=1`: Node catches fire (or is ignited) from any neighboring burning block from any side
* `flammable=2`: Node catches fire (or is ignited) from a neighboring burning block on its top side
* `spawner=1`: Node spawns an entity when the level is loaded
* `screws`: Node has screws (note: don't rely on screws to check if the node is rotatable;
  use `rotatable` instead)
    * `1`: Copper screws
    * `2`: Iron screws

### Laser node groups

* `laser=X`: Node *is* a laser node, consisting of 1 to 3 laser beams
  (X = number uniquely identifying each possible combination of
  laser axes and colors)
  (not including blocks that *interact* with lasers like mirrors)
* `laser_block=1`: A "laser block", a node that interacts with lasers without
    being a laser itself.
* `laser_block_color=X`: A laser block with an associated laser color (X = colorcode, see `lzr_globals`)

#### Laser blocks

Group rating 1 is for the inactive state, group rating 2 is for the active state.

* `emitter`: Emitter
* `emitter_color=X`: Emitter color (X = colorcode, see `lzr_globals`)
* `detector`: Detector
* `detector_color=X`: Colored detector (X = colorcode, see `lzr_globals`)
* `crystal`: Crystal
* `mirror`: Mirror (normal)
* `transmissive_mirror`: Transmissive Mirror
* `double_mirror`: Double Mirror
* `hollow_barrel`: Hollow barrel
* `shy_skull`: Shy Skull
* `cursed_skull`: Cursed Skull
* `lightbox`: Any lightbox, can be turned on or off
* `barricade`: Barricade
* `bomb`: Bomb
* `mixer`: Mixer
* `cabinet_half`: Half cabinet
* `ships_wheel`: Ship’s wheel
* `palm_leaves`: Palm leaves
* `laser_element_*`: Generic default group name assigned to less important laser blocks, like slabs. (The asterisk is a placeholder)
* `barrier`: Invisible barrier preventing player from passing through.
   NOTE: Nodes of this group must support a single laser going through the node. Barrier nodes
   may only be placed as part of the level boundary and not anywhere else
   * `barrier=1`: Without laser
   * `barrier=2`: With laser
* `rain_membrane`: Same as `barrier` but additionally, raindrops can fall through
   * `rain_membrane=1`: Without laser
   * `rain_membrane=2`: With laser

### Trigger groups

Some nodes can trigger other nodes or be triggered, causing
a special event to happen.

* `sender=1`: Block is a sender and can send signals to receivers
* `receiver=1`: Block is a receiver and can accept signals from senders

* `orphan_receiver=1`: Node is not a receiver itself, but it represents
  a state of a group of nodes, one of which is a receiver. Only used
  for informational purposes.

### Misc. categorization groups

* `treasure=1`: Treasure
* `teleporter`: Teleporter
* `chest`: Chest
    * `1`: closed, unlocked
    * `2`: closed, locked
    * `3`: open, empty
    * `4`: open, with treasure
* `chest_closed=1`: Closed chest
* `chest_open=1`: Open chest
* `chest_open_treasure=1`: Open chest with treasure
* `water=3`: Water
* `liquid=3`: Node is a liquid in a semantic sense
* `pane=1`: Pane (flat window-like block)
* `door`: Door segment
    * `1`: Bottom segment
    * `2`: Top segment
* `door_exit=1`: Exit door segment
* `door_locked=1`: Locked door segment
* `stair`: Stair
    * `1`: Normal stair
    * `2`: Inner stair
    * `3`: Outer stair
* `skull`: Any skull (cursed or shy)
    * `1`: Active
    * `2`: Inactive
* `cracked=1`: Node has visible cracks (usually in stone)
  Usually combined with `explosion_destroys`
* `potted_flower=1`: Any potted flower
* `plant=1`: Any small decorative plant (standalone)
* `plant_on_ground=1`: A node that is a block with a plant *on* it (using `plantlike_rooted` or similar)
* `stone=1`: Stone
* `ground=1`: Soft ground like dirt, sand, seabed, etc.
* `furniture=1`: Furniture like chair, table, etc. but also workstations like forge
* `leaves=1`: Leaves and foliage
* `fruit=1`: Fruit, nut, vegetable, or similar
* `wood=1`: Wooden planks
* `tree=1`: Tree trunk
* `thatch=1`: Thatch
* `cloth=1`: Cloth
* `glass=1`: Glass
* `crate=1`: Crate / box
* `barrel=1`: Barrel (for storage)
* `slab`: Slab
    * `1`: bottom
    * `2`: top
* `double_slab`: Double slab

## Item groups reference

This is a reference of groups for all items that are not nodes.

* `pickaxe=1`: Pickaxe
* `bucket=1`: Bucket
* `editor_tool=1`: This tool is meant specifically for level editing
* `dev_tool=1`: This tool is for development purposes

