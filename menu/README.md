# Information about the menu images

## `icon.png`

`icon.png` can be recreated with the following Lua code:

    minetest.show_formspec(me:get_player_name(), "form", "size[20,20]box[-0.5,-0.5;21,11;#000000ff]item_image[0,0;10,10;lzr_laser:emitter_red_takable_on]item_image[11,0;10,10;lzr_laser:laser_001]")

This displays a rendering of the red emitter and a red laser beam, which can
then be manually combined with an image editor of your choice.

## `header.png`

This was built with the fonts:

* Gohu GohuFont Medium for the “Laz” and
* Alte Schwabacher for the “arr!”.

## `header_es.png`

Spanish version of `header.png`. This is currently unused by the game because
translated menu images are not (yet) supported in Luanti (as of 5.9.1).

## `*.svg` files

These are the vector source files to generate the header PNGs.
Use an program that can open SVG files to edit them.

## `background.png`

Just the wooden planks texture repeated.
Slightly darkened for better readability of the text.

## `footer.png`

A footer text message in English. No translation is available.
To be removed in v2.0.0.
