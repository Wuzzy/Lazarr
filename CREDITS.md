# Credits for Lazarr!

Lazarr! is a pirate-themed laser puzzle game designed by Wuzzy.

This game uses many artworks from other people, mostly from
the Luanti community and independent artwork sharing communities like
freesound.org and opengameart.org. Without these communities,
this game would not be possible.

This game is free software (including the artwork). See README.md
for the licensing of the game as a whole.

A list of all people who worked on this game, both directly and
indirectly, follows:

## Idea

- Wuzzy

## Lead Programming

- Wuzzy
	- License: GPLv3+

## Additional Programming

- Player model: Minetest Game developers
	- Origin: <https://github.com/minetest/minetest_game>
	- Modifications were made
	- License: LGPLv2.1+
- Stairs and slabs: Minetest Game developers
	- Origin: <https://github.com/minetest/minetest_game>
	- Modifications were made
	- License: LGPLv2.1+
- Decoration blocks from xdecor mod: Jean-Patrick Guerrero (alias jp)
	- Origin: <https://github.com/minetest-mods/xdecor>
	- Modifications were made
	- License: BSD 3-Clause
- Original screwdriver2 mod, used for the rotating hook: 12Me21
	- Origin: <https://github.com/12Me21/screwdriver2>
	- Modifications were made
	- License: MIT

## Level Design

- Wuzzy
	- License: CC BY 4.0

## 2D Graphics

- Water, wood (normal), tree (normal), stone (normal, tile, mossy tile, brick, mossy brick, circular, mossy circular), sandstone brick, sandstone block, gold block, wooden chest, onyx chest, ship lightbox, wood lightbox, light crate, heavy crate, mossy heavy crate, bonfire, seaweed, purple coral, crab grass, fire, wool (cloth), bomb, bookshelf, teleporter, television, pickaxe, bucket: Jean-Patrick Guerrero (alias jp)
	- File names: `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`, `default_water.png`
	- Origin: <https://content.luanti.org/packages/jp/pixelbox/>
	- Modifications were made
	- License: CC0
- Barrel (normal), cabinet, half cabinet, candle (floor, inventory, wall, wield), cauldron, cobweb, flower pot, empty shelf, iron lightbox, lantern, multishelf, wooden lightbox, woodframed glass, wood frame, chair/table wood, television, barricade, saw: Jean-Patrick Guerrero (alias jp)
	- File names: `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`, `xdecor_barrel_sides.png`
	- Origin: <https://github.com/minetest-mods/xdecor>
	- Modifications were made
	- License: CC0
- Emitter base image: Jean-Patrick Guerrero (alias jp)
	- File name: `lzr_laser_emitter_on.png`
	- Original file name: `xdecor_wooden_lightbox.png`
	- Origin: <https://content.luanti.org/packages/jp/pixelbox/>
	- Modifications were made
	- License: CC0
- Detector base image: Jean-Patrick Guerrero (alias jp)
	- File name: `lzr_laser_detector.png`
	- Original file name: `xdecor_wood_tile.png`
	- Origin: <https://content.luanti.org/packages/jp/pixelbox/>
	- Modifications were made
	- License: CC0
- Emitter extra images: Wuzzy
	- File names: `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`, `lzr_laser_emitter_color_template_off.png`
	- License: CC0
- Detector extra images: Wuzzy
	- File names: `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`, `lzr_laser_detector_colored_front.png`
	- License: CC0
- Sandstone, palm wood, coconut wood, loose wood, mossy wood, cave stone (normal, block, brick, pillar), stone pillar, circular stone, mossy circular stone, forge animation, cold forge, hanging candle, ship’s wheel, mirror, mixer, beam splitter, double mirror, crystal, ant grass (small and large), iron grate/bars, rusty iron grate/bars, old crate, old barrel, screws, wooden door, maps, paintings: Wuzzy
	- File names: `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`, `default_sandstone.png`
	- License: CC0
- Parrots: Wuzzy
	- File name: `lzr_parrot_npc_*.png`
	- License: CC0
- Player texture: Jean-Patrick Guerrero (alias jp)
	- File name: `character.png`
	- Original file name: `character.png`
	- Origin: <https://content.luanti.org/packages/jp/pixelbox/>
	- License: CC0
- Scorched player texture: Wuzzy
	- File names: `character_scorched.png`, `character_scorched.png`
	- License: CC0
- Raindrop, crosshair, wieldhand, smoke puff, bomb smoke, book interface, damage screen, trigger icons, laser particles, level corner marker, menu item marker, laser patterns, trigger tool, forbidden icon: Wuzzy
	- File names: `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`, `lzr_weather_rain.png`
	- License: CC0
- Main menu images: Wuzzy
	- File names: `background.png`, `background.png`, `background.png`, `background.png`, `background.png`
	- License: CC BY 4.0
- Vessels shelf: Vanessa Ezekowitz (alias VanessaE)
	- File name: `lzr_decor_vessels_shelf.png`
	- Original file name: `vessels_shelf.png`
	- Origin: <https://github.com/minetest/minetest_game>
	- Modifications were made
	- License: CC BY 3.0
- Ocean stone (all variants), ocean lantern: StarNinjas
	- File names: `xocean_*.png`, `xocean_*.png`
	- Original file name: `xocean_*.png`
	- Origin: <https://github.com/starninjas/xocean>
	- Modifications were made
	- License: MIT
- Island-style blocks: Dirt, grass cover, island stone, island grass, sand, seabed, shrub leaves, palm tree, twigs: Termos
	- File names: `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`
	- Original file names: `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`, `default_dirt.png`
	- Origin: <https://content.luanti.org/packages/Termos/islands/>
	- License: MIT
- Wild Cotton: Extex101
	- File name: `farming_cotton_wild.png`
	- Original file name: `farming_cotton_wild.png`
	- Origin: <https://github.com/minetest/minetest_game>
	- License: CC BY 3.0
- Skulls: Mikita Wiśniewski (alias rudzik8)
	- File names: `lzr_laser_cskull_*.png`, `lzr_laser_cskull_*.png`
	- Original file names: `lzr_laser_cskull_*.png`, `lzr_laser_cskull_*.png`
	- License: CC BY 3.0
- Coconut and coconut tree: Neuromancer
	- File name: `coconut*.png`
	- Original file name: `coconut*.png`
	- Origin: <https://github.com/Neuromancer56/coconut_tree>
	- License: CC BY 4.0
- Thatch: Mossmanikin
	- File names: `dryplants_thatch.png`, `dryplants_thatch.png`
	- Original file names: `dryplants_thatch.png`, `dryplants_thatch.png`
	- Origin: <https://github.com/mt-mods/plantlife_modpack>
	- License: CC BY 4.0
- Hotbar: Bonemouse
	- File name: `lzr_gui_hotbar_*.png`
	- Original file name: `gui.png`
	- Origin: <http://www.minecraftforum.net/topic/242175-Isabella/>
	- Modifications were made
	- License: CC BY 3.0
- Dialog window background: Q\_x
	- File name: `lzr_gui_bg.png`
	- Original file name: `sheet of old paper.tif`
	- Origin: <https://opengameart.org/content/sheet-of-old-paper>
	- Modifications were made
	- License: CC0
- Dialog window background: Lamoot
	- File name: `lzr_gui_bg.png`
	- Original file name: `paper_background.png`
	- Origin: <https://opengameart.org/content/rpg-gui-construction-kit-v10>
	- Modifications were made
	- License: CC BY 3.0
- Dialog window button: Lamoot
	- File names: `lzr_gui_button.png`, `lzr_gui_button.png`, `lzr_gui_button.png`
	- Original file name: `RPG_GUI_v1.png`
	- Origin: <https://opengameart.org/content/rpg-gui-construction-kit-v10>
	- Modifications were made
	- License: CC BY 3.0
- Hook: CraftPix.net
	- File name: `lzr_hook_hook.png`
	- Original file name: `Icon39.png`
	- Origin: <https://opengameart.org/content/48-pirate-stuff-icons>
	- Modifications were made
	- License: OGA-BY 3.0
- Speaker: MCL <temp1@cubesoftware.xyz>
	- File name: `lzr_menu_speaker_*.png`
	- Original file name: `xdecor_speaker_*.png`
	- License: CC BY 4.0
- Any texture not listed above: Wuzzy
	- License: CC BY 4.0

## 3D Graphics

- Mirror, crystal, ship’s wheel, parrot, trivial shapes: Wuzzy
	- License: CC0
- Skull: Mikita Wiśniewski (alias rudzik8)
	- File names: `lzr_laser_skull.blend`, `lzr_laser_skull.blend`
	- Original file names: `lzr_laser_skull.blend`, `lzr_laser_skull.blend`
	- License: CC BY 3.0
- Table, chair: Jean-Patrick Guerrero (alias jp)
	- Origin: <https://github.com/minetest-mods/xdecor>
	- License: CC0
- Player model: Minetest Game developers
	- File names: `character.b3d`, `character.b3d`
	- Original file names: `character.b3d`, `character.b3d`
	- Origin: <https://github.com/minetest/minetest_game>
	- License: CC BY 3.0

## Music

- Exploring the Ocean: Leonardo Paz (alias leopaz)
	- File name: `lzr_ambience_leopaz_exploring_the_ocean.ogg`
	- Original file name: `01_exploring_the_ocean.wav`
	- Origin: <https://opengameart.org/content/ocean-music-pack>
	- License: CC BY 4.0
- Seaside Village: Leonardo Paz (alias leopaz)
	- File name: `lzr_ambience_leopaz_seaside_village.ogg`
	- Original file name: `02_seaside_village.wav`
	- Origin: <https://opengameart.org/content/ocean-music-pack>
	- License: CC BY 4.0
- Siren’s Call: Leonardo Paz (alias leopaz)
	- File name: `lzr_ambience_leopaz_sirens_call.ogg`
	- Original file name: `04_sirens_call.wav`
	- Origin: <https://opengameart.org/content/ocean-music-pack>
	- License: CC BY 4.0
- Stranded in the Ocean: Leonardo Paz (alias leopaz)
	- File name: `lzr_ambience_leopaz_stranded_in_the_ocean.ogg`
	- Original file name: `05_stranded_in_the_ocean.wav`
	- Origin: <https://opengameart.org/content/ocean-music-pack>
	- License: CC BY 4.0
- A Friendly Nautilus Shows us the Way: Vandalorum
	- File name: `lzr_ambience_vandalorum_a_friendly_nautilus_shows_us_the_way.ogg`
	- Original file name: `vandalorum_-_a_friendly_nautilus_shows_us_the_way.wav`
	- Origin: <https://opengameart.org/content/water-level-synth-music>
	- License: CC BY 4.0
- A Mist Hangs Over the Water: Vandalorum
	- File name: `lzr_ambience_vandalorum_a_mist_hangs_over_the_water.ogg`
	- Original file name: `vandalorum_-_a_mist_hangs_over_the_water.wav`
	- Origin: <https://opengameart.org/content/water-level-synth-music>
	- License: CC BY 4.0
- Ruins of the Ancient Atlantians: Vandalorum
	- File name: `lzr_ambience_vandalorum_ruins_of_the_ancient_atlantians.ogg`
	- Original file name: `vandalorum_-_ruins_of_the_ancient_atlantians.wav`
	- Origin: <https://opengameart.org/content/water-level-synth-music>
	- License: CC BY 4.0
- Swept Beneath the Tide: Vandalorum
	- File name: `lzr_ambience_vandalorum_swept_beneath_the_tide.ogg`
	- Original file name: `vandalorum_-_swept_beneath_the_tide.wav`
	- Origin: <https://opengameart.org/content/water-level-synth-music>
	- License: CC BY 4.0
- The Cold Wizard Summons a Storm: Vandalorum
	- File name: `lzr_ambience_vandalorum_the_cold_wizard_summons_a_storm.ogg`
	- Original file name: `vandalorum_-_the_cold_wizard_summons_a_storm.wav`
	- Origin: <https://opengameart.org/content/water-level-synth-music>
	- License: CC BY 4.0
- Ancient Mystery Waltz Allegro: Kevin MacLeod (incompetech.com)
	- File name: `lzr_ambience_kml_ancient_mystery_waltz_allegro.ogg`
	- Original file name: `Ancient Mystery Waltz Allegro.mp3`
	- License: CC BY 4.0
- Ave Marimba: Kevin MacLeod (incompetech.com)
	- File name: `lzr_ambience_kml_ave_marimba.ogg`
	- Original file name: `Ave Marimba.mp3`
	- License: CC BY 4.0
- Island Meet and Greet: Kevin MacLeod (incompetech.com)
	- File name: `lzr_ambience_kml_island_meet_and_greet.ogg`
	- Original file name: `Island Meet and Greet.mp3`
	- License: CC BY 4.0
- Moonlight Beach: Kevin MacLeod (incompetech.com)
	- File name: `lzr_ambience_kml_moonlight_beach.ogg`
	- Original file name: `Moonlight Beach.mp3`
	- License: CC BY 4.0
- Morgana Rides: Kevin MacLeod (incompetech.com)
	- File name: `lzr_ambience_kml_morgana_rides.ogg`
	- Original file name: `Morgana Rides.mp3`
	- License: CC BY 4.0
- River Fire: Kevin MacLeod (incompetech.com)
	- File name: `lzr_ambience_kml_river_fire.ogg`
	- Original file name: `River Fire.mp3`
	- License: CC BY 4.0
- Tiki Bar Mixer: Kevin MacLeod (incompetech.com)
	- File name: `lzr_ambience_kml_tiki_bar_mixer.ogg`
	- Original file name: `Tiki Bar Mixer.mp3`
	- License: CC BY 4.0

## Sound Effects

- Stone digging, dirt footstep/placement/digging, sand footstep/placement/digging, cloth footstep/placement/digging, leaves footstep, grass footstep/digging, jungle litter footstep, water footstep/placement/digging, sticks footstep/placement, forge footstep, inventory full warning: Wuzzy
	- File names: `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`, `lzr_sounds_dug_stone.*.ogg`
	- License: MIT
- Stone footstep: KiranKeegan
	- File name: `lzr_sounds_footstep_stone.*.ogg`
	- License: CC0
- Stone placement: StarNinjas
	- File name: `lzr_sounds_place_stone.*.ogg`
	- License: CC0
- Metal digging: Ivan Gabovitch (alias qubodup)
	- File name: `default_dug_metal.*.ogg`
	- License: CC0
- Metal digging: yadronoff
	- File name: `default_dig_metal.ogg`
	- Original file name: `Metal Rumble.wav`
	- Origin: <https://www.freesound.org/people/yadronoff/sounds/320397/>
	- License: CC BY 3.0
- Metal footstep: mypantsfelldown
	- File name: `default_metal_footstep.*.ogg`
	- Original file name: `Metal Footsteps`
	- Origin: <https://freesound.org/people/mypantsfelldown/sounds/398937/>
	- License: CC0
- Metal placement: Ogrebane
	- File name: `default_place_node_metal.*.ogg`
	- Original file name: `default_place_node_metal.*.ogg`
	- Origin: <http://opengameart.org/content/wood-and-metal-sound-effects-volume-2>
	- License: CC0
- Glass footstep: deleted\_user\_2194797
	- File name: `lzr_sounds_footstep_glass.*.ogg`
	- Original file name: `Ding 4`
	- Origin: <https://freesound.org/people/deleted_user_2104797/sounds/325252/>
	- Modifications were made
	- License: CC0
- Glass digging: kelsey\_w
	- File name: `lzr_sounds_dig_glass.ogg`
	- Original file name: `Glass Bump.wav`
	- Origin: <https://freesound.org/people/kelsey_w/sounds/467039/>
	- Modifications were made
	- License: CC BY 3.0
- Glass placement: kbnevel
	- File name: `lzr_sounds_place_glass.*.ogg`
	- Original file name: `Glass_Clink.aif`
	- Origin: <https://freesound.org/people/kbnevel/sounds/119839/>
	- Modifications were made
	- License: CC0
- Wood footstep: Cpfcfan10
	- File name: `lzr_sounds_footstep_wood.*.ogg`
	- Original file name: `Wooden Stairs`
	- Origin: <https://freesound.org/people/Cpfcfan10/sounds/522555/>
	- Modifications were made
	- License: CC BY 4.0
- Wood placement and digging: youandbiscuitme
	- File name: `lzr_sounds_place_wood.*.ogg`
	- Original file name: `wooden object set on table 1`
	- Origin: <https://freesound.org/people/youandbiscuitme/sounds/258244/>
	- Modifications were made
	- License: CC BY 3.0
- Normal rotation, wood rotation: el\_boss
	- File names: `lzr_hook_rotate.ogg`, `lzr_hook_rotate.ogg`
	- Original file name: `Puzzle piece rotation (clockwise)`
	- Origin: <https://freesound.org/people/el_boss/sounds/560700/>
	- License: CC0
- Glass/mirror rotation: killamn97
	- File name: `lzr_sounds_rotate_glass.ogg`
	- Original file name: `Rotate Mirror 3.wav`
	- Origin: <https://freesound.org/people/killianm97/sounds/554236/>
	- License: CC0
- Stone rotation: Ivan Gabovitch (alias qubodup)
	- File name: `lzr_sounds_rotate_stone.ogg`
	- Original file name: `Stone Touch`
	- Origin: <https://freesound.org/people/qubodup/sounds/743264/>
	- License: CC0
- Metal rotation: MultiMax2121
	- File name: `lzr_sounds_rotate_metal.ogg`
	- Original file name: `grenade sound effect.mp3`
	- Origin: <https://freesound.org/people/MultiMax2121/sounds/156896/>
	- Modifications were made
	- License: CC0
- Laser emitter activation: PhonosUPF
	- File name: `lzr_laser_emitter_activate.ogg`
	- Original file name: `accordion percussion 2a`
	- Origin: <https://freesound.org/people/PhonosUPF/sounds/501976/>
	- License: CC0
- Level entering and leaving: junggle
	- File names: `lzr_levels_level_enter.ogg`, `lzr_levels_level_enter.ogg`
	- Original file name: `accordeon_21.wav`
	- Origin: <https://freesound.org/people/junggle/sounds/27355/>
	- Modifications were made
	- License: CC BY 4.0
- Level completion jingle: Fupicat
	- File name: `lzr_levels_level_complete.ogg`
	- Original file name: `WinBandoneon.wav`
	- Origin: <https://freesound.org/people/Fupicat/sounds/521641/>
	- Modifications were made
	- License: CC0
- Game completion jingle: X3nus
	- File name: `lzr_levels_level_set_complete.ogg`
	- Original file name: `Pirate's Bounty.wav`
	- Origin: <https://freesound.org/s/449939/>
	- License: CC BY 3.0
- Button: Fourier
	- File name: `lzr_sounds_button.ogg`
	- Original file name: `button press 1.wav`
	- Origin: <https://opengameart.org/content/forward-button-press-ui-sound>
	- Modifications were made
	- License: CC BY 3.0
- Barricade burning: florianreichelt
	- File name: `lzr_laser_quickburn.*.ogg`
	- Original file name: `flames sizzling of a bonfire - FIRE`
	- Origin: <https://freesound.org/people/florianreichelt/sounds/563012>
	- Modifications were made
	- License: CC0
- Sticks digging / barricade breaking: kevinkace
	- File name: `lzr_sounds_dug_sticks.ogg`
	- Original file name: `Crate Break 4.wav`
	- Origin: <https://freesound.org/people/kevinkace/sounds/66780/>
	- License: CC0
- Skull footstep/placement/digging/rotation: The Battle for Wesnoth developers
	- File name: `lzr_laser_skull_*.ogg`
	- Original file names: `skeleton-die-1.ogg`, `skeleton-die-1.ogg`, `skeleton-die-1.ogg`, `skeleton-die-1.ogg`, `skeleton-die-1.ogg`
	- Origin: <https://github.com/wesnoth/wesnoth/>
	- License: GPLv2+
- Squeaky wood: rubberduck
	- File name: `lzr_sounds_footstep_wood_squeak.*.ogg`
	- Origin: <https://opengameart.org/content/100-cc0-metal-and-wood-sfx>
	- License: CC0
- Chest opening: JUDITH136
	- File name: `lzr_treasure_chest_open.ogg`
	- Original file name: `20.wav`
	- Origin: <https://freesound.org/people/JUDITH136/sounds/408001/>
	- License: CC BY 3.0
- Chest opening fails: Dymewiz
	- File name: `lzr_treasure_chest_open_fail.ogg`
	- Original file name: `locking a door_02.wav`
	- Origin: <https://freesound.org/people/Dymewiz/sounds/131029/>
	- License: CC BY 3.0
- Chest lock breaks: MAJ061785
	- File name: `lzr_treasure_chest_lock_break.ogg`
	- Original file name: `Breaking lock.aif`
	- Origin: <https://freesound.org/people/MAJ061785/sounds/85533/>
	- License: CC BY 3.0
- Chest lock regenerates: MAJ061785
	- File name: `lzr_treasure_chest_lock_regen.ogg`
	- Original file name: `Breaking lock.aif`
	- Origin: <https://freesound.org/people/MAJ061785/sounds/85533/>
	- Modifications were made
	- License: CC BY 3.0
- Detector activates/deactivates: Artninja
	- File names: `lzr_laser_detector_activate.ogg`, `lzr_laser_detector_activate.ogg`
	- Original file name: `lock_mechanisms_from_van`
	- Origin: <https://freesound.org/people/Artninja/sounds/719255/>
	- Modifications were made
	- License: CC BY 4.0
- Lightbox activates/deactivates: Ryding
	- File names: `lzr_decor_lightbox_on.ogg`, `lzr_decor_lightbox_on.ogg`
	- Original file name: `Flourescent tube`
	- Origin: <https://freesound.org/people/Ryding/sounds/718460/>
	- Modifications were made
	- License: CC BY 4.0
- Speaker turns on/off: mincedbeats
	- File names: `lzr_menu_speaker_turn_on.ogg`, `lzr_menu_speaker_turn_on.ogg`
	- Original file name: `Yamaha HS80 Switch.wav`
	- Origin: <https://freesound.org/people/mincedbeats/sounds/593996/>
	- Modifications were made
	- License: CC0
- Player damage: newagesoup
	- File name: `lzr_damage_damage.ogg`
	- Original file name: `PUNCH-BOXING-03.wav`
	- Origin: <https://freesound.org/people/newagesoup/sounds/348242/>
	- License: CC0
- Skull laugh: klankbeeld
	- File name: `lzr_fallout_skull_laugh.ogg`
	- Original file name: `laugh.WAV`
	- Origin: <https://freesound.org/people/klankbeeld/sounds/126113/>
	- Modifications were made
	- License: CC BY 4.0
- Bomb fuse: maximumplay3r
	- File name: `lzr_laser_bomb_fuse.ogg`
	- Original file name: `Fuse Ignition`
	- Origin: <https://freesound.org/people/maximumplay3r/sounds/713344/>
	- License: CC0
- Bomb explosion: WaveAdventurer
	- File name: `lzr_laser_bomb_explode.ogg`
	- Original file name: `explosion1(crunchier)`
	- Origin: <https://freesound.org/people/WaveAdventurer/sounds/732252/>
	- Modifications were made
	- License: CC BY 4.0
- Change color or screw: artisticdude
	- File names: `lzr_laser_change_color.ogg`, `lzr_laser_change_color.ogg`
	- Origin: <https://opengameart.org/content/rpg-sound-pack>
	- License: CC0
- Old crate breaks: melle\_teich
	- File name: `lzr_laser_crate_old_break.*.ogg`
	- Original file name: `Tearing pieces of wood`
	- Origin: <https://freesound.org/people/melle_teich/sounds/628398/>
	- License: CC0
- Default block digging/placement: Mito551
	- File names: `default_dug_node.*.ogg`, `default_dug_node.*.ogg`, `default_dug_node.*.ogg`
	- Original file names: `default_dug_node.*.ogg`, `default_dug_node.*.ogg`, `default_dug_node.*.ogg`
	- Origin: <https://github.com/minetest/minetest_game>
	- License: CC BY 3.0
- Parrot curr: Breviceps
	- File name: `lzr_parrot_npc_curr.ogg`
	- Original file name: `Animal / Cartoon - Cooing, Curr - Parrot`
	- Origin: <https://freesound.org/people/Breviceps/sounds/445119/>
	- License: CC0
- Door lock: saha213131
	- File name: `lzr_doors_door_locked.ogg`
	- Original file name: `doorlocked01`
	- Origin: <https://freesound.org/people/saha213131/sounds/730189/>
	- License: CC0
- Any sound not listed above: Wuzzy
	- License: CC BY 4.0

## Translations

- German: Wuzzy
	- License: CC BY 4.0
- Chinese (traditional): Emojigit
	- License: CC BY 4.0
- Spanish: megustanlosfrijoles
	- License: CC BY 4.0
- French: Nazalassa
	- License: CC BY 4.0
- Russian: f1refa11
	- License: CC BY 4.0

## Other stuff

- See the licence files of the individual mods

## File licensing

Every file is (at least) dual-licensed. This means it is both licensed
under the terms of the game license (see README.md) as well as an individual
file license (shown in this list).

Only licenses compatible with free software are used in this game.

## License references

This is a reference of all license name abbreviations of the licenses listed above:

* GPLv3+: GNU General Public License version 3, or any later version <https://www.gnu.org/licenses/gpl-3.0.en.html>
* GPLv2+: GNU General Public License version 2, or any later version <https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>
* LGPLv2.1+: GNU Lesser General Public License version 2.1, or any later version <https://www.gnu.org/licenses/old-licenses/lgpl-2.1>
* MIT: The MIT License <https://mit-license.org/>
* BSD 3-Clause: The BSD 3-Clause License <https://opensource.org/license/BSD-3-clause>
* CC0: Creative Commons Zero <https://creativecommons.org/publicdomain/zero/1.0/>
* CC BY 3.0: Creative Commons Attribution 3.0 Unported <https://creativecommons.org/licenses/by/3.0/>
* CC BY 4.0: Creative Commons Attribution 4.0 International <https://creativecommons.org/licenses/by/4.0/>
* CC BY-SA 3.0: Creative Commons Attribution-ShareAlike 3.0 Unported <https://creativecommons.org/licenses/by-sa/3.0/>
* CC BY-SA 4.0: Creative Commons Attribution-ShareAlike 4.0 International <https://creativecommons.org/licenses/by-sa/4.0/>
* OGA-BY 3.0: OpenGameArt.org Legal Code Attribution 3.0 Unported <https://static.opengameart.org/OGA-BY-3.0.txt>
